﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using Common.Helper;
using System.IO.Compression;




namespace Common
{
    public class DataHandler
    {

        string Destination = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Resource";
        string Destination1 = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Log";
        string Destination2 = ConfigurationManager.AppSettings["DESTINATION_PATH2"];

        public void SetUserDataToFile(UserCredential user)
        {
            if (!Directory.Exists(Destination))
            {
                Directory.CreateDirectory(Destination);
            }
            string JsonUser = JsonConvert.SerializeObject(user);
            JsonUser = Helper.Crypto.Encrypt(JsonUser);
            string filename = Destination + ConfigurationManager.AppSettings["UserInfo"];
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                file.WriteLine(JsonUser);
                file.Close();
            }

        }

        public void SetDBBilledWorkOrderListToCSV(List<BilledWorkOrder> list, int count)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                string filepath = Destination + ConfigurationManager.AppSettings["BilledWorkOrder"] + "-" + DateTime.Now.Minute.ToString() + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["BilledWorkOrder"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                {
                    file.WriteLine("WorkOrderID,Type,RequestBy,AppointmentDateTime,ArrivalDateTime,InMileage,PromisedCompletionDateTime,WorkStage,StorageRate,FreeStorageDays,StorageDays,CompletedDateTime,OutMileage,CustomerID,FirstName,LastName,MiddleName,CustomerCompanyName,NamePrefix,NameSuffix,Address1,Address2,City,State,PostalCode,PostalCodeSuffix,County,Country,EmailAddress,HomePhone,MobilePhone,WorkPhone,WorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactEmail,DoNotContactLetter,VIN,Make,Model,ModelYear,StockNumber,ServiceWriter_EmployeeCode,ServiceWriter_FirstName,ServiceWriter_LastName,LaborRateAmount,TotalNonRetailPartsAmount,TotalNonRetailLaborAmount,TotalPartsAmount,TotalLaborAmount,TotalContractLaborAmount,TotalESPDeductibleAmount,TotalShopSuppliesAmount,TotalStorageAmount,TotalTaxesFeesAmount,TotalWorkOrderAmount,TotalDepositsApplied,TotalDueAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57}"
                            , list[i].WorkOrderID.ToString()
                            , list[i].Type
                            , list[i].RequestBy.ToString()
                            , list[i].AppointmentDateTime.ToString()
                            , list[i].ArrivalDateTime.ToString()
                            , list[i].InMileage.ToString()
                            , list[i].PromisedCompletionDateTime.ToString()
                            , list[i].WorkStage.ToString()
                            , list[i].StorageRate.ToString()
                            , list[i].FreeStorageDays.ToString()
                            , list[i].StorageDays.ToString()
                            , list[i].CompletedDateTime.ToString()
                            , list[i].OutMileage.ToString()
                            , list[i].CustomerID.ToString()
                            , list[i].FirstName.ToString()
                            , list[i].LastName.ToString()
                            , list[i].MiddleName.ToString()
                            , list[i].CustomerCompanyName.ToString()
                            , list[i].NamePrefix.ToString()
                            , list[i].NameSuffix.ToString()
                            , list[i].Address1.ToString()
                            , list[i].Address2.ToString()
                            , list[i].City.ToString()
                            , list[i].State.ToString()
                            , list[i].PostalCode.ToString()
                            , list[i].PostalCodeSuffix.ToString()
                            , list[i].County.ToString()
                            , list[i].Country.ToString()
                            , list[i].EmailAddress.ToString()
                            , list[i].HomePhone.ToString()
                            , list[i].MobilePhone.ToString()
                            , list[i].WorkPhone.ToString()
                            , list[i].WorkPhoneExt.ToString()
                            , list[i].PreferredContact.ToString()
                            , list[i].DoNotContactPhone.ToString()
                            , list[i].DoNotContactEmail.ToString()
                            , list[i].DoNotContactLetter.ToString()
                            , list[i].VIN.ToString()
                            , list[i].Make.ToString()
                            , list[i].Model.ToString()
                            , list[i].ModelYear.ToString()
                            , list[i].StockNumber.ToString()
                            , list[i].ServiceWriter_EmployeeCode.ToString()
                            , list[i].ServiceWriter_FirstName.ToString()
                            , list[i].ServiceWriter_LastName.ToString()
                            , list[i].LaborRateAmount.ToString()
                            , list[i].TotalNonRetailPartsAmount.ToString()
                            , list[i].TotalNonRetailLaborAmount.ToString()
                            , list[i].TotalPartsAmount.ToString()
                            , list[i].TotalLaborAmount.ToString()
                            , list[i].TotalContractLaborAmount.ToString()
                            , list[i].TotalESPDeductibleAmount.ToString()
                            , list[i].TotalShopSuppliesAmount.ToString()
                            , list[i].TotalStorageAmount.ToString()
                            , list[i].TotalTaxesFeesAmount.ToString()
                            , list[i].TotalWorkOrderAmount.ToString()
                            , list[i].TotalDepositsApplied.ToString()
                            , list[i].TotalDueAmount.ToString());

                    }
                    file.Close();
                }

                // FTPUploadFile FTP = new FTPUploadFile();
                // FTP.FileUploadFTP(filepath, filename);

            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBBilledWorkOrderListToCSV";
                logdata.base_message = "Set DBBilledWorkOrder list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBClosedVehicleDealsListToCSV(List<ClosedVehicleDeals> list)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }

                string filename = Destination + ConfigurationManager.AppSettings["ClosedVehicleDeals"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
                {
                    file.WriteLine("DealID,DealStatus,CreateDAteTime,ContractDateTime,ClosedDateTime,DeliveryDateTime,InvoiceDateTime,InactiveDateTime,VIN,Make,Model,ModelYear,Mileage,Color,FactoryInstalledOptions,PreOwnedFlag,StockNumber,BuyerCustomerID,BuyerFirstName,BuyerLastName,BuyerMiddleName,BuyerCompanyName,BuyerNamePrefix,BuyerNameSuffix,BuyerAddress1,BuyerAddress2,BuyerCity,BuyerState,BuyerPostalCode,BuyerPostalCodeSuffix,BuyerCounty,BuyerCountry,BuyerEmailAddress,BuyerHomePhone,BuyerMobilePhone,BuyerWorkPhone,BuyerWorkPhoneExt,BuyerPreferredContact,BuyerDoNotContactPhone,BuyerDoNotContactEmail,BuyerDoNotContactLetter,CoBuyerCustomerID,CoBuyerFirstName,CoBuyerLastName,CoBuyerMiddleName,CoBuyerCompanyName,CoBuyerNamePrefix,CoBuyerNameSuffix,CoBuyerAddress1,CoBuyerAddress2,CoBuyerCity,CoBuyerState,CoBuyerPostalCode,CoBuyerPostalCodeSuffix,CoBuyerCounty,CoBuyerCountry,CoBuyerEmailAddress,CoBuyerHomePhone,CoBuyerMobilePhone,CoBuyerWorkPhone,CoBuyerWorkPhoneExt,CoBuyerPreferredContact,CoBuyerDoNotContactPhone,CoBuyerDoNotContactEmail,CoBuyerDoNotContactLetter,VehicleSalesperson1_EmployeeCode,VehicleSalesperson1_FirstName,VehicleSalesperson1_LastName,VehicleSalesperson2_EmployeeCode,VehicleSalesperson2_FirstName,VehicleSalesperson2_LastName,FinanceInsuranceSalesperson1_EmployeeCode,FinanceInsuranceSalesperson1_FirstName,FinanceInsuranceSalesperson1_LastName,FinanceInsuranceSalesperson2_EmployeeCode,FinanceInsuranceSalesperson2_FirstName,FinanceInsuranceSalesperson2_LastName,SalesManager_EmployeeCode,SalesManager_FirstName,SalesManager_LastName,Lienholder,LoanAPR,LoanTerm,LoanFirstPayDate,LoanPaymentsPerYear,LoanPaymentAmount,DeallinkApplicationID,TotalRetailPrice,TotalAccessoriesandLaborAmount,TotalAddOnAmount,TotalESPAmount,TotalPPMAmount,TotalAccidentHealthAmount,TotalCreditLifeAmount,TotalChargesandFeesAmount,TotalVehicleTaxAmount,TotalAccessoriesandLaborTaxAmount,TotalSupplementalChargesTaxAmount,TotalAccessoryFeesAmount,TotalDocStampsAmount,TotalTaxAmount,TotalTradeInAllowanceAmount,TotalTradeInPayoffAmount,TotalTradeInActualCashValueAmount,TotalDepositAmount,TotalDownPaymentAmount,TotalRebateAmount,TotalFinancedAmount,TotalDueAmount,AddOnItemID,AddOnItemName,AddOnItemVendorID,AddOnItemStatus,AddOnItemUnitPriceAmount,AddOnItemQuantity,AddOnItemListPriceAmount,AddOnItemDiscountAmount,AddOnItemDiscountRate,AddOnItemSalesTaxAmount,ChargeID,ChargeName,ChargeVendorID,ChargeCostAmount,ChargeListPriceAmount,ChargeSalesTaxAmount,TradeInVIN,TradeInMake,TradeInModel,TradeInModelYear,TradeInMileage,TradeInColor,TradeInFactoryInstalledOptions,TradeInStockNumber,TradeInAllowanceAmount,TradeInPayoffAmount,TradeInActualCashValueAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57},{58},{59},{60},{61},{62},{63},{64},{65},{66},{67},{68},{69},{70},{71},{72},{73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94},{95},{96},{97},{98},{99},{100},{101},{102},{103},{104},{105},{106},{107},{108},{109},{110},{111},{112},{113},{114},{115},{116},{117},{118},{119},{120},{121},{122},{123},{124},{125},{126},{127},{128},{129},{130},{131},{132},{133},{134},{135}"
                            , list[i].DealID
                            , list[i].DealStatus
                            , list[i].CreateDAteTime
                            , list[i].ContractDateTime
                            , list[i].ClosedDateTime
                            , list[i].DeliveryDateTime
                            , list[i].InvoiceDateTime
                            , list[i].InactiveDateTime
                            , list[i].VIN
                            , list[i].Make
                            , list[i].Model
                            , list[i].ModelYear
                            , list[i].Mileage
                            , list[i].Color
                            , list[i].FactoryInstalledOptions
                            , list[i].PreOwnedFlag
                            , list[i].StockNumber
                            , list[i].BuyerCustomerID
                            , list[i].BuyerFirstName
                            , list[i].BuyerLastName
                            , list[i].BuyerMiddleName
                            , list[i].BuyerCompanyName
                            , list[i].BuyerNamePrefix
                            , list[i].BuyerNameSuffix
                            , list[i].BuyerAddress1
                            , list[i].BuyerAddress2
                            , list[i].BuyerCity
                            , list[i].BuyerState
                            , list[i].BuyerPostalCode
                            , list[i].BuyerPostalCodeSuffix
                            , list[i].BuyerCounty
                            , list[i].BuyerCountry
                            , list[i].BuyerEmailAddress
                            , list[i].BuyerHomePhone
                            , list[i].BuyerMobilePhone
                            , list[i].BuyerWorkPhone
                            , list[i].BuyerWorkPhoneExt
                            , list[i].BuyerPreferredContact
                            , list[i].BuyerDoNotContactPhone
                            , list[i].BuyerDoNotContactEmail
                            , list[i].BuyerDoNotContactLetter
                            , list[i].CoBuyerCustomerID
                            , list[i].CoBuyerFirstName
                            , list[i].CoBuyerLastName
                            , list[i].CoBuyerMiddleName
                            , list[i].CoBuyerCompanyName
                            , list[i].CoBuyerNamePrefix
                            , list[i].CoBuyerNameSuffix
                            , list[i].CoBuyerAddress1
                            , list[i].CoBuyerAddress2
                            , list[i].CoBuyerCity
                            , list[i].CoBuyerState
                            , list[i].CoBuyerPostalCode
                            , list[i].CoBuyerPostalCodeSuffix
                            , list[i].CoBuyerCounty
                            , list[i].CoBuyerCountry
                            , list[i].CoBuyerEmailAddress
                            , list[i].CoBuyerHomePhone
                            , list[i].CoBuyerMobilePhone
                            , list[i].CoBuyerWorkPhone
                            , list[i].CoBuyerWorkPhoneExt
                            , list[i].CoBuyerPreferredContact
                            , list[i].CoBuyerDoNotContactPhone
                            , list[i].CoBuyerDoNotContactEmail
                            , list[i].CoBuyerDoNotContactLetter
                            , list[i].VehicleSalesperson1_EmployeeCode
                            , list[i].VehicleSalesperson1_FirstName
                            , list[i].VehicleSalesperson1_LastName
                            , list[i].VehicleSalesperson2_EmployeeCode
                            , list[i].VehicleSalesperson2_FirstName
                            , list[i].VehicleSalesperson2_LastName
                            , list[i].FinanceInsuranceSalesperson1_EmployeeCode
                            , list[i].FinanceInsuranceSalesperson1_FirstName
                            , list[i].FinanceInsuranceSalesperson1_LastName
                            , list[i].FinanceInsuranceSalesperson2_EmployeeCode
                            , list[i].FinanceInsuranceSalesperson2_FirstName
                            , list[i].FinanceInsuranceSalesperson2_LastName
                            , list[i].SalesManager_EmployeeCode
                            , list[i].SalesManager_FirstName
                            , list[i].SalesManager_LastName
                            , list[i].Lienholder
                            , list[i].LoanAPR
                            , list[i].LoanTerm
                            , list[i].LoanFirstPayDate
                            , list[i].LoanPaymentsPerYear
                            , list[i].LoanPaymentAmount
                            , list[i].DeallinkApplicationID
                            , list[i].TotalRetailPrice
                            , list[i].TotalAccessoriesandLaborAmount
                            , list[i].TotalAddOnAmount
                            , list[i].TotalESPAmount
                            , list[i].TotalPPMAmount
                            , list[i].TotalAccidentHealthAmount
                            , list[i].TotalCreditLifeAmount
                            , list[i].TotalChargesandFeesAmount
                            , list[i].TotalVehicleTaxAmount
                            , list[i].TotalAccessoriesandLaborTaxAmount
                            , list[i].TotalSupplementalChargesTaxAmount
                            , list[i].TotalAccessoryFeesAmount
                            , list[i].TotalDocStampsAmount
                            , list[i].TotalTaxAmount
                            , list[i].TotalTradeInAllowanceAmount
                            , list[i].TotalTradeInPayoffAmount
                            , list[i].TotalTradeInActualCashValueAmount
                            , list[i].TotalDepositAmount
                            , list[i].TotalDownPaymentAmount
                            , list[i].TotalRebateAmount
                            , list[i].TotalFinancedAmount
                            , list[i].TotalDueAmount
                            , list[i].AddOnItemID
                            , list[i].AddOnItemName
                            , list[i].AddOnItemVendorID
                            , list[i].AddOnItemStatus
                            , list[i].AddOnItemUnitPriceAmount
                            , list[i].AddOnItemQuantity
                            , list[i].AddOnItemListPriceAmount
                            , list[i].AddOnItemDiscountAmount
                            , list[i].AddOnItemDiscountRate
                            , list[i].AddOnItemSalesTaxAmount
                            , list[i].ChargeID
                            , list[i].ChargeName
                            , list[i].ChargeVendorID
                            , list[i].ChargeCostAmount
                            , list[i].ChargeListPriceAmount
                            , list[i].ChargeSalesTaxAmount
                            , list[i].TradeInVIN
                            , list[i].TradeInMake
                            , list[i].TradeInModel
                            , list[i].TradeInModelYear
                            , list[i].TradeInMileage
                            , list[i].TradeInColor
                            , list[i].TradeInFactoryInstalledOptions
                            , list[i].TradeInStockNumber
                            , list[i].TradeInAllowanceAmount
                            , list[i].TradeInPayoffAmount
                            , list[i].TradeInActualCashValueAmount);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);


                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBClosedVehicleDealsListToCSV";
                logdata.base_message = "Set DBClosedVehicleDeals list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBClosedVehicleDealsListToCSV(List<ClosedVehicleDeals> list, int count)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }

                string filepath = Destination + ConfigurationManager.AppSettings["ClosedVehicleDeals"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["ClosedVehicleDeals"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                {
                    file.WriteLine("DealID,DealStatus,CreateDAteTime,ContractDateTime,ClosedDateTime,DeliveryDateTime,InvoiceDateTime,InactiveDateTime,VIN,Make,Model,ModelYear,Mileage,Color,FactoryInstalledOptions,PreOwnedFlag,StockNumber,BuyerCustomerID,BuyerFirstName,BuyerLastName,BuyerMiddleName,BuyerCompanyName,BuyerNamePrefix,BuyerNameSuffix,BuyerAddress1,BuyerAddress2,BuyerCity,BuyerState,BuyerPostalCode,BuyerPostalCodeSuffix,BuyerCounty,BuyerCountry,BuyerEmailAddress,BuyerHomePhone,BuyerMobilePhone,BuyerWorkPhone,BuyerWorkPhoneExt,BuyerPreferredContact,BuyerDoNotContactPhone,BuyerDoNotContactEmail,BuyerDoNotContactLetter,CoBuyerCustomerID,CoBuyerFirstName,CoBuyerLastName,CoBuyerMiddleName,CoBuyerCompanyName,CoBuyerNamePrefix,CoBuyerNameSuffix,CoBuyerAddress1,CoBuyerAddress2,CoBuyerCity,CoBuyerState,CoBuyerPostalCode,CoBuyerPostalCodeSuffix,CoBuyerCounty,CoBuyerCountry,CoBuyerEmailAddress,CoBuyerHomePhone,CoBuyerMobilePhone,CoBuyerWorkPhone,CoBuyerWorkPhoneExt,CoBuyerPreferredContact,CoBuyerDoNotContactPhone,CoBuyerDoNotContactEmail,CoBuyerDoNotContactLetter,VehicleSalesperson1_EmployeeCode,VehicleSalesperson1_FirstName,VehicleSalesperson1_LastName,VehicleSalesperson2_EmployeeCode,VehicleSalesperson2_FirstName,VehicleSalesperson2_LastName,FinanceInsuranceSalesperson1_EmployeeCode,FinanceInsuranceSalesperson1_FirstName,FinanceInsuranceSalesperson1_LastName,FinanceInsuranceSalesperson2_EmployeeCode,FinanceInsuranceSalesperson2_FirstName,FinanceInsuranceSalesperson2_LastName,SalesManager_EmployeeCode,SalesManager_FirstName,SalesManager_LastName,Lienholder,LoanAPR,LoanTerm,LoanFirstPayDate,LoanPaymentsPerYear,LoanPaymentAmount,DeallinkApplicationID,TotalRetailPrice,TotalAccessoriesandLaborAmount,TotalAddOnAmount,TotalESPAmount,TotalPPMAmount,TotalAccidentHealthAmount,TotalCreditLifeAmount,TotalChargesandFeesAmount,TotalVehicleTaxAmount,TotalAccessoriesandLaborTaxAmount,TotalSupplementalChargesTaxAmount,TotalAccessoryFeesAmount,TotalDocStampsAmount,TotalTaxAmount,TotalTradeInAllowanceAmount,TotalTradeInPayoffAmount,TotalTradeInActualCashValueAmount,TotalDepositAmount,TotalDownPaymentAmount,TotalRebateAmount,TotalFinancedAmount,TotalDueAmount,AddOnItemID,AddOnItemName,AddOnItemVendorID,AddOnItemStatus,AddOnItemUnitPriceAmount,AddOnItemQuantity,AddOnItemListPriceAmount,AddOnItemDiscountAmount,AddOnItemDiscountRate,AddOnItemSalesTaxAmount,ChargeID,ChargeName,ChargeVendorID,ChargeCostAmount,ChargeListPriceAmount,ChargeSalesTaxAmount,TradeInVIN,TradeInMake,TradeInModel,TradeInModelYear,TradeInMileage,TradeInColor,TradeInFactoryInstalledOptions,TradeInStockNumber,TradeInAllowanceAmount,TradeInPayoffAmount,TradeInActualCashValueAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57},{58},{59},{60},{61},{62},{63},{64},{65},{66},{67},{68},{69},{70},{71},{72},{73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94},{95},{96},{97},{98},{99},{100},{101},{102},{103},{104},{105},{106},{107},{108},{109},{110},{111},{112},{113},{114},{115},{116},{117},{118},{119},{120},{121},{122},{123},{124},{125},{126},{127},{128},{129},{130},{131},{132},{133},{134},{135}"
                            , list[i].DealID
                            , list[i].DealStatus
                            , list[i].CreateDAteTime
                            , list[i].ContractDateTime
                            , list[i].ClosedDateTime
                            , list[i].DeliveryDateTime
                            , list[i].InvoiceDateTime
                            , list[i].InactiveDateTime
                            , list[i].VIN
                            , list[i].Make
                            , list[i].Model
                            , list[i].ModelYear
                            , list[i].Mileage
                            , list[i].Color
                            , list[i].FactoryInstalledOptions
                            , list[i].PreOwnedFlag
                            , list[i].StockNumber
                            , list[i].BuyerCustomerID
                            , list[i].BuyerFirstName
                            , list[i].BuyerLastName
                            , list[i].BuyerMiddleName
                            , list[i].BuyerCompanyName
                            , list[i].BuyerNamePrefix
                            , list[i].BuyerNameSuffix
                            , list[i].BuyerAddress1
                            , list[i].BuyerAddress2
                            , list[i].BuyerCity
                            , list[i].BuyerState
                            , list[i].BuyerPostalCode
                            , list[i].BuyerPostalCodeSuffix
                            , list[i].BuyerCounty
                            , list[i].BuyerCountry
                            , list[i].BuyerEmailAddress
                            , list[i].BuyerHomePhone
                            , list[i].BuyerMobilePhone
                            , list[i].BuyerWorkPhone
                            , list[i].BuyerWorkPhoneExt
                            , list[i].BuyerPreferredContact
                            , list[i].BuyerDoNotContactPhone
                            , list[i].BuyerDoNotContactEmail
                            , list[i].BuyerDoNotContactLetter
                            , list[i].CoBuyerCustomerID
                            , list[i].CoBuyerFirstName
                            , list[i].CoBuyerLastName
                            , list[i].CoBuyerMiddleName
                            , list[i].CoBuyerCompanyName
                            , list[i].CoBuyerNamePrefix
                            , list[i].CoBuyerNameSuffix
                            , list[i].CoBuyerAddress1
                            , list[i].CoBuyerAddress2
                            , list[i].CoBuyerCity
                            , list[i].CoBuyerState
                            , list[i].CoBuyerPostalCode
                            , list[i].CoBuyerPostalCodeSuffix
                            , list[i].CoBuyerCounty
                            , list[i].CoBuyerCountry
                            , list[i].CoBuyerEmailAddress
                            , list[i].CoBuyerHomePhone
                            , list[i].CoBuyerMobilePhone
                            , list[i].CoBuyerWorkPhone
                            , list[i].CoBuyerWorkPhoneExt
                            , list[i].CoBuyerPreferredContact
                            , list[i].CoBuyerDoNotContactPhone
                            , list[i].CoBuyerDoNotContactEmail
                            , list[i].CoBuyerDoNotContactLetter
                            , list[i].VehicleSalesperson1_EmployeeCode
                            , list[i].VehicleSalesperson1_FirstName
                            , list[i].VehicleSalesperson1_LastName
                            , list[i].VehicleSalesperson2_EmployeeCode
                            , list[i].VehicleSalesperson2_FirstName
                            , list[i].VehicleSalesperson2_LastName
                            , list[i].FinanceInsuranceSalesperson1_EmployeeCode
                            , list[i].FinanceInsuranceSalesperson1_FirstName
                            , list[i].FinanceInsuranceSalesperson1_LastName
                            , list[i].FinanceInsuranceSalesperson2_EmployeeCode
                            , list[i].FinanceInsuranceSalesperson2_FirstName
                            , list[i].FinanceInsuranceSalesperson2_LastName
                            , list[i].SalesManager_EmployeeCode
                            , list[i].SalesManager_FirstName
                            , list[i].SalesManager_LastName
                            , list[i].Lienholder
                            , list[i].LoanAPR
                            , list[i].LoanTerm
                            , list[i].LoanFirstPayDate
                            , list[i].LoanPaymentsPerYear
                            , list[i].LoanPaymentAmount
                            , list[i].DeallinkApplicationID
                            , list[i].TotalRetailPrice
                            , list[i].TotalAccessoriesandLaborAmount
                            , list[i].TotalAddOnAmount
                            , list[i].TotalESPAmount
                            , list[i].TotalPPMAmount
                            , list[i].TotalAccidentHealthAmount
                            , list[i].TotalCreditLifeAmount
                            , list[i].TotalChargesandFeesAmount
                            , list[i].TotalVehicleTaxAmount
                            , list[i].TotalAccessoriesandLaborTaxAmount
                            , list[i].TotalSupplementalChargesTaxAmount
                            , list[i].TotalAccessoryFeesAmount
                            , list[i].TotalDocStampsAmount
                            , list[i].TotalTaxAmount
                            , list[i].TotalTradeInAllowanceAmount
                            , list[i].TotalTradeInPayoffAmount
                            , list[i].TotalTradeInActualCashValueAmount
                            , list[i].TotalDepositAmount
                            , list[i].TotalDownPaymentAmount
                            , list[i].TotalRebateAmount
                            , list[i].TotalFinancedAmount
                            , list[i].TotalDueAmount
                            , list[i].AddOnItemID
                            , list[i].AddOnItemName
                            , list[i].AddOnItemVendorID
                            , list[i].AddOnItemStatus
                            , list[i].AddOnItemUnitPriceAmount
                            , list[i].AddOnItemQuantity
                            , list[i].AddOnItemListPriceAmount
                            , list[i].AddOnItemDiscountAmount
                            , list[i].AddOnItemDiscountRate
                            , list[i].AddOnItemSalesTaxAmount
                            , list[i].ChargeID
                            , list[i].ChargeName
                            , list[i].ChargeVendorID
                            , list[i].ChargeCostAmount
                            , list[i].ChargeListPriceAmount
                            , list[i].ChargeSalesTaxAmount
                            , list[i].TradeInVIN
                            , list[i].TradeInMake
                            , list[i].TradeInModel
                            , list[i].TradeInModelYear
                            , list[i].TradeInMileage
                            , list[i].TradeInColor
                            , list[i].TradeInFactoryInstalledOptions
                            , list[i].TradeInStockNumber
                            , list[i].TradeInAllowanceAmount
                            , list[i].TradeInPayoffAmount
                            , list[i].TradeInActualCashValueAmount);
                    }
                    file.Close();
                }

                //  FTPUploadFile FTP = new FTPUploadFile();
                // FTP.FileUploadFTP(filepath, filename);

            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBClosedVehicleDealsListToCSV";
                logdata.base_message = "Set DBClosedVehicleDeals list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBCustomerListToCSV(List<Customer> list)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                string filename = Destination + ConfigurationManager.AppSettings["Customer"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
                {
                    file.WriteLine("CustomerID,CompanyName,FirstName,LastName,MiddleName,NamePrefix,NameSuffix,InActiveDateTime,Address1,Address2,City,State,PostalCode,PostalCodeSuffix,County,Country,EmailAddress,HomePhone,MobilePhone,WorkPhone,WorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactLetter,DoNotContactEmail,Gender,BirthDate,CustomerInceptionDate,LastPurchaseDate,CustomerType,VIN,Make,Model,ModelYear,Mileage,HOG,Loyalty");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36}"
                            , list[i].CustomerID
                            , list[i].CompanyName
                            , list[i].FirstName
                            , list[i].LastName
                            , list[i].MiddleName
                            , list[i].NamePrefix
                            , list[i].NameSuffix
                            , list[i].InActiveDateTime
                            , list[i].Address1
                            , list[i].Address2
                            , list[i].City
                            , list[i].State
                            , list[i].PostalCode
                            , list[i].PostalCodeSuffix
                            , list[i].County
                            , list[i].Country
                            , list[i].EmailAddress
                            , list[i].HomePhone
                            , list[i].MobilePhone
                            , list[i].WorkPhone
                            , list[i].WorkPhoneExt
                            , list[i].PreferredContact
                            , list[i].DoNotContactPhone
                            , list[i].DoNotContactLetter
                            , list[i].DoNotContactEmail
                            , list[i].Gender
                            , list[i].BirthDate
                            , list[i].CustomerInceptionDate
                            , list[i].LastPurchaseDate
                            , list[i].CustomerType
                            , list[i].VIN
                            , list[i].Make
                            , list[i].Model
                            , list[i].ModelYear
                            , list[i].Mileage
                            , list[i].HOG
                            , list[i].Loyalty);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBCustomerListToCSV";
                logdata.base_message = "Set DBCustomer list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBCustomerListToCSV(List<Customer> list, int count)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                string filepath = Destination + ConfigurationManager.AppSettings["Customer"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Customer"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];


                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                {
                    file.WriteLine("CustomerID,CompanyName,FirstName,LastName,MiddleName,NamePrefix,NameSuffix,InActiveDateTime,Address1,Address2,City,State,PostalCode,PostalCodeSuffix,County,Country,EmailAddress,HomePhone,MobilePhone,WorkPhone,WorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactLetter,DoNotContactEmail,Gender,BirthDate,CustomerInceptionDate,LastPurchaseDate,CustomerType,VIN,Make,Model,ModelYear,Mileage,HOG,Loyalty");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36}"
                            , list[i].CustomerID
                            , list[i].CompanyName
                            , list[i].FirstName
                            , list[i].LastName
                            , list[i].MiddleName
                            , list[i].NamePrefix
                            , list[i].NameSuffix
                            , list[i].InActiveDateTime
                            , list[i].Address1
                            , list[i].Address2
                            , list[i].City
                            , list[i].State
                            , list[i].PostalCode
                            , list[i].PostalCodeSuffix
                            , list[i].County
                            , list[i].Country
                            , list[i].EmailAddress
                            , list[i].HomePhone
                            , list[i].MobilePhone
                            , list[i].WorkPhone
                            , list[i].WorkPhoneExt
                            , list[i].PreferredContact
                            , list[i].DoNotContactPhone
                            , list[i].DoNotContactLetter
                            , list[i].DoNotContactEmail
                            , list[i].Gender
                            , list[i].BirthDate
                            , list[i].CustomerInceptionDate
                            , list[i].LastPurchaseDate
                            , list[i].CustomerType
                            , list[i].VIN
                            , list[i].Make
                            , list[i].Model
                            , list[i].ModelYear
                            , list[i].Mileage
                            , list[i].HOG
                            , list[i].Loyalty);
                    }
                    file.Close();
                }

                // FTPUploadFile FTP = new FTPUploadFile();
                // FTP.FileUploadFTP(filepath, filename);
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBCustomerListToCSV";
                logdata.base_message = "Set DBCustomer list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBInventoryListToCSV(List<Inventory> list)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                string filename = Destination + ConfigurationManager.AppSettings["Inventory"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
                {
                    file.WriteLine("VIN,Make,Model,ModelYear,Mileage,Color,EngineCylinderCount,EngineSize,FactoryInstalledOptions,PreOwnedFlag,StockNumber,KeyNumber,InventoryType,InventoryStatus,ExpectedDeliveryDate,deliveryDate,AvailableDate,IsServiceDate,Location,MSRP,BookVAlueAmount,SuggestedList,OriginalInvoiceAmount,OriginalInvoiceDateTime,OriginalInvoiceNumber");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24}"
                            , list[i].vin
                            , list[i].make
                            , list[i].model
                            , list[i].modelYear
                            , list[i].mileage
                            , list[i].color
                            , list[i].engineCylinderCount
                            , list[i].engineSize
                            , list[i].factoryInstalledOptions
                            , list[i].preOwnedFlag
                            , list[i].stockNumber
                            , list[i].keyNumber
                            , list[i].inventoryType
                            , list[i].inventoryStatus
                            , list[i].expectedDeliveryDate
                            , list[i].deliveryDate
                            , list[i].availableDate
                            , list[i].isServiceDate
                            , list[i].location
                            , list[i].MSRP
                            , list[i].bookVAlueAmount
                            , list[i].suggestedList
                            , list[i].originalInvoiceAmount
                            , list[i].originalInvoiceDateTime
                            , list[i].originalInvoiceNumber);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBInventoryListToCSV";
                logdata.base_message = "Set DBInventory list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBInventoryListToCSV(List<Inventory> list, int count)
        {
            try
            {
                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                string filepath = Destination + ConfigurationManager.AppSettings["Inventory"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Inventory"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                {
                    file.WriteLine("VIN,Make,Model,ModelYear,Mileage,Color,EngineCylinderCount,EngineSize,FactoryInstalledOptions,PreOwnedFlag,StockNumber,KeyNumber,InventoryType,InventoryStatus,ExpectedDeliveryDate,deliveryDate,AvailableDate,IsServiceDate,Location,MSRP,BookVAlueAmount,SuggestedList,OriginalInvoiceAmount,OriginalInvoiceDateTime,OriginalInvoiceNumber");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24}"
                           , list[i].vin
                            , list[i].make
                            , list[i].model
                            , list[i].modelYear
                            , list[i].mileage
                            , list[i].color
                            , list[i].engineCylinderCount
                            , list[i].engineSize
                            , list[i].factoryInstalledOptions
                            , list[i].preOwnedFlag
                            , list[i].stockNumber
                            , list[i].keyNumber
                            , list[i].inventoryType
                            , list[i].inventoryStatus
                            , list[i].expectedDeliveryDate
                            , list[i].deliveryDate
                            , list[i].availableDate
                            , list[i].isServiceDate
                            , list[i].location
                            , list[i].MSRP
                            , list[i].bookVAlueAmount
                            , list[i].suggestedList
                            , list[i].originalInvoiceAmount
                            , list[i].originalInvoiceDateTime
                            , list[i].originalInvoiceNumber);
                    }
                    file.Close();
                }

                // FTPUploadFile FTP = new FTPUploadFile();
                // FTP.FileUploadFTP(filepath, filename);
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBInventoryListToCSV";
                logdata.base_message = "Set DBInventory list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBInvoiceListToCSV(List<Invoice> list)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                // string filename = Destination + ConfigurationManager.AppSettings["Invoice"];
                string filepath = Destination2 + ConfigurationManager.AppSettings["Inventory"] + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Inventory"] + ConfigurationManager.AppSettings["extCSV"];

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                //  using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
                {
                    file.WriteLine("InvoiceID,TransactionType,PointofSalesOrderID,WorkOrderID,DealID,EComOrderNum,InvoiceDateTime,TotalAmount,TotalTenderedAmount,TotalShippingAmount,TotalSalesTaxAmount,ChangeGivenAmount,PaidByCashAmount,PaidByCheckAmount,PaidByCreditCardAmount,PaidByCustomerAccountAmount,PaidByGiftCertificateAmount,CustomerID,CustomerFirstName,CustomerLastName,CustomerMiddleName,CustomerCompanyName,CustomerPrefixCode,CustomerNameSuffix,Address1,Address2,CustomerCity,CustomerState,CustomerPostalCode,CustomerPostalCodeSuffix,CustomerCounty,CustomerCountry,CustomerEmailAddress,CustomerHomePhone,CustomerMobilePhone,CustomerWorkPhone,CustomerWorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactEmail,DoNotContactLetter,Salesperson_EmployeeCode,Salesperson_FirstName,Salesperson_LastName,LineItemID,LineItemName,LineItemVendorID,LineItemStatus,LineItemUnitPriceAmount,LineItemQuantity,LineItemListPriceAmount,LineItemDiscountAmount,LineItemDiscountRate,LineItemDeposit,LineItemSalesTaxAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54}"
                            , list[i].InvoiceID
                            , list[i].TransactionType
                            , list[i].PointofSalesOrderID
                            , list[i].WorkOrderID
                            , list[i].DealID, list[i].EComOrderNum
                            , list[i].InvoiceDateTime
                            , list[i].TotalAmount
                            , list[i].TotalTenderedAmount
                            , list[i].TotalShippingAmount
                            , list[i].TotalSalesTaxAmount
                            , list[i].ChangeGivenAmount
                            , list[i].PaidByCashAmount
                            , list[i].PaidByCheckAmount
                            , list[i].PaidByCreditCardAmount
                            , list[i].PaidByCustomerAccountAmount
                            , list[i].PaidByGiftCertificateAmount, list[i].CustomerID
                            , list[i].CustomerFirstName
                            , list[i].CustomerLastName
                            , list[i].CustomerMiddleName
                            , list[i].CustomerCompanyName
                            , list[i].CustomerPrefixCode
                            , list[i].CustomerNameSuffix
                            , list[i].Address1
                            , list[i].Address2
                            , list[i].CustomerCity
                            , list[i].CustomerState
                            , list[i].CustomerPostalCode
                            , list[i].CustomerPostalCodeSuffix
                            , list[i].CustomerCounty
                            , list[i].CustomerCountry
                            , list[i].CustomerEmailAddress
                            , list[i].CustomerHomePhone
                            , list[i].CustomerMobilePhone
                            , list[i].CustomerWorkPhone
                            , list[i].CustomerWorkPhoneExt
                            , list[i].PreferredContact
                            , list[i].DoNotContactPhone
                            , list[i].DoNotContactEmail
                            , list[i].DoNotContactLetter
                            , list[i].Salesperson_EmployeeCode
                            , list[i].Salesperson_FirstName
                            , list[i].Salesperson_LastName
                            , list[i].LineItemID
                            , list[i].LineItemName
                            , list[i].LineItemVendorID
                            , list[i].LineItemStatus
                            , list[i].LineItemUnitPriceAmount
                            , list[i].LineItemQuantity
                            , list[i].LineItemListPriceAmount
                            , list[i].LineItemDiscountAmount
                            , list[i].LineItemDiscountRate
                            , list[i].LineItemDeposit
                            , list[i].LineItemSalesTaxAmount);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBInvoiceListToCSV";
                logdata.base_message = "Set DBInvoice list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBInvoiceListToCSV(List<Invoice> list, int count)
        {
            try
            {


                if (!File.Exists(Destination))
                {
                    Directory.CreateDirectory(Destination);
                }
                string filepath = Destination + ConfigurationManager.AppSettings["Invoice"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Invoice"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
                {

                    file.WriteLine("InvoiceID,TransactionType,PointofSalesOrderID,WorkOrderID,DealID,EComOrderNum,InvoiceDateTime,TotalAmount,TotalTenderedAmount,TotalShippingAmount,TotalSalesTaxAmount,ChangeGivenAmount,PaidByCashAmount,PaidByCheckAmount,PaidByCreditCardAmount,PaidByCustomerAccountAmount,PaidByGiftCertificateAmount,CustomerID,CustomerFirstName,CustomerLastName,CustomerMiddleName,CustomerCompanyName,CustomerPrefixCode,CustomerNameSuffix,Address1,Address2,CustomerCity,CustomerState,CustomerPostalCode,CustomerPostalCodeSuffix,CustomerCounty,CustomerCountry,CustomerEmailAddress,CustomerHomePhone,CustomerMobilePhone,CustomerWorkPhone,CustomerWorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactEmail,DoNotContactLetter,Salesperson_EmployeeCode,Salesperson_FirstName,Salesperson_LastName,LineItemID,LineItemName,LineItemVendorID,LineItemStatus,LineItemUnitPriceAmount,LineItemQuantity,LineItemListPriceAmount,LineItemDiscountAmount,LineItemDiscountRate,LineItemDeposit,LineItemSalesTaxAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54}"
                            , list[i].InvoiceID
                            , list[i].TransactionType
                            , list[i].PointofSalesOrderID
                            , list[i].WorkOrderID
                            , list[i].DealID, list[i].EComOrderNum
                            , list[i].InvoiceDateTime
                            , list[i].TotalAmount
                            , list[i].TotalTenderedAmount
                            , list[i].TotalShippingAmount
                            , list[i].TotalSalesTaxAmount
                            , list[i].ChangeGivenAmount
                            , list[i].PaidByCashAmount
                            , list[i].PaidByCheckAmount
                            , list[i].PaidByCreditCardAmount
                            , list[i].PaidByCustomerAccountAmount
                            , list[i].PaidByGiftCertificateAmount, list[i].CustomerID
                            , list[i].CustomerFirstName
                            , list[i].CustomerLastName
                            , list[i].CustomerMiddleName
                            , list[i].CustomerCompanyName
                            , list[i].CustomerPrefixCode
                            , list[i].CustomerNameSuffix
                            , list[i].Address1
                            , list[i].Address2
                            , list[i].CustomerCity
                            , list[i].CustomerState
                            , list[i].CustomerPostalCode
                            , list[i].CustomerPostalCodeSuffix
                            , list[i].CustomerCounty
                            , list[i].CustomerCountry
                            , list[i].CustomerEmailAddress
                            , list[i].CustomerHomePhone
                            , list[i].CustomerMobilePhone
                            , list[i].CustomerWorkPhone
                            , list[i].CustomerWorkPhoneExt
                            , list[i].PreferredContact
                            , list[i].DoNotContactPhone
                            , list[i].DoNotContactEmail
                            , list[i].DoNotContactLetter
                            , list[i].Salesperson_EmployeeCode
                            , list[i].Salesperson_FirstName
                            , list[i].Salesperson_LastName
                            , list[i].LineItemID
                            , list[i].LineItemName
                            , list[i].LineItemVendorID
                            , list[i].LineItemStatus
                            , list[i].LineItemUnitPriceAmount
                            , list[i].LineItemQuantity
                            , list[i].LineItemListPriceAmount
                            , list[i].LineItemDiscountAmount
                            , list[i].LineItemDiscountRate
                            , list[i].LineItemDeposit
                            , list[i].LineItemSalesTaxAmount);
                    }


                    file.Close();
                }

                // FTPUploadFile FTP = new FTPUploadFile();
                // FTP.FileUploadFTP(filepath, filename);
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBInvoiceListToCSV";
                logdata.base_message = "Set DBInvoice list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }

        public void SetDBBilledWorkOrderListToCSV(List<BilledWorkOrder> list, int count, int dealerID)
        {
            try
            {
                if (!File.Exists(ConfigurationManager.AppSettings["BilledWorkOrder_PATH"]))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["BilledWorkOrder_PATH"]);
                }
                string filepath1 = ConfigurationManager.AppSettings["BilledWorkOrder_PATH"] + ConfigurationManager.AppSettings["BilledWorkOrder"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["BilledWorkOrder"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath1))
                {
                    file.WriteLine("WorkOrderID,Type,RequestBy,AppointmentDateTime,ArrivalDateTime,InMileage,PromisedCompletionDateTime,WorkStage,StorageRate,FreeStorageDays,StorageDays,CompletedDateTime,OutMileage,CustomerID,FirstName,LastName,MiddleName,CustomerCompanyName,NamePrefix,NameSuffix,Address1,Address2,City,State,PostalCode,PostalCodeSuffix,County,Country,EmailAddress,HomePhone,MobilePhone,WorkPhone,WorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactEmail,DoNotContactLetter,VIN,Make,Model,ModelYear,StockNumber,ServiceWriter_EmployeeCode,ServiceWriter_FirstName,ServiceWriter_LastName,LaborRateAmount,TotalNonRetailPartsAmount,TotalNonRetailLaborAmount,TotalPartsAmount,TotalLaborAmount,TotalContractLaborAmount,TotalESPDeductibleAmount,TotalShopSuppliesAmount,TotalStorageAmount,TotalTaxesFeesAmount,TotalWorkOrderAmount,TotalDepositsApplied,TotalDueAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57}"
                            , list[i].WorkOrderID.ToString()
                            , list[i].Type
                            , list[i].RequestBy.ToString()
                            , list[i].AppointmentDateTime.ToString()
                            , list[i].ArrivalDateTime.ToString()
                            , list[i].InMileage.ToString()
                            , list[i].PromisedCompletionDateTime.ToString()
                            , list[i].WorkStage.ToString()
                            , list[i].StorageRate.ToString()
                            , list[i].FreeStorageDays.ToString()
                            , list[i].StorageDays.ToString()
                            , list[i].CompletedDateTime.ToString()
                            , list[i].OutMileage.ToString()
                            , list[i].CustomerID.ToString()
                            , list[i].FirstName.ToString()
                            , list[i].LastName.ToString()
                            , list[i].MiddleName.ToString()
                            , list[i].CustomerCompanyName.ToString()
                            , list[i].NamePrefix.ToString()
                            , list[i].NameSuffix.ToString()
                            , list[i].Address1.ToString()
                            , list[i].Address2.ToString()
                            , list[i].City.ToString()
                            , list[i].State.ToString()
                            , list[i].PostalCode.ToString()
                            , list[i].PostalCodeSuffix.ToString()
                            , list[i].County.ToString()
                            , list[i].Country.ToString()
                            , list[i].EmailAddress.ToString()
                            , list[i].HomePhone.ToString()
                            , list[i].MobilePhone.ToString()
                            , list[i].WorkPhone.ToString()
                            , list[i].WorkPhoneExt.ToString()
                            , list[i].PreferredContact.ToString()
                            , list[i].DoNotContactPhone.ToString()
                            , list[i].DoNotContactEmail.ToString()
                            , list[i].DoNotContactLetter.ToString()
                            , list[i].VIN.ToString()
                            , list[i].Make.ToString()
                            , list[i].Model.ToString()
                            , list[i].ModelYear.ToString()
                            , list[i].StockNumber.ToString()
                            , list[i].ServiceWriter_EmployeeCode.ToString()
                            , list[i].ServiceWriter_FirstName.ToString()
                            , list[i].ServiceWriter_LastName.ToString()
                            , list[i].LaborRateAmount.ToString()
                            , list[i].TotalNonRetailPartsAmount.ToString()
                            , list[i].TotalNonRetailLaborAmount.ToString()
                            , list[i].TotalPartsAmount.ToString()
                            , list[i].TotalLaborAmount.ToString()
                            , list[i].TotalContractLaborAmount.ToString()
                            , list[i].TotalESPDeductibleAmount.ToString()
                            , list[i].TotalShopSuppliesAmount.ToString()
                            , list[i].TotalStorageAmount.ToString()
                            , list[i].TotalTaxesFeesAmount.ToString()
                            , list[i].TotalWorkOrderAmount.ToString()
                            , list[i].TotalDepositsApplied.ToString()
                            , list[i].TotalDueAmount.ToString());

                    }
                    file.Close();
                }


                //  FTPUploadFile FTP = new FTPUploadFile();
                //  FTP.FileUploadFTP(filepath, filename, dealerID);

            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBBilledWorkOrderListToCSV";
                logdata.base_message = "Set DBBilledWorkOrder list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBClosedVehicleDealsListToCSV(List<ClosedVehicleDeals> list, int count, int dealerID)
        {
            try
            {


                if (!File.Exists(ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"]))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"]);
                }
                string filepath1 = ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"] + ConfigurationManager.AppSettings["ClosedVehicleDeals"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["ClosedVehicleDeals"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath1))
                {
                    file.WriteLine("DealID,DealStatus,CreateDateTime,ContractDateTime,ClosedDateTime,DeliveryDateTime,InvoiceDateTime,InactiveDateTime,VIN,Make,Model,ModelYear,Mileage,Color,FactoryInstalledOptions,PreOwnedFlag,StockNumber,BuyerCustomerID,BuyerFirstName,BuyerLastName,BuyerMiddleName,BuyerCompanyName,BuyerNamePrefix,BuyerNameSuffix,BuyerAddress1,BuyerAddress2,BuyerCity,BuyerState,BuyerPostalCode,BuyerPostalCodeSuffix,BuyerCounty,BuyerCountry,BuyerEmailAddress,BuyerHomePhone,BuyerMobilePhone,BuyerWorkPhone,BuyerWorkPhoneExt,BuyerPreferredContact,BuyerDoNotContactPhone,BuyerDoNotContactEmail,BuyerDoNotContactLetter,CoBuyerCustomerID,CoBuyerFirstName,CoBuyerLastName,CoBuyerMiddleName,CoBuyerCompanyName,CoBuyerNamePrefix,CoBuyerNameSuffix,CoBuyerAddress1,CoBuyerAddress2,CoBuyerCity,CoBuyerState,CoBuyerPostalCode,CoBuyerPostalCodeSuffix,CoBuyerCounty,CoBuyerCountry,CoBuyerEmailAddress,CoBuyerHomePhone,CoBuyerMobilePhone,CoBuyerWorkPhone,CoBuyerWorkPhoneExt,CoBuyerPreferredContact,CoBuyerDoNotContactPhone,CoBuyerDoNotContactEmail,CoBuyerDoNotContactLetter,VehicleSalesperson1_EmployeeCode,VehicleSalesperson1_FirstName,VehicleSalesperson1_LastName,VehicleSalesperson2_EmployeeCode,VehicleSalesperson2_FirstName,VehicleSalesperson2_LastName,FinanceInsuranceSalesperson1_EmployeeCode,FinanceInsuranceSalesperson1_FirstName,FinanceInsuranceSalesperson1_LastName,FinanceInsuranceSalesperson2_EmployeeCode,FinanceInsuranceSalesperson2_FirstName,FinanceInsuranceSalesperson2_LastName,SalesManager_EmployeeCode,SalesManager_FirstName,SalesManager_LastName,Lienholder,LoanAPR,LoanTerm,LoanFirstPayDate,LoanPaymentsPerYear,LoanPaymentAmount,DeallinkApplicationID,TotalRetailPrice,TotalAccessoriesandLaborAmount,TotalAddOnAmount,TotalESPAmount,TotalPPMAmount,TotalAccidentHealthAmount,TotalCreditLifeAmount,TotalChargesandFeesAmount,TotalVehicleTaxAmount,TotalAccessoriesandLaborTaxAmount,TotalSupplementalChargesTaxAmount,TotalAccessoryFeesAmount,TotalDocStampsAmount,TotalTaxAmount,TotalTradeInAllowanceAmount,TotalTradeInPayoffAmount,TotalTradeInActualCashValueAmount,TotalDepositAmount,TotalDownPaymentAmount,TotalRebateAmount,TotalFinancedAmount,TotalDueAmount,AddOnItemID,AddOnItemName,AddOnItemVendorID,AddOnItemStatus,AddOnItemUnitPriceAmount,AddOnItemQuantity,AddOnItemListPriceAmount,AddOnItemDiscountAmount,AddOnItemDiscountRate,AddOnItemSalesTaxAmount,ChargeID,ChargeName,ChargeVendorID,ChargeCostAmount,ChargeListPriceAmount,ChargeSalesTaxAmount,TradeInVIN,TradeInMake,TradeInModel,TradeInModelYear,TradeInMileage,TradeInColor,TradeInFactoryInstalledOptions,TradeInStockNumber,TradeInAllowanceAmount,TradeInPayoffAmount,TradeInActualCashValueAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54},{55},{56},{57},{58},{59},{60},{61},{62},{63},{64},{65},{66},{67},{68},{69},{70},{71},{72},{73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94},{95},{96},{97},{98},{99},{100},{101},{102},{103},{104},{105},{106},{107},{108},{109},{110},{111},{112},{113},{114},{115},{116},{117},{118},{119},{120},{121},{122},{123},{124},{125},{126},{127},{128},{129},{130},{131},{132},{133},{134},{135}"
                            , list[i].DealID
                            , list[i].DealStatus
                            , list[i].CreateDAteTime
                            , list[i].ContractDateTime
                            , list[i].ClosedDateTime
                            , list[i].DeliveryDateTime
                            , list[i].InvoiceDateTime
                            , list[i].InactiveDateTime
                            , list[i].VIN
                            , list[i].Make
                            , list[i].Model
                            , list[i].ModelYear
                            , list[i].Mileage
                            , list[i].Color
                            , list[i].FactoryInstalledOptions
                            , list[i].PreOwnedFlag
                            , list[i].StockNumber
                            , list[i].BuyerCustomerID
                            , list[i].BuyerFirstName
                            , list[i].BuyerLastName
                            , list[i].BuyerMiddleName
                            , list[i].BuyerCompanyName
                            , list[i].BuyerNamePrefix
                            , list[i].BuyerNameSuffix
                            , list[i].BuyerAddress1
                            , list[i].BuyerAddress2
                            , list[i].BuyerCity
                            , list[i].BuyerState
                            , list[i].BuyerPostalCode
                            , list[i].BuyerPostalCodeSuffix
                            , list[i].BuyerCounty
                            , list[i].BuyerCountry
                            , list[i].BuyerEmailAddress
                            , list[i].BuyerHomePhone
                            , list[i].BuyerMobilePhone
                            , list[i].BuyerWorkPhone
                            , list[i].BuyerWorkPhoneExt
                            , list[i].BuyerPreferredContact
                            , list[i].BuyerDoNotContactPhone
                            , list[i].BuyerDoNotContactEmail
                            , list[i].BuyerDoNotContactLetter
                            , list[i].CoBuyerCustomerID
                            , list[i].CoBuyerFirstName
                            , list[i].CoBuyerLastName
                            , list[i].CoBuyerMiddleName
                            , list[i].CoBuyerCompanyName
                            , list[i].CoBuyerNamePrefix
                            , list[i].CoBuyerNameSuffix
                            , list[i].CoBuyerAddress1
                            , list[i].CoBuyerAddress2
                            , list[i].CoBuyerCity
                            , list[i].CoBuyerState
                            , list[i].CoBuyerPostalCode
                            , list[i].CoBuyerPostalCodeSuffix
                            , list[i].CoBuyerCounty
                            , list[i].CoBuyerCountry
                            , list[i].CoBuyerEmailAddress
                            , list[i].CoBuyerHomePhone
                            , list[i].CoBuyerMobilePhone
                            , list[i].CoBuyerWorkPhone
                            , list[i].CoBuyerWorkPhoneExt
                            , list[i].CoBuyerPreferredContact
                            , list[i].CoBuyerDoNotContactPhone
                            , list[i].CoBuyerDoNotContactEmail
                            , list[i].CoBuyerDoNotContactLetter
                            , list[i].VehicleSalesperson1_EmployeeCode
                            , list[i].VehicleSalesperson1_FirstName
                            , list[i].VehicleSalesperson1_LastName
                            , list[i].VehicleSalesperson2_EmployeeCode
                            , list[i].VehicleSalesperson2_FirstName
                            , list[i].VehicleSalesperson2_LastName
                            , list[i].FinanceInsuranceSalesperson1_EmployeeCode
                            , list[i].FinanceInsuranceSalesperson1_FirstName
                            , list[i].FinanceInsuranceSalesperson1_LastName
                            , list[i].FinanceInsuranceSalesperson2_EmployeeCode
                            , list[i].FinanceInsuranceSalesperson2_FirstName
                            , list[i].FinanceInsuranceSalesperson2_LastName
                            , list[i].SalesManager_EmployeeCode
                            , list[i].SalesManager_FirstName
                            , list[i].SalesManager_LastName
                            , list[i].Lienholder
                            , list[i].LoanAPR
                            , list[i].LoanTerm
                            , list[i].LoanFirstPayDate
                            , list[i].LoanPaymentsPerYear
                            , list[i].LoanPaymentAmount
                            , list[i].DeallinkApplicationID
                            , list[i].TotalRetailPrice
                            , list[i].TotalAccessoriesandLaborAmount
                            , list[i].TotalAddOnAmount
                            , list[i].TotalESPAmount
                            , list[i].TotalPPMAmount
                            , list[i].TotalAccidentHealthAmount
                            , list[i].TotalCreditLifeAmount
                            , list[i].TotalChargesandFeesAmount
                            , list[i].TotalVehicleTaxAmount
                            , list[i].TotalAccessoriesandLaborTaxAmount
                            , list[i].TotalSupplementalChargesTaxAmount
                            , list[i].TotalAccessoryFeesAmount
                            , list[i].TotalDocStampsAmount
                            , list[i].TotalTaxAmount
                            , list[i].TotalTradeInAllowanceAmount
                            , list[i].TotalTradeInPayoffAmount
                            , list[i].TotalTradeInActualCashValueAmount
                            , list[i].TotalDepositAmount
                            , list[i].TotalDownPaymentAmount
                            , list[i].TotalRebateAmount
                            , list[i].TotalFinancedAmount
                            , list[i].TotalDueAmount
                            , list[i].AddOnItemID
                            , list[i].AddOnItemName
                            , list[i].AddOnItemVendorID
                            , list[i].AddOnItemStatus
                            , list[i].AddOnItemUnitPriceAmount
                            , list[i].AddOnItemQuantity
                            , list[i].AddOnItemListPriceAmount
                            , list[i].AddOnItemDiscountAmount
                            , list[i].AddOnItemDiscountRate
                            , list[i].AddOnItemSalesTaxAmount
                            , list[i].ChargeID
                            , list[i].ChargeName
                            , list[i].ChargeVendorID
                            , list[i].ChargeCostAmount
                            , list[i].ChargeListPriceAmount
                            , list[i].ChargeSalesTaxAmount
                            , list[i].TradeInVIN
                            , list[i].TradeInMake
                            , list[i].TradeInModel
                            , list[i].TradeInModelYear
                            , list[i].TradeInMileage
                            , list[i].TradeInColor
                            , list[i].TradeInFactoryInstalledOptions
                            , list[i].TradeInStockNumber
                            , list[i].TradeInAllowanceAmount
                            , list[i].TradeInPayoffAmount
                            , list[i].TradeInActualCashValueAmount);
                    }
                    file.Close();
                }


                // FTPUploadFile FTP = new FTPUploadFile();
                //  FTP.FileUploadFTP(filepath, filename, dealerID);

            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBClosedVehicleDealsListToCSV";
                logdata.base_message = "Set DBClosedVehicleDeals list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBCustomerListToCSV(List<Customer> list, int count, int dealerID)
        {
            try
            {


                if (!File.Exists(ConfigurationManager.AppSettings["Customer_PATH"]))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["Customer_PATH"]);
                }
                string filepath1 = ConfigurationManager.AppSettings["Customer_PATH"] + ConfigurationManager.AppSettings["Customer"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Customer"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];


                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath1))
                {
                    file.WriteLine("CustomerID,CompanyName,FirstName,LastName,MiddleName,NamePrefix,NameSuffix,InActiveDateTime,Address1,Address2,City,State,PostalCode,PostalCodeSuffix,County,Country,EmailAddress,HomePhone,MobilePhone,WorkPhone,WorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactLetter,DoNotContactEmail,Gender,BirthDate,CustomerInceptionDate,LastPurchaseDate,CustomerType,VIN,Make,Model,ModelYear,Mileage,HOG,Loyalty");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36}"
                            , list[i].CustomerID
                            , list[i].CompanyName
                            , list[i].FirstName
                            , list[i].LastName
                            , list[i].MiddleName
                            , list[i].NamePrefix
                            , list[i].NameSuffix
                            , list[i].InActiveDateTime
                            , list[i].Address1
                            , list[i].Address2
                            , list[i].City
                            , list[i].State
                            , list[i].PostalCode
                            , list[i].PostalCodeSuffix
                            , list[i].County
                            , list[i].Country
                            , list[i].EmailAddress
                            , list[i].HomePhone
                            , list[i].MobilePhone
                            , list[i].WorkPhone
                            , list[i].WorkPhoneExt
                            , list[i].PreferredContact
                            , list[i].DoNotContactPhone
                            , list[i].DoNotContactLetter
                            , list[i].DoNotContactEmail
                            , list[i].Gender
                            , list[i].BirthDate
                            , list[i].CustomerInceptionDate
                            , list[i].LastPurchaseDate
                            , list[i].CustomerType
                            , list[i].VIN
                            , list[i].Make
                            , list[i].Model
                            , list[i].ModelYear
                            , list[i].Mileage
                            , list[i].HOG
                            , list[i].Loyalty);
                    }
                    file.Close();
                }

                // FTPUploadFile FTP = new FTPUploadFile();
                //  FTP.FileUploadFTP(filepath, filename, dealerID);
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBCustomerListToCSV";
                logdata.base_message = "Set DBCustomer list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBInventoryListToCSV(List<Inventory> list, int count, int dealerID)
        {
            try
            {
                if (!File.Exists(ConfigurationManager.AppSettings["Inventory_PATH"]))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["Inventory_PATH"]);
                }
                string filepath1 = ConfigurationManager.AppSettings["Inventory_PATH"] + ConfigurationManager.AppSettings["Inventory"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Inventory"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath1))
                {
                    file.WriteLine("VIN,Make,Model,ModelYear,Mileage,Color,EngineCylinderCount,EngineSize,FactoryInstalledOptions,PreOwnedFlag,StockNumber,KeyNumber,InventoryType,InventoryStatus,ExpectedDeliveryDate,DeliveryDate,AvailableDate,IsServiceDate,Location,MSRP,BookVAlueAmount,SuggestedList,OriginalInvoiceAmount,OriginalInvoiceDateTime,OriginalInvoiceNumber");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24}"
                            , list[i].vin
                            , list[i].make
                            , list[i].model
                            , list[i].modelYear
                            , list[i].mileage
                            , list[i].color
                            , list[i].engineCylinderCount
                            , list[i].engineSize
                            , list[i].factoryInstalledOptions
                            , list[i].preOwnedFlag
                            , list[i].stockNumber
                            , list[i].keyNumber
                            , list[i].inventoryType
                            , list[i].inventoryStatus
                            , list[i].expectedDeliveryDate
                            , list[i].deliveryDate
                            , list[i].availableDate
                            , list[i].isServiceDate
                            , list[i].location
                            , list[i].MSRP
                            , list[i].bookVAlueAmount
                            , list[i].suggestedList
                            , list[i].originalInvoiceAmount
                            , list[i].originalInvoiceDateTime
                            , list[i].originalInvoiceNumber);
                    }
                    file.Close();
                }

                //  FTPUploadFile FTP = new FTPUploadFile();
                //  FTP.FileUploadFTP(filepath, filename, dealerID);
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBInventoryListToCSV";
                logdata.base_message = "Set DBInventory list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public void SetDBInvoiceListToCSV(List<Invoice> list, int count, int dealerID)
        {
            try
            {


                if (!File.Exists(ConfigurationManager.AppSettings["Invoice_PATH"]))
                {
                    Directory.CreateDirectory(ConfigurationManager.AppSettings["Invoice_PATH"]);
                }
                string filepath1 = ConfigurationManager.AppSettings["Invoice_PATH"] + ConfigurationManager.AppSettings["Invoice"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];
                string filename = ConfigurationManager.AppSettings["Invoice"] + "_" + count + ConfigurationManager.AppSettings["extCSV"];

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath1))
                {

                    file.WriteLine("InvoiceID,TransactionType,PointofSalesOrderID,WorkOrderID,DealID,EComOrderNum,InvoiceDateTime,TotalAmount,TotalTenderedAmount,TotalShippingAmount,TotalSalesTaxAmount,ChangeGivenAmount,PaidByCashAmount,PaidByCheckAmount,PaidByCreditCardAmount,PaidByCustomerAccountAmount,PaidByGiftCertificateAmount,CustomerID,CustomerFirstName,CustomerLastName,CustomerMiddleName,CustomerCompanyName,CustomerPrefixCode,CustomerNameSuffix,Address1,Address2,CustomerCity,CustomerState,CustomerPostalCode,CustomerPostalCodeSuffix,CustomerCounty,CustomerCountry,CustomerEmailAddress,CustomerHomePhone,CustomerMobilePhone,CustomerWorkPhone,CustomerWorkPhoneExt,PreferredContact,DoNotContactPhone,DoNotContactEmail,DoNotContactLetter,Salesperson_EmployeeCode,Salesperson_FirstName,Salesperson_LastName,LineItemID,LineItemName,LineItemVendorID,LineItemStatus,LineItemUnitPriceAmount,LineItemQuantity,LineItemListPriceAmount,LineItemDiscountAmount,LineItemDiscountRate,LineItemDeposit,LineItemSalesTaxAmount");
                    for (int i = 0; i < list.Count; i++)
                    {
                        file.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},{52},{53},{54}"
                            , list[i].InvoiceID
                            , list[i].TransactionType
                            , list[i].PointofSalesOrderID
                            , list[i].WorkOrderID
                            , list[i].DealID
                            , list[i].EComOrderNum
                            , list[i].InvoiceDateTime
                            , list[i].TotalAmount
                            , list[i].TotalTenderedAmount
                            , list[i].TotalShippingAmount
                            , list[i].TotalSalesTaxAmount
                            , list[i].ChangeGivenAmount
                            , list[i].PaidByCashAmount
                            , list[i].PaidByCheckAmount
                            , list[i].PaidByCreditCardAmount
                            , list[i].PaidByCustomerAccountAmount
                            , list[i].PaidByGiftCertificateAmount
                            , list[i].CustomerID
                            , list[i].CustomerFirstName
                            , list[i].CustomerLastName
                            , list[i].CustomerMiddleName
                            , list[i].CustomerCompanyName
                            , list[i].CustomerPrefixCode
                            , list[i].CustomerNameSuffix
                            , list[i].Address1
                            , list[i].Address2
                            , list[i].CustomerCity
                            , list[i].CustomerState
                            , list[i].CustomerPostalCode
                            , list[i].CustomerPostalCodeSuffix
                            , list[i].CustomerCounty
                            , list[i].CustomerCountry
                            , list[i].CustomerEmailAddress
                            , list[i].CustomerHomePhone
                            , list[i].CustomerMobilePhone
                            , list[i].CustomerWorkPhone
                            , list[i].CustomerWorkPhoneExt
                            , list[i].PreferredContact
                            , list[i].DoNotContactPhone
                            , list[i].DoNotContactEmail
                            , list[i].DoNotContactLetter
                            , list[i].Salesperson_EmployeeCode
                            , list[i].Salesperson_FirstName
                            , list[i].Salesperson_LastName
                            , list[i].LineItemID
                            , list[i].LineItemName
                            , list[i].LineItemVendorID
                            , list[i].LineItemStatus
                            , list[i].LineItemUnitPriceAmount
                            , list[i].LineItemQuantity
                            , list[i].LineItemListPriceAmount
                            , list[i].LineItemDiscountAmount
                            , list[i].LineItemDiscountRate
                            , list[i].LineItemDeposit
                            , list[i].LineItemSalesTaxAmount);
                    }


                    file.Close();
                }



                // FTPUploadFile FTP = new FTPUploadFile();
                // FTP.FileUploadFTP(filepath, filename, dealerID);
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SetDBInvoiceListToCSV";
                logdata.base_message = "Set DBInvoice list to CSV";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }

        public UserCredential GetUserDataFromFile()
        {

            UserCredential user = new UserCredential();
            string filename = Destination + ConfigurationManager.AppSettings["UserInfo"];
            if (File.Exists(filename))
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    string prevData = sr.ReadToEnd();
                    if (prevData != "")
                    {
                        prevData = Helper.Crypto.Decrypt(prevData);
                    }

                    user = JsonConvert.DeserializeObject<UserCredential>(prevData);


                }
            }
            return user;
        }
        public string GetUserDataFromFilePath()
        {
            string filename = Destination + ConfigurationManager.AppSettings["UserInfo"];
            return filename;
        }
        public void SetUserResultToFile(string InputStr)
        {

            if (File.Exists(Destination1))
            {
                string filename = Destination1 + ConfigurationManager.AppSettings["ErrorLog"];
                using (StreamReader sr = new StreamReader(filename))
                {
                    string prevData = sr.ReadToEnd();
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append(prevData);
                        sb.Append(InputStr);
                        sb.Append("LogTime:");
                        sb.Append(DateTime.Now.ToString());
                        file.WriteLine(sb.ToString());
                        file.Close();
                        file.Dispose();
                    }
                }
            }
            else
            {
                Directory.CreateDirectory(Destination1);
                string filename = Destination1 + ConfigurationManager.AppSettings["ErrorLog"];
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(InputStr);
                    sb.Append("LogTime:");
                    sb.Append(DateTime.Now.ToString());
                    file.WriteLine(sb.ToString());
                    file.Close();
                    file.Dispose();
                }
            }

        }
        public void SetErrorLog(string InputStr)
        {
            Directory.CreateDirectory(Destination1);
            string filename = Destination1 + "\\" + "ErrorLog_" + DateTime.Now.Ticks.ToString() + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(InputStr);
                sb.Append(Environment.NewLine);
                sb.Append("LogTime:");
                sb.Append(DateTime.Now.ToString());
                file.WriteLine(sb.ToString());
                file.Close();
                file.Dispose();
            }
        }
        public void SetLog(string InputStr)
        {
            Directory.CreateDirectory(Destination1);
            string filename = Destination1 + "\\" + "Log_" + DateTime.Now.Ticks.ToString() + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(InputStr);
                sb.Append(Environment.NewLine);
                sb.Append("LogTime:");
                sb.Append(DateTime.Now.ToString());
                file.WriteLine(sb.ToString());
                file.Close();
                file.Dispose();
            }
        }
        public async Task WriteToLog(Exception ex)
        {
            //log the exception
            Common.Model.LogData exception = new Common.Model.LogData();
            exception.LogType = Common.Helper.LogType.Exception;
            exception.LogTime = DateTime.Now;
            exception.ExceptionInformation = ex;
            if (ex is Common.CustomException.APIException)
            {
                Common.CustomException.APIException apiex = (Common.CustomException.APIException)ex;
                exception.DataToLog.Add("Code", apiex.ErrorCode);
            }
            //write into log
            WriteLogOperation log = new WriteLogOperation(new FileLog());
            await log.Add(exception);
        }
        public async Task WriteToDBLog(Exception ex)
        {
            //log the exception
            Common.Model.LogData exception = new Common.Model.LogData();
            exception.LogType = Common.Helper.LogType.Exception;
            exception.LogTime = DateTime.Now;
            exception.ExceptionInformation = ex;
            if (ex is Common.CustomException.APIException)
            {
                Common.CustomException.APIException apiex = (Common.CustomException.APIException)ex;
                exception.DataToLog.Add("Code", apiex.ErrorCode);
            }
            //write into log
            WriteLogOperation log = new WriteLogOperation(new DBLog());
            await log.Add(exception);
        }
        public void DeleteUserDataFromFile()
        {
            string filename = Destination + ConfigurationManager.AppSettings["UserInfo"];
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }
    }
}
