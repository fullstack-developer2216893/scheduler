﻿using System;

namespace Common.Model
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string NamePrefix { get; set; }
        public string NameSuffix { get; set; }
        public DateTime InActiveDateTime { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodeSuffix { get; set; } 
        public string County { get; set; }
        //Newly Added
        public string Country { get; set; }
        public string EmailAddress { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string PreferredContact { get; set; }
        public bool DoNotContactPhone { get; set; }
        public bool DoNotContactLetter { get; set; }
        public bool DoNotContactEmail { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public DateTime CustomerInceptionDate { get; set; }
        public DateTime LastPurchaseDate { get; set; }
        public string CustomerType { get; set; }
        public string VIN { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }   
        public int ModelYear { get; set; }
        public int Mileage { get; set; }
        public string HOG { get; set; }
        public string Loyalty { get; set; }
    }
}
