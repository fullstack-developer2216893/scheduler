﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    public class CRMLogData
    {
        public string log_type { get; set; }
        public string method_name { get; set; }
        public string base_message { get; set; }
        public string exception_message { get; set; }
        public string parameter_name { get; set; }
        public string stack_trace { get; set; }
        //public DateTime log_time { get; set; }
        //public DateTime created { get; set; } 
        //public DateTime modified { get; set; }
        public int? id { get; set; }

    }
    
}
