﻿using System;

namespace Common.Model
{
    public class Invoice
    {
        public int InvoiceID { get; set; }
        public string TransactionType { get; set; }
        public int PointofSalesOrderID { get; set; }
        public string WorkOrderID { get; set; }
        public int DealID { get; set; }
        public int EComOrderNum { get; set; }
        public DateTime InvoiceDateTime { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalTenderedAmount { get; set; }
        public decimal TotalShippingAmount { get; set; }
        public decimal TotalSalesTaxAmount { get; set; }
        public decimal ChangeGivenAmount { get; set; }
        public decimal PaidByCashAmount { get; set; }
        public decimal PaidByCheckAmount { get; set; } 
        public decimal PaidByCreditCardAmount { get; set; }
        public decimal PaidByCustomerAccountAmount { get; set; }
        public decimal PaidByGiftCertificateAmount { get; set; }
        public int CustomerID { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerCompanyName { get; set; }
        public string CustomerPrefixCode { get; set; }
        public string CustomerNameSuffix { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustomerPostalCode { get; set; }
        public string CustomerPostalCodeSuffix { get; set; }
        public string CustomerCounty { get; set; }
        public string CustomerCountry { get; set; }
        public string CustomerEmailAddress { get; set; }
        public string CustomerHomePhone { get; set; }
        public string CustomerMobilePhone { get; set; }
        public string CustomerWorkPhone { get; set; }
        public string CustomerWorkPhoneExt { get; set; }
        public string PreferredContact { get; set; }
        public string DoNotContactPhone { get; set; }
        public string DoNotContactEmail { get; set; }
        public string DoNotContactLetter { get; set; }
        public string Salesperson_EmployeeCode { get; set; }
        public string Salesperson_FirstName { get; set; }
        public string Salesperson_LastName { get; set; }
        public string LineItemID { get; set; }
        public string LineItemName { get; set; }
        public string LineItemVendorID { get; set; }
        public string LineItemStatus { get; set; }
        public decimal LineItemUnitPriceAmount { get; set; }
        public decimal LineItemQuantity { get; set; }
        public decimal LineItemListPriceAmount { get; set; }
        public decimal LineItemDiscountAmount { get; set; }
        public decimal LineItemDiscountRate { get; set; }
        public decimal LineItemDeposit { get; set; }
        public decimal LineItemSalesTaxAmount { get; set; }
    }
}
