﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
  public  class CRMLogDataResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public CRMLogData data { get; set; }

        public CRMLogDataResponse()
        {
            data = new CRMLogData(); 
        }
    }
   
}
