﻿using System;

namespace Common.Model
{
    public class Inventory
    {
        public string vin { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public int modelYear { get; set; }
        public int mileage { get; set; }
        public string color { get; set; }
        public int engineCylinderCount { get; set; }
        public int engineSize { get; set; }
        public string factoryInstalledOptions { get; set; }
        public string preOwnedFlag { get; set; }
        public string stockNumber { get; set; }
        public string keyNumber { get; set; }
        public string inventoryType { get; set; }
        public string inventoryStatus { get; set; } 
        public DateTime expectedDeliveryDate { get; set; }
        public DateTime deliveryDate { get; set; }
        public DateTime availableDate { get; set; }
        public DateTime isServiceDate { get; set; }
        public string location { get; set; }
        public decimal MSRP { get; set; }
        public decimal bookVAlueAmount { get; set; }
        public decimal suggestedList { get; set; }
        public decimal originalInvoiceAmount { get; set; }
        public DateTime originalInvoiceDateTime { get; set; }
        public string originalInvoiceNumber { get; set; }
    }
}
