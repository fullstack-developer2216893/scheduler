﻿using System;
using System.Collections.Generic;

namespace Common.Model
{
    public class LogData
    {
        public string LogType { get; set; }
        public Dictionary<string, string> DataToLog { get; set; }
        public Exception ExceptionInformation { get; set; }
        public DateTime LogTime { get; set; }
        public string Message { get; set; }
        //public Object SpecialInformation { get; set; }

        public LogData()
        {
            DataToLog = new Dictionary<string, string>(); 
        }
    }
}
