﻿using System;

namespace Common.Model
{
    public class BilledWorkOrder
    {
        public string WorkOrderID { get; set; } 
        public int Type { get; set; }
        public string RequestBy { get; set; }
        public DateTime AppointmentDateTime { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public int InMileage { get; set; }
        public DateTime PromisedCompletionDateTime { get; set; }
        public string WorkStage { get; set; }
        public decimal StorageRate { get; set; }
        public int FreeStorageDays { get; set; }
        public int StorageDays { get; set; }
        public DateTime CompletedDateTime { get; set; } 
        public int OutMileage { get; set; }
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string CustomerCompanyName { get; set; }
        public string NamePrefix { get; set; }
        public string NameSuffix { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodeSuffix { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string EmailAddress { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExt { get; set; }
        public string PreferredContact { get; set; }
        public bool DoNotContactPhone { get; set; }
        public bool DoNotContactEmail { get; set; }
        public bool DoNotContactLetter { get; set; }
        public string VIN { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int ModelYear { get; set; }
        public string StockNumber { get; set; }
        public string ServiceWriter_EmployeeCode { get; set; }
        public string ServiceWriter_FirstName { get; set; }
        public string ServiceWriter_LastName { get; set; }
        public decimal LaborRateAmount { get; set; }
        public decimal TotalNonRetailPartsAmount { get; set; }
        public decimal TotalNonRetailLaborAmount { get; set; }
        public decimal TotalPartsAmount { get; set; }
        public decimal TotalLaborAmount { get; set; }
        public decimal TotalContractLaborAmount { get; set; }
        public decimal TotalESPDeductibleAmount { get; set; }
        public decimal TotalShopSuppliesAmount { get; set; }
        public decimal TotalStorageAmount { get; set; }
        public decimal TotalTaxesFeesAmount { get; set; }
        public decimal TotalWorkOrderAmount { get; set; }
        public decimal TotalDepositsApplied { get; set; }
        public decimal TotalDueAmount { get; set; }



    }
}
