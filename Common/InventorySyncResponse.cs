﻿using System.Collections.Generic;
namespace Common
{
    public class InventorySyncResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public VINS data { get; set; }
       

      public InventorySyncResponse()
        {
            data = new VINS();
        }
    }
    public class VINS
    {
       // public Dictionary<string, string> AddedVins { get; set; }
        //public AddedVins AddedVins { get; set; }
        // public List<object> NotAddedVins { get; set; }
        public List<string> AddedVins { get; set; }
        public List<string> NotAddedVins { get; set; }
        public List<string> duplicateVins { get; set; }
        public List<string> existingVins { get; set; }
        public VINS()
        { 
           // AddedVins = new Dictionary<string, string>();
            //AddedVins = new AddedVins();
            //NotAddedVins = new List<object>();
            AddedVins = new List<string>();
            NotAddedVins = new List<string>();
            duplicateVins = new List<string>();
            existingVins = new List<string>();
        }
    }
   
}
