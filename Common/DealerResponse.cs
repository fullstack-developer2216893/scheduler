﻿using System.Collections.Generic;

namespace Common
{
    public class DealerResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public List<DealerCommon> data { get; set; }

        public DealerResponse()
        {
            data = new List<DealerCommon>();
        }
    }
} 
