﻿namespace Common
{
    public class InvoiceResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public InvoiceResponseData data { get; set; }
        public InvoiceResponse()
        {
            data = new InvoiceResponseData();
        }
    }
    public class InvoiceResponseData
    {

    } 
}
