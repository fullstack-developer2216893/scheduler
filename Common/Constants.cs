﻿namespace Common
{
    public static class Constants
    {
        public const int InventoryAPILoadCount = 1;
        public const int LeadAPILoadCount = 1;
        public const int InvoiceAPILoadCount = 1;

        public const string UserInfoFilePath = "\\Common\\Resource\\UserInfo.txt";
        public const string ErrorLogFilePath = "\\Common\\Resource\\ErrorLog.txt";
        public const string CredentialsAPIUrl = @"api/2.0/talon_access";
        public const string DealerPOSTAPIUrl = @"contacts/add_lead.json";
        public const string DealerGETAPIUrl = @"api/2.0/talon_default_settings/";
        public const string json = ".json";
        //public const string InventoryPOSTAPIUrl = @"api/xml_inventories.json";
        //public const string InventoryGETAPIUrl = @"api/xml_inventories/";
        public const string InventoryGETAPIUrl = @"api/2.0/inventory/";
        public const string InventoryPOSTAPIUrl = @"api/2.0/inventory.json";
        public const string LastExecutionGETAPIUrl = @"api/2.0/talon_log.json";
        public const string LastExecutionPOSTAPIUrl = @"api/2.0/talon_log.json";
        public const string LeadPOSTAPIUrl = @"api/leads.json";
        public const string InvoicePOSTAPIUrl = @"api/2.0/talon_invoice.json";
        public const string RegPOSTAPIUrl = @"api/2.0/vendors.json";
        public const string GetSettingsAPIUrl = @"api/2.0/talon_default_settings.json";
        public const string PostLogDataAPIUrl = @"api/2.0/talon_app_logs.json";
        public const string GetLogDataAPIUrl = @"api/2.0/talon_app_logs.json";
        public const string PostCSVStatusAPIUrl = @"api/2.0/talon_csv_status.json";
        public const string PutCSVStatusAPIUrl = @"api/2.0/talon_csv_status";
        public const string GetCSVStatusAPIUrl = @"api/2.0/talon_csv_status.json";

        public const string BilledWorkOrder = "BilledWorkOrder";
        public const string ClosedVehicleDeals = "ClosedVehicleDeals";
        public const string Customer = "Customer";
        public const string Inventory = "Inventory";
        public const string Invoice = "Invoice";
    }
}
