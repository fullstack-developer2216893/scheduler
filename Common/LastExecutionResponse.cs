﻿using System.Collections.Generic;

namespace Common
{
    public class LastExecutionResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public List<LastExecutionInfo> data { get; set; }
        public LastExecutionResponse()
        {
            data = new List<LastExecutionInfo>();
        }
    } 

}
