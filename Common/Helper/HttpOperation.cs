﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helper
{
    public static class HttpOperation
    {
        public static CustomHttpResponse PostRequest(string Url, string JsonContent)
        {
            return ProcessPostRequest(Url, JsonContent, "", "");
        }
        public static CustomHttpResponse PostRequest(string Url, string JsonContent, string AuthToken)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = ProcessPostRequest(Url, JsonContent, AuthToken, "");
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }
        public static CustomHttpResponse PutRequest(string Url, string JsonContent, string AuthToken, string DealerID)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = ProcessPutRequest(Url, JsonContent, AuthToken, DealerID);
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }
        public static CustomHttpResponse PostRequest(string Url, string JsonContent, string AuthToken, string DealerID)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = ProcessPostRequest(Url, JsonContent, AuthToken, DealerID);
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }

        public static async Task<CustomHttpResponse> PostRequestAsync(string Url, string JsonContent, string AuthToken)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = await ProcessPostRequestAsync(Url, JsonContent, AuthToken, "POST");
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }
        public static async Task<CustomHttpResponse> PostRequestAsync(string Url, string JsonContent, string AuthToken, string DealerID)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = await ProcessPostRequestAsync(Url, JsonContent, AuthToken, DealerID, "POST");
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }
        public static async Task<CustomHttpResponse> PutRequestAsync(string Url, string JsonContent, string AuthToken)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = await ProcessPostRequestAsync(Url, JsonContent, AuthToken, "PUT");
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }
        public static async Task<CustomHttpResponse> PutRequestAsync(string Url, string JsonContent, string AuthToken, string DealerID)
        {
            CustomHttpResponse response = new CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                response = await ProcessPostRequestAsync(Url, JsonContent, AuthToken, DealerID, "PUT");
            }
            else
            {
                response.StatusCode = "Unauthorized";
            }
            return response;
        }
        
        private static CustomHttpResponse ProcessPostRequest(string Url, string JsonContent, string AuthToken, string DealerID)
        {
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";

            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
                if (DealerID != "" && DealerID != null)
                {
                    request.Headers.Add("DealerID", DealerID);
                }
            }

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(JsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
           request.Accept = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || length > 0)
                    {

                        string result = "";
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            result = reader.ReadToEnd();
                        }
                        returnmessage.StatusCode = response.StatusCode.ToString(); //success
                        returnmessage.Message = result;
                    }
                    else
                    {
                        returnmessage.StatusCode = response.StatusCode.ToString(); //failure. May need to log few more information here along with status code
                    }
                }
            }
            catch (WebException ex)
            {
                //var data = ex.Data;
                //string msg = ex.Message;                
                HttpWebResponse expresp = (HttpWebResponse)ex.Response;
                string result = "";
                using (var reader = new StreamReader(expresp.GetResponseStream()))
                {
                    result = reader.ReadToEnd(); // do something fun...
                }
                returnmessage.StatusCode = expresp.StatusCode.ToString();
                returnmessage.Message = result;

            }
            catch (Exception ex)
            {
                // Need to log any other type of exception
            }
            return returnmessage;
        }
        private static CustomHttpResponse ProcessPutRequest(string Url, string JsonContent, string AuthToken, string DealerID)
        {
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "PUT";

            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
                if (DealerID != "" && DealerID != null)
                {
                    request.Headers.Add("DealerID", DealerID);
                }
            }

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(JsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Accept = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created || length > 0)
                    {

                        string result = "";
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            result = reader.ReadToEnd();
                        }
                        returnmessage.StatusCode = response.StatusCode.ToString(); //success
                        returnmessage.Message = result;
                    }
                    else
                    {
                        returnmessage.StatusCode = response.StatusCode.ToString(); //failure. May need to log few more information here along with status code
                    }
                }
            }
            catch (WebException ex)
            {
                //var data = ex.Data;
                //string msg = ex.Message;                
                HttpWebResponse expresp = (HttpWebResponse)ex.Response;
                string result = "";
                using (var reader = new StreamReader(expresp.GetResponseStream()))
                {
                    result = reader.ReadToEnd(); // do something fun...
                }
                returnmessage.StatusCode = expresp.StatusCode.ToString();
                returnmessage.Message = result;

            }
            catch (Exception ex)
            {
                // Need to log any other type of exception
            }
            return returnmessage;
        }

        private static async Task<CustomHttpResponse> ProcessPostRequestAsync(string Url, string JsonContent, string AuthToken, String Method)
        {
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = Method;

            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
            }

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(JsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Accept = @"application/json";
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                var response = await request.GetResponseAsync();
                var httpresponse = (HttpWebResponse)response;
                length = httpresponse.ContentLength;
                if (httpresponse.StatusCode == HttpStatusCode.OK || length > 0)
                {
                    string result = "";
                    using (var reader = new StreamReader(httpresponse.GetResponseStream()))
                    {
                        result = reader.ReadToEnd();
                    }
                    returnmessage.StatusCode = httpresponse.StatusCode.ToString(); //success
                    returnmessage.Message = result;
                }
                else
                {
                    returnmessage.StatusCode = httpresponse.StatusCode.ToString(); //failure. May need to log few more information here along with status code
                }

            }
            catch (WebException ex)
            {
                //var data = ex.Data;
                //string msg = ex.Message;                
                HttpWebResponse expresp = (HttpWebResponse)ex.Response;
                string result = "";
                using (var reader = new StreamReader(expresp.GetResponseStream()))
                {
                    result = reader.ReadToEnd(); // do something fun...
                }
                returnmessage.StatusCode = expresp.StatusCode.ToString();
                returnmessage.Message = result;

            }
            catch (Exception ex)
            {
                // Need to log any other type of exception
            }
            return returnmessage;
        }
        private static async Task<CustomHttpResponse> ProcessPostRequestAsync(string Url, string JsonContent, string AuthToken, string DealerID, String Method)
        {
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = Method;

            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
                request.Headers.Add("DealerID", DealerID);
            }

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(JsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Accept = @"application/json";
            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                var response = await request.GetResponseAsync();
                var httpresponse = (HttpWebResponse)response;
                length = httpresponse.ContentLength;
                if (httpresponse.StatusCode == HttpStatusCode.OK || httpresponse.StatusCode == HttpStatusCode.Created || length > 0)
                {
                    string result = "";
                    using (var reader = new StreamReader(httpresponse.GetResponseStream()))
                    {
                        result = reader.ReadToEnd();
                    }
                    returnmessage.StatusCode = httpresponse.StatusCode.ToString(); //success
                    returnmessage.Message = result;
                }
                else
                {
                    returnmessage.StatusCode = httpresponse.StatusCode.ToString(); //failure. May need to log few more information here along with status code
                }
                
            }
            catch (WebException ex)
            {
                //var data = ex.Data;
                //string msg = ex.Message;                
                HttpWebResponse expresp = (HttpWebResponse)ex.Response;
                string result = "";
                using (var reader = new StreamReader(expresp.GetResponseStream()))
                {
                    result = reader.ReadToEnd(); // do something fun...
                }
                returnmessage.StatusCode = expresp.StatusCode.ToString();
                returnmessage.Message = result;

            }
            catch (Exception ex)
            {
                // Need to log any other type of exception
            }
            return returnmessage;
        }

        public static CustomHttpResponse GetRequest(string Url, string AuthToken)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
            }

            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.StatusCode = "OK";
                    returnmessage.Message = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                returnmessage.StatusCode = ex.Status.ToString();
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.Message = reader.ReadToEnd();
                }

            }
            return returnmessage;
        }
        public static CustomHttpResponse GetRequest(string Url, string AuthToken, String DealerID)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
                request.ContentType = "application/json";
                request.Headers.Add("DealerID", DealerID);
            }

            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.StatusCode = "OK";
                    returnmessage.Message = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                returnmessage.StatusCode = ex.Status.ToString();
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.Message = reader.ReadToEnd();
                }

            }
            return returnmessage;
        }
        public static async Task<CustomHttpResponse> GetRequestAsync(string Url, string AuthToken)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
            }

            try
            {
                WebResponse response = await request.GetResponseAsync();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.StatusCode = "OK";
                    returnmessage.Message = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                returnmessage.StatusCode = ex.Status.ToString();
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.Message = reader.ReadToEnd();
                }

            }
            return returnmessage;
        }
        public static async Task<CustomHttpResponse> GetRequestAsync(string Url, string AuthToken, string DealerID)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            CustomHttpResponse returnmessage = new Helper.CustomHttpResponse();
            if (AuthToken.Length > 0)
            {
                request.Headers.Add("Authorization", AuthToken);
                request.ContentType = "application/json";
                request.Headers.Add("DealerID", DealerID);
            }

            try
            {
                WebResponse response = await request.GetResponseAsync();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.StatusCode = "OK";
                    returnmessage.Message = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                returnmessage.StatusCode = ex.Status.ToString();
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    returnmessage.Message = reader.ReadToEnd();
                }

            }
            return returnmessage;
        }
    }
}  
