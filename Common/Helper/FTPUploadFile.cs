﻿using Common.Model;
using System;
using System.IO;
using System.Net;


namespace Common.Helper
{
    public class FTPUploadFile
    {


        public void FileUploadFTP(string filepath, string filename)
        {
            try
            {
                var host = "ftp://ftp_drop.dealershipperformancecrm.com/Destination/" + filename;
                var username = "dp360-talon";
                var password = "8XLk4qvBSM17u^io";
                var uploadFile = filepath;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(host);
                request.Credentials = new NetworkCredential(username, password);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Timeout = int.MaxValue;
                request.ReadWriteTimeout = int.MaxValue;
                Console.WriteLine("Wait....");
                using (Stream fileStream = File.OpenRead(uploadFile))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    fileStream.CopyTo(ftpStream);
                }
                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Console.WriteLine("Upload File Complete, status {response.StatusDescription}");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "FileUploadFTP";
                logdata.base_message = "FTP File Upload";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }

        }
        public void FileUploadFTP(string filepath, string filename, int dealerID)
        {
        x: if (DoesFtpDirectoryExist(dealerID.ToString()))
            {
                try
                {
                    var host = "ftp://ftp_drop.dealershipperformancecrm.com/Destination/" + dealerID.ToString() + filename.Replace('\\', '/');
                    var username = "dp360-talon";
                    var password = "8XLk4qvBSM17u^io";
                    var uploadFile = filepath;

                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(host);
                    request.Credentials = new NetworkCredential(username, password);
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    request.Timeout = int.MaxValue;
                    request.ReadWriteTimeout = int.MaxValue;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    Console.WriteLine("Wait....");
                    using (Stream fileStream = File.OpenRead(uploadFile))
                    using (Stream ftpStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(ftpStream);
                    }
                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    {
                        //Console.WriteLine($"Upload File Complete, status {response.StatusDescription}");
                        //Console.ReadLine();
                        request.Abort();
                    }
                }
                catch (Exception ex)
                {
                    // Console.WriteLine(ex.Message);
                    DataHandler DH = new DataHandler();
                    DH.SetErrorLog(ex.Message);

                    CRMLogData logdata = new CRMLogData();
                    logdata.id = null;
                    logdata.log_type = "Error";
                    logdata.method_name = "FileUploadFTP";
                    logdata.base_message = "FTP File Upload";
                    logdata.exception_message = ex.Message;
                    logdata.parameter_name = "no parameter";
                    logdata.stack_trace = ex.StackTrace;
                    //logdata.log_time = DateTime.Now;
                    //logdata.created = DateTime.Now;
                    //logdata.modified = DateTime.Now;

                    CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                    logdataResponse = new BLLogData().Set(logdata);
                    throw;
                }
            }
            else
            {
                MakeDirectoryFTP(dealerID.ToString());
                goto x;
            }
        }
        public void MakeDirectoryFTP(string DID)
        {

            try
            {
                WebRequest request = WebRequest.Create("ftp://ftp_drop.dealershipperformancecrm.com/Destination/" + DID);
                var username = "dp360-talon";
                var password = "8XLk4qvBSM17u^io";
                request.Credentials = new NetworkCredential(username, password);
                //request.UsePassive = true;
                //request.UseBinary = true;
                //request.KeepAlive = false;
                //Console.WriteLine("Getting the response");

                request.Method = WebRequestMethods.Ftp.MakeDirectory;

                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                    // Console.WriteLine(resp.StatusCode);
                    //  Console.ReadLine();
                    request.Abort();
                }

            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "MakeDirectoryFTP";
                logdata.base_message = "Make directory FTP";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }

        }
        public bool DoesFtpDirectoryExist(string DID)
        {
            bool res = false;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftp_drop.dealershipperformancecrm.com/Destination/" + DID + "/");
                var username = "dp360-talon";
                var password = "8XLk4qvBSM17u^io";
                request.Credentials = new NetworkCredential(username, password);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                request.Abort();
                Console.WriteLine(response.StatusCode);
                Console.ReadLine();
                res = true;
            }
            catch (WebException ex)
            {
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "DoesFtpDirectoryExist";
                logdata.base_message = "Does FTP directory exist";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                res = false;
            }
            return res;
        }
    }
}
