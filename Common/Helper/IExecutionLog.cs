﻿using System.Threading.Tasks;

namespace Common.Helper
{
    public interface IExecutionLog
    {
        Task Add(Model.LogData LogInformation);
    }
}
 