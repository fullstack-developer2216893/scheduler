﻿using Common.Model;
using System.Threading.Tasks;

namespace Common.Helper
{
    public class WriteLogOperation
    {
        private IExecutionLog operationtype;
        public WriteLogOperation(IExecutionLog OperationType)
        {
            operationtype = OperationType;
        }
        public async Task Add(LogData LogInformation)
        {
            await operationtype.Add(LogInformation);
        }
    } 
}
