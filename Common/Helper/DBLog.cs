﻿using System;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using System.IO;
using System.Collections;
using System.Configuration;

namespace Common.Helper
{
    public class DBLog : IExecutionLog
    {
        async Task IExecutionLog.Add(LogData LogInformation)
        {
            //string Destination = Directory.GetCurrentDirectory() + "\\Log";
            string Destination = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Log";
            if (!File.Exists(Destination))
            {
                Directory.CreateDirectory(Destination);
            }

            //string filename = "Log_" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString()
            //    + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString()
            //    + DateTime.Now.Millisecond.ToString() + ".txt";
            string filename = "DBErrorLog_" + DateTime.Now.Ticks.ToString() + ".txt";

            filename = Destination + "\\" + filename;

            StringBuilder InfoToLog = new StringBuilder();

            InfoToLog.Append("Log Type: " + LogInformation.LogType);
            InfoToLog.Append(Environment.NewLine);
            InfoToLog.Append("Time: " + LogInformation.LogTime);
            InfoToLog.Append(Environment.NewLine);
            InfoToLog.Append("Base Message: " + LogInformation.Message);
            InfoToLog.Append(Environment.NewLine);
            InfoToLog.Append(new String('-', 100));
            InfoToLog.Append(Environment.NewLine);
            if (LogInformation.LogType == LogType.Exception)
            {
                InfoToLog.Append("Exception Message: " + LogInformation.ExceptionInformation.Message);
                InfoToLog.Append(Environment.NewLine);

                foreach (DictionaryEntry de in LogInformation.ExceptionInformation.Data)
                {
                    InfoToLog.Append(de.Key + ": " + de.Value);
                    InfoToLog.Append(Environment.NewLine);
                }
                InfoToLog.Append(new String('-', 100));
                InfoToLog.Append(Environment.NewLine);
                InfoToLog.Append(LogInformation.ExceptionInformation.StackTrace);
                InfoToLog.Append(Environment.NewLine);
            }
            if (LogInformation.DataToLog != null && LogInformation.DataToLog.Count > 0)
            {
                InfoToLog.Append(new String('-', 100));
                InfoToLog.Append(Environment.NewLine);
                InfoToLog.Append(Environment.NewLine);
                InfoToLog.Append("Additional Information: ");
                InfoToLog.Append(Environment.NewLine);
                foreach (var info in LogInformation.DataToLog)
                {
                    InfoToLog.Append(info.Key + ": " + info.Value);
                    InfoToLog.Append(Environment.NewLine);
                } 
            }
            InfoToLog.Append(new String('=', 100));

            UnicodeEncoding uniencoding = new UnicodeEncoding();
            byte[] result = uniencoding.GetBytes(InfoToLog.ToString());

            using (FileStream SourceStream = File.Open(filename, FileMode.OpenOrCreate))
            {
                SourceStream.Seek(0, SeekOrigin.End);
                await SourceStream.WriteAsync(result, 0, result.Length);
            }
        }
    }
}
