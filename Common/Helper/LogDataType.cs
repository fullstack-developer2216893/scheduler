﻿namespace Common.Helper
{
    public static class LogDataKey
    {
        public const string Usermessage = "User message";
        public const string EffectedClass = "Effected Class";
        public const string EffectedMethod = "Effected Method";
        public const string OccuranceTime = "Occurance Time";
        public const string AdditionalInformation = "Additional Information";
        public const string LogType = "Log Data Type";
    }

    public static class LogType
    {
        public const string Exception = "Exception";
        public const string Information = "Information";
        public const string Result = "Result";
    }
}
 