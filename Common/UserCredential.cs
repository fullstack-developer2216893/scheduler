﻿namespace Common
{
    public class UserCredential
    {
        public string username { get; set; }
        public string password { get; set; }
        public string dealer_id { get; set; }
        public bool execute_service { get; set; }
    }
}
 