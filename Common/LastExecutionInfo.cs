﻿namespace Common
{
    public class LastExecutionInfo
    {
        public string execution_timestamp { get; set; }
        public int record_insert_count { get; set; }
        public int record_update_count { get; set; }
        public int status { get; set; }
        public string log { get; set; }
        public string log_type { get; set; }
        public int execution_count { get; set; }
        public int execution_status { get; set; }
        public string response { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public string modified { get; set; }
        public string created { get; set; }
        public int id { get; set; } 
    }
}
