﻿using System;

namespace Common
{
    public class DBCredentials
    {
        public string ip_address { get; set; }
        public int dealer_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string db_name { get; set; }
        public DateTime modified { get; set; }
        public DateTime created { get; set; }
        public DateTime last_modification_date { get; set; }

    }
} 
