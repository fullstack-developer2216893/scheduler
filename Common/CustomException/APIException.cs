﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.CustomException
{
    public class APIException : Exception
    {
        public string ErrorCode { get; set; }
        public APIException(string Message, string Code)
            : base(Message)
        {
            ErrorCode = Code; 
        }
    }
}
