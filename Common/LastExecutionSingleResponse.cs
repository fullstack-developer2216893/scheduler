﻿namespace Common
{
    public class LastExecutionSingleResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public LastExecutionInfo data { get; set; }
        public LastExecutionSingleResponse()
        {
            data = new LastExecutionInfo();
        }
    } 
}
