﻿using Common.Helper;
using Common.Model;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Text;

namespace Common
{
    public class BLLogData
    {
        public CRMLogDataResponse Set(CRMLogData logdata)
        {
            CRMLogDataResponse logdataResponse = new CRMLogDataResponse();

            try
            {


                string token = Global.Token;
                CustomHttpResponse response = new Helper.CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.PostLogDataAPIUrl;
                    string JsonString = JsonConvert.SerializeObject(logdata);
                    response = HttpOperation.PostRequest(Url, JsonString, token, Global.DealerID.ToString());

                    logdataResponse = JsonConvert.DeserializeObject<CRMLogDataResponse>(response.Message);
                }
                else
                { 
                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(response.Message);
                    sb.Append(Environment.NewLine);
                    sb.Append("Method:");
                    sb.Append("BLLogData-Set");
                    DH.SetErrorLog(sb.ToString());
                }

                    
                
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLogData in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");
                throw;
            }
            return logdataResponse;
        }
    }
}
