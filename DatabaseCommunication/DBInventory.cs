﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;
using Common.Model;

namespace DatabaseCommunication
{
    public class DBInventory
    {
        public int GetInventoryCount(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            int count = 0;
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQueryCount();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;

                    count = Convert.ToInt32(cmd.ExecuteScalar());


                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInventory in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInventoryCount");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching inventory. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInventoryCount";
                logdata.base_message = "Get Inventory count";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return count;
        }
        public async Task<List<Inventory>> GetInventory(DBCredentials Credentials)
        {
            List<Inventory> inventorylist = new List<Inventory>();
            try
            {

                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery();
                SqlDataReader reader = null;
                DatabaseConnection db = new DatabaseConnection();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    //SqlParameter param = new SqlParameter();
                    //param.ParameterName = "@Date";
                    //param.Value = lastexecution.ExecutionDateTime;
                    //param.SqlDbType = System.Data.SqlDbType.DateTime;
                    //cmd.Parameters.Add(param);

                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;

                    reader = await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        Inventory inventory = new Inventory();
                        inventory.vin = reader["VIN"].ToString();
                        inventory.make = reader["Make"].ToString();
                        inventory.model = reader["Model"].ToString();
                        inventory.modelYear = Convert.ToInt32(reader["ModelYear"]);
                        inventory.mileage = Convert.ToInt32(reader["Mileage"]);
                        inventory.color = reader["Color"].ToString();
                        inventory.engineCylinderCount = Convert.ToInt32(reader["EngineCylinderCount"]);
                        inventory.engineSize = Convert.ToInt32(reader["EngineSize"]);
                        inventory.factoryInstalledOptions = reader["FactoryInstalledOptions"].ToString();
                        inventory.preOwnedFlag = reader["PreOwnedFlag"].ToString();
                        inventory.stockNumber = reader["StockNumber"].ToString();
                        inventory.keyNumber = reader["KeyNumber"].ToString();
                        inventory.inventoryType = reader["InventoryType"].ToString();
                        inventory.inventoryStatus = reader["InventoryStatus"].ToString();
                        inventory.expectedDeliveryDate = Convert.ToDateTime(reader["ExpectedDeliveryDate"]);
                        inventory.deliveryDate = Convert.ToDateTime(reader["deliveryDate"]);
                        inventory.availableDate = Convert.ToDateTime(reader["AvailableDate"]);
                        inventory.isServiceDate = Convert.ToDateTime(reader["IsServiceDate"]);
                        inventory.location = reader["Location"].ToString();
                        inventory.MSRP = Convert.ToDecimal(reader["MSRP"]);
                        inventory.bookVAlueAmount = Convert.ToDecimal(reader["BookVAlueAmount"]);
                        inventory.suggestedList = Convert.ToDecimal(reader["SuggestedList"]);
                        inventory.originalInvoiceAmount = Convert.ToDecimal(reader["OriginalInvoiceAmount"]);
                        inventory.originalInvoiceDateTime = Convert.ToDateTime(reader["OriginalInvoiceDateTime"]);
                        inventory.originalInvoiceNumber = reader["OriginalInvoiceNumber"].ToString();

                        inventorylist.Add(inventory);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInventory in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInventory Async");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching inventory. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInventory";
                logdata.base_message = "Get Inventory";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }

            return inventorylist;
        }
        public List<Inventory> GetInventory(DBCredentials Credentials, int count, int pageSize)
        {
            List<Inventory> inventorylist = new List<Inventory>();
            try
            {

                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery(count, pageSize);
                SqlDataReader reader = null;
                DatabaseConnection db = new DatabaseConnection();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    //SqlParameter param = new SqlParameter();
                    //param.ParameterName = "@Date";
                    //param.Value = lastexecution.ExecutionDateTime;
                    //param.SqlDbType = System.Data.SqlDbType.DateTime;
                    //cmd.Parameters.Add(param);

                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;

                    SqlParameter param1 = new SqlParameter();
                    param1.ParameterName = "@PageNumber";
                    param1.Value = count;
                    param1.SqlDbType = System.Data.SqlDbType.Int;

                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@PageSize";
                    param2.Value = pageSize;
                    param2.SqlDbType = System.Data.SqlDbType.Int;


                    cmd.Parameters.Add(param1);
                    cmd.Parameters.Add(param2);

                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Inventory inventory = new Inventory();
                        inventory.vin = reader["VIN"].ToString();
                        inventory.make = reader["Make"].ToString();
                        inventory.model = reader["Model"].ToString();
                        inventory.modelYear = Convert.ToInt32(reader["ModelYear"]);
                        inventory.mileage = Convert.ToInt32(reader["Mileage"]);
                        inventory.color = reader["Color"].ToString();
                        inventory.engineCylinderCount = Convert.ToInt32(reader["EngineCylinderCount"]);
                        inventory.engineSize = Convert.ToInt32(reader["EngineSize"]);
                        inventory.factoryInstalledOptions = reader["FactoryInstalledOptions"].ToString();
                        inventory.preOwnedFlag = reader["PreOwnedFlag"].ToString();
                        inventory.stockNumber = reader["StockNumber"].ToString();
                        inventory.keyNumber = reader["KeyNumber"].ToString();
                        inventory.inventoryType = reader["InventoryType"].ToString();
                        inventory.inventoryStatus = reader["InventoryStatus"].ToString();
                        inventory.expectedDeliveryDate = Convert.ToDateTime(reader["ExpectedDeliveryDate"]);
                        inventory.deliveryDate = Convert.ToDateTime(reader["deliveryDate"]);
                        inventory.availableDate = Convert.ToDateTime(reader["AvailableDate"]);
                        inventory.isServiceDate = Convert.ToDateTime(reader["IsServiceDate"]);
                        inventory.location = reader["Location"].ToString();
                        inventory.MSRP = Convert.ToDecimal(reader["MSRP"]);
                        inventory.bookVAlueAmount = Convert.ToDecimal(reader["BookVAlueAmount"]);
                        inventory.suggestedList = Convert.ToDecimal(reader["SuggestedList"]);
                        inventory.originalInvoiceAmount = Convert.ToDecimal(reader["OriginalInvoiceAmount"]);
                        inventory.originalInvoiceDateTime = Convert.ToDateTime(reader["OriginalInvoiceDateTime"]);
                        inventory.originalInvoiceNumber = reader["OriginalInvoiceNumber"].ToString();

                        inventorylist.Add(inventory);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInventory in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInventory");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching inventory. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInventory";
                logdata.base_message = "Get Inventory";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }

            return inventorylist;
        }
        public List<Inventory> GetInventoryList(DBCredentials Credentials)
        {
            List<Inventory> inventorylist = new List<Inventory>();
            try
            {

                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery();
                SqlDataReader reader = null;
              //  DatabaseConnection db = new DatabaseConnection();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    //SqlParameter param = new SqlParameter();
                    //param.ParameterName = "@Date";
                    //param.Value = lastexecution.ExecutionDateTime;
                    //param.SqlDbType = System.Data.SqlDbType.DateTime;
                    //cmd.Parameters.Add(param);

                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;

                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Inventory inventory = new Inventory();
                        inventory.vin = reader["VIN"].ToString();
                        inventory.make = reader["Make"].ToString();
                        inventory.model = reader["Model"].ToString();
                        inventory.modelYear = Convert.ToInt32(reader["ModelYear"]);
                        inventory.mileage = Convert.ToInt32(reader["Mileage"]);
                        inventory.color = reader["Color"].ToString();
                        inventory.engineCylinderCount = Convert.ToInt32(reader["EngineCylinderCount"]);
                        inventory.engineSize = Convert.ToInt32(reader["EngineSize"]);
                        inventory.factoryInstalledOptions = reader["FactoryInstalledOptions"].ToString();
                        inventory.preOwnedFlag = reader["PreOwnedFlag"].ToString();
                        inventory.stockNumber = reader["StockNumber"].ToString();
                        inventory.keyNumber = reader["KeyNumber"].ToString();
                        inventory.inventoryType = reader["InventoryType"].ToString();
                        inventory.inventoryStatus = reader["InventoryStatus"].ToString();
                        inventory.expectedDeliveryDate = Convert.ToDateTime(reader["ExpectedDeliveryDate"]);
                        inventory.deliveryDate = Convert.ToDateTime(reader["deliveryDate"]);
                        inventory.availableDate = Convert.ToDateTime(reader["AvailableDate"]);
                        inventory.isServiceDate = Convert.ToDateTime(reader["IsServiceDate"]);
                        inventory.location = reader["Location"].ToString();
                        inventory.MSRP = Convert.ToDecimal(reader["MSRP"]);
                        inventory.bookVAlueAmount = Convert.ToDecimal(reader["BookVAlueAmount"]);
                        inventory.suggestedList = Convert.ToDecimal(reader["SuggestedList"]);
                        inventory.originalInvoiceAmount = Convert.ToDecimal(reader["OriginalInvoiceAmount"]);
                        inventory.originalInvoiceDateTime = Convert.ToDateTime(reader["OriginalInvoiceDateTime"]);
                        inventory.originalInvoiceNumber = reader["OriginalInvoiceNumber"].ToString();

                        inventorylist.Add(inventory);
                    }
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInventory in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInventoryList");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching inventory. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInventoryList";
                logdata.base_message = "Get Inventory list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }

            return inventorylist;
        }

        private string CreateQueryCount()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();

            query.Append(" select count(*)");
            query.Append(" from view_Extract_VehicleInventory");

            return query.ToString();
        }
        private string CreateQuery() 
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select   ISNULL(VIN,'')VIN,ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(Mileage,0)Mileage,ISNULL(Color,'')Color,ISNULL(EngineCylinderCount,0)EngineCylinderCount,ISNULL(EngineSize,0)EngineSize,ISNULL(FactoryInstalledOptions,'')FactoryInstalledOptions");
            query.Append(",ISNULL([Pre-OwnedFlag],'') as PreOwnedFlag,ISNULL(StockNumber,'')StockNumber,ISNULL(KeyNumber,'')KeyNumber,ISNULL(InventoryType,'')InventoryType,ISNULL(InventoryStatus,'')InventoryStatus,ISNULL(ExpectedDeliveryDate,'1900-01-01')ExpectedDeliveryDate,ISNULL(deliveryDate,'1900-01-01')deliveryDate");
            query.Append(",ISNULL(AvailableDate,'1900-01-01')AvailableDate,ISNULL(InServiceDate,'1900-01-01')IsServiceDate,ISNULL(Location,'')Location,ISNULL(MSRP,0)MSRP,ISNULL(BookVAlueAmount,0)BookVAlueAmount,ISNULL(SuggestedList,0)SuggestedList,ISNULL(OriginalInvoiceAmount,0)OriginalInvoiceAmount");
            query.Append(",ISNULL(OriginalInvoiceDateTime,'1900-01-01')OriginalInvoiceDateTime,ISNULL(OriginalInvoiceNumber,'')OriginalInvoiceNumber");
            query.Append(" from view_Extract_VehicleInventory");
            //   query.Append(" from [dbo].[Inventory]");

            return query.ToString();
        }
        private string CreateQuery(int count, int pageSize)
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select   ISNULL(VIN,'')VIN,ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(Mileage,0)Mileage,ISNULL(Color,'')Color,ISNULL(EngineCylinderCount,0)EngineCylinderCount,ISNULL(EngineSize,0)EngineSize,ISNULL(FactoryInstalledOptions,'')FactoryInstalledOptions");
            query.Append(",ISNULL([Pre-OwnedFlag],'') as PreOwnedFlag,ISNULL(StockNumber,'')StockNumber,ISNULL(KeyNumber,'')KeyNumber,ISNULL(InventoryType,'')InventoryType,ISNULL(InventoryStatus,'')InventoryStatus,ISNULL(ExpectedDeliveryDate,'1900-01-01')ExpectedDeliveryDate,ISNULL(deliveryDate,'1900-01-01')deliveryDate");
            query.Append(",ISNULL(AvailableDate,'1900-01-01')AvailableDate,ISNULL(InServiceDate,'1900-01-01')IsServiceDate,ISNULL(Location,'')Location,ISNULL(MSRP,0)MSRP,ISNULL(BookVAlueAmount,0)BookVAlueAmount,ISNULL(SuggestedList,0)SuggestedList,ISNULL(OriginalInvoiceAmount,0)OriginalInvoiceAmount");
            query.Append(",ISNULL(OriginalInvoiceDateTime,'1900-01-01')OriginalInvoiceDateTime,ISNULL(OriginalInvoiceNumber,'')OriginalInvoiceNumber");

            query.Append(" from view_Extract_VehicleInventory");
            //  query.Append(" from [dbo].[Inventory]");
            query.Append(" ORDER BY VIN");
            query.Append(" OFFSET @PageSize * (@PageNumber - 1) ROWS");
            query.Append(" FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)");
            return query.ToString();
        }
    }
}
