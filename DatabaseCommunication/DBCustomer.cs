﻿using Common;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseCommunication
{
    public class DBCustomer
    {
        public int GetCustomerCount(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            int count = 0;
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQueryCount();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;

                    count = Convert.ToInt32(cmd.ExecuteScalar());


                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBCustomer in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetCustomerCount");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of customer. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetCustomerCount";
                logdata.base_message = "Get Customer count";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return count;
        }
        public async Task<List<Customer>> GetCustomer(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            List<Customer> customerlist = new List<Customer>();
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery();
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    // string date = LastExecutionInfo.execution_timestamp.ToString("yyyy-MM-dd");
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    reader = await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        Customer cust = new Customer();
                        cust.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                        cust.CompanyName = reader["CompanyName"].ToString();
                        cust.FirstName = reader["FirstName"].ToString();
                        cust.LastName = reader["LastName"].ToString();
                        cust.MiddleName = reader["MiddleName"].ToString();
                        cust.NamePrefix = reader["NamePrefix"].ToString();
                        cust.NameSuffix = reader["NameSuffix"].ToString();
                        cust.InActiveDateTime = Convert.ToDateTime(reader["InActiveDateTime"]);
                        cust.Address1 = reader["Address1"].ToString();
                        cust.Address2 = reader["Address2"].ToString();
                        cust.City = reader["City"].ToString();
                        cust.State = reader["State"].ToString();
                        cust.PostalCode = reader["PostalCode"].ToString();
                        cust.PostalCodeSuffix = reader["PostalCodeSuffix"].ToString();
                        cust.County = reader["County"].ToString();
                        cust.Country = reader["Country"].ToString();
                        cust.EmailAddress = reader["EmailAddress"].ToString();
                        cust.HomePhone = reader["HomePhone"].ToString();
                        cust.MobilePhone = reader["MobilePhone"].ToString();
                        cust.WorkPhone = reader["WorkPhone"].ToString();
                        cust.WorkPhoneExt = reader["WorkPhoneExt"].ToString();
                        cust.PreferredContact = reader["PreferredContact"].ToString();
                        cust.DoNotContactPhone = Convert.ToBoolean(reader["DoNotContactPhone"]);
                        cust.DoNotContactLetter = Convert.ToBoolean(reader["DoNotContactLetter"]);
                        cust.DoNotContactEmail = Convert.ToBoolean(reader["DoNotContactEmail"]);
                        cust.Gender = reader["Gender"].ToString();
                        cust.BirthDate = reader["BirthDate"].ToString();
                        cust.CustomerInceptionDate = Convert.ToDateTime(reader["CustomerInceptionDate"]);
                        cust.LastPurchaseDate = Convert.ToDateTime(reader["LastPurchaseDate"]);
                        cust.CustomerType = reader["CustomerType"].ToString();
                        cust.VIN = reader["VIN"].ToString();
                        cust.Make = reader["Make"].ToString();
                        cust.Model = reader["Model"].ToString();
                        cust.ModelYear = Convert.ToInt32(reader["ModelYear"]);
                        cust.Mileage = Convert.ToInt32(reader["Mileage"]);
                        cust.HOG = reader["HOG"].ToString();
                        cust.Loyalty = reader["Loyalty"].ToString();

                        customerlist.Add(cust);
                    }
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBCustomer in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetCustomer Async");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of customer. Check exception message for more details");

                
                     CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetCustomer";
                logdata.base_message = "Get Customer";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return customerlist;
        }
        public List<Customer> GetCustomer(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo, int count, int pageSize)
        {
            List<Customer> customerlist = new List<Customer>();
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery(count, pageSize);
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    // string date = LastExecutionInfo.execution_timestamp.ToString("yyyy-MM-dd");
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;

                    SqlParameter param1 = new SqlParameter();
                    param1.ParameterName = "@PageNumber";
                    param1.Value = count;
                    param1.SqlDbType = System.Data.SqlDbType.Int;

                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@PageSize";
                    param2.Value = pageSize;
                    param2.SqlDbType = System.Data.SqlDbType.Int;

                    cmd.Parameters.Add(param);
                    cmd.Parameters.Add(param1);
                    cmd.Parameters.Add(param2);

                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Customer cust = new Customer();
                        cust.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                        cust.CompanyName = reader["CompanyName"].ToString();
                        cust.FirstName = reader["FirstName"].ToString();
                        cust.LastName = reader["LastName"].ToString();
                        cust.MiddleName = reader["MiddleName"].ToString();
                        cust.NamePrefix = reader["NamePrefix"].ToString();
                        cust.NameSuffix = reader["NameSuffix"].ToString();
                        cust.InActiveDateTime = Convert.ToDateTime(reader["InActiveDateTime"]);
                        cust.Address1 = reader["Address1"].ToString();
                        cust.Address2 = reader["Address2"].ToString();
                        cust.City = reader["City"].ToString();
                        cust.State = reader["State"].ToString();
                        cust.PostalCode = reader["PostalCode"].ToString();
                        cust.PostalCodeSuffix = reader["PostalCodeSuffix"].ToString();
                        cust.County = reader["County"].ToString();
                        cust.Country = reader["Country"].ToString();
                        cust.EmailAddress = reader["EmailAddress"].ToString();
                        cust.HomePhone = reader["HomePhone"].ToString();
                        cust.MobilePhone = reader["MobilePhone"].ToString();
                        cust.WorkPhone = reader["WorkPhone"].ToString();
                        cust.WorkPhoneExt = reader["WorkPhoneExt"].ToString();
                        cust.PreferredContact = reader["PreferredContact"].ToString();
                        cust.DoNotContactPhone = Convert.ToBoolean(reader["DoNotContactPhone"]);
                        cust.DoNotContactLetter = Convert.ToBoolean(reader["DoNotContactLetter"]);
                        cust.DoNotContactEmail = Convert.ToBoolean(reader["DoNotContactEmail"]);
                        cust.Gender = reader["Gender"].ToString();
                        cust.BirthDate = reader["BirthDate"].ToString();
                        cust.CustomerInceptionDate = Convert.ToDateTime(reader["CustomerInceptionDate"]);
                        cust.LastPurchaseDate = Convert.ToDateTime(reader["LastPurchaseDate"]);
                        cust.CustomerType = reader["CustomerType"].ToString();
                        cust.VIN = reader["VIN"].ToString();
                        cust.Make = reader["Make"].ToString();
                        cust.Model = reader["Model"].ToString();
                        cust.ModelYear = Convert.ToInt32(reader["ModelYear"]);
                        cust.Mileage = Convert.ToInt32(reader["Mileage"]);
                        cust.HOG = reader["HOG"].ToString();
                        cust.Loyalty = reader["Loyalty"].ToString();

                        customerlist.Add(cust);
                    }
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBCustomer in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetCustomer");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of customer. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetCustomer";
                logdata.base_message = "Get Customer list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return customerlist;
        }

        private string CreateQueryCount()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();

            query.Append(" select count(*)");
            query.Append(" from view_Extract_Customer ");
            query.Append(" where LastPurchaseDate>@Date");

            return query.ToString(); 
        }
        private string CreateQuery()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(CustomerID,0)CustomerID,ISNULL(CompanyName,'')CompanyName,ISNULL(FirstName,'')FirstName,ISNULL(LastName,'')LastName,ISNULL(MiddleName,'')MiddleName,ISNULL(NamePrefix,'')NamePrefix,ISNULL(NameSuffix,'')NameSuffix,ISNULL(InActiveDateTime,'1900-01-01')InActiveDateTime,ISNULL(Address1,'')Address1");
            query.Append(",ISNULL(Address2,'') Address2,ISNULL(City,'')City,ISNULL(State,'')State,ISNULL(PostalCode,'')PostalCode,ISNULL(PostalCodeSuffix,'')PostalCodeSuffix,ISNULL(County,'')County,ISNULL(Country,'')Country,ISNULL(EmailAddress,'')EmailAddress,ISNULL(HomePhone,'')HomePhone,ISNULL(MobilePhone,'')MobilePhone,ISNULL(WorkPhone,'')WorkPhone,ISNULL(WorkPhoneExt,'')WorkPhoneExt ");
            query.Append(", ISNULL(PreferredContact,0)PreferredContact,ISNULL(DoNotContactPhone,0)DoNotContactPhone,ISNULL(DoNotContactLetter,0)DoNotContactLetter,ISNULL(DoNotContactEmail,0)DoNotContactEmail,ISNULL(Gender,'')Gender,ISNULL(BirthDate,'')BirthDate,ISNULL(CustomerInceptionDate,'1900-01-01')CustomerInceptionDate ");
            query.Append(", ISNULL(LastPurchaseDate,'1900-01-01')LastPurchaseDate,ISNULL(CustomerType,'')CustomerType,ISNULL(VIN,'')VIN,ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(Mileage,0)Mileage,ISNULL(HOG,'')HOG,ISNULL(Loyalty,'')Loyalty");
            query.Append(" from view_Extract_Customer ");
            // query.Append(" from [dbo].[Customer] ");
            query.Append(" where LastPurchaseDate>@Date");

            return query.ToString();
        }
        private string CreateQuery(int count, int pageSize)
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(CustomerID,0)CustomerID,ISNULL(CompanyName,'')CompanyName,ISNULL(FirstName,'')FirstName,ISNULL(LastName,'')LastName,ISNULL(MiddleName,'')MiddleName,ISNULL(NamePrefix,'')NamePrefix,ISNULL(NameSuffix,'')NameSuffix,ISNULL(InActiveDateTime,'1900-01-01')InActiveDateTime,ISNULL(Address1,'')Address1");
            query.Append(",ISNULL(Address2,'') Address2,ISNULL(City,'')City,ISNULL(State,'')State,ISNULL(PostalCode,'')PostalCode,ISNULL(PostalCodeSuffix,'')PostalCodeSuffix,ISNULL(County,'')County,ISNULL(Country,'')Country,ISNULL(EmailAddress,'')EmailAddress,ISNULL(HomePhone,'')HomePhone,ISNULL(MobilePhone,'')MobilePhone,ISNULL(WorkPhone,'')WorkPhone,ISNULL(WorkPhoneExt,'')WorkPhoneExt ");
            query.Append(", ISNULL(PreferredContact,0)PreferredContact,ISNULL(DoNotContactPhone,0)DoNotContactPhone,ISNULL(DoNotContactLetter,0)DoNotContactLetter,ISNULL(DoNotContactEmail,0)DoNotContactEmail,ISNULL(Gender,'')Gender,ISNULL(BirthDate,'')BirthDate,ISNULL(CustomerInceptionDate,'1900-01-01')CustomerInceptionDate ");
            query.Append(", ISNULL(LastPurchaseDate,'1900-01-01')LastPurchaseDate,ISNULL(CustomerType,'')CustomerType,ISNULL(VIN,'')VIN,ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(Mileage,0)Mileage,ISNULL(HOG,'')HOG,ISNULL(Loyalty,'')Loyalty");

            query.Append(" from view_Extract_Customer ");
            // query.Append(" from [dbo].[Customer] ");
            query.Append(" where LastPurchaseDate>@Date");
            query.Append(" ORDER BY CustomerID");
            query.Append(" OFFSET @PageSize * (@PageNumber - 1) ROWS");
            query.Append(" FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)");


            return query.ToString();
        }
    }
}
