﻿using System;
using System.Data.SqlClient;
using Common;

namespace DatabaseCommunication
{
    public class DatabaseConnection
    {
        public bool TestConnection(DBCredentials Credentials)
        {
            string conString = Helper.DBConnection.CreateConnectionString(Credentials);
            using (SqlConnection connection = new SqlConnection(conString))
            {
                try
                {
                   connection.Open(); 
                    //connection.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
