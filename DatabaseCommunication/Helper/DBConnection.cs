﻿using System.Text;
using Common;

namespace DatabaseCommunication.Helper
{
    public static class DBConnection
    {
        public static string CreateConnectionString(DBCredentials Credentials)
        {
                    StringBuilder connectionString = new StringBuilder();
                    connectionString.Append("Data Source = ");
                  connectionString.Append(Credentials.ip_address);
                 //connectionString.Append("192.168.1.12");
             
            
                    connectionString.Append(";initial catalog = ");
                 connectionString.Append(Credentials.db_name);
                  //connectionString.Append("Talon_Data");
            
            
           
                    connectionString.Append("; User Id = ");
                    connectionString.Append(Credentials.username);
                  //connectionString.Append("sa");
              
                    connectionString.Append("; Password = ");
                      connectionString.Append(Credentials.password);
                    //connectionString.Append("123");
             
            return connectionString.ToString();
        }
    }}
