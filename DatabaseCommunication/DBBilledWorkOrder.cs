﻿using Common;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;


namespace DatabaseCommunication
{
    public class DBBilledWorkOrder
    {
        public int GetBilledWorkOrderCount(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            int count = 0;
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQueryCount();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;

                    count = Convert.ToInt32(cmd.ExecuteScalar());


                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBBilledWorkOrder in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetBilledWorkOrderCount");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of billed worked order. Check exception message for more details");
                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetBilledWorkOrderCount";
                logdata.base_message = "Get BilledWorkOrder count";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return count;
        }
        public async Task<List<BilledWorkOrder>> GetBilledWorkOrder(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            List<BilledWorkOrder> billedworkorderlist = new List<BilledWorkOrder>();
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery();
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;
                    reader = await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        BilledWorkOrder billedworkorder = new BilledWorkOrder();
                        billedworkorder.WorkOrderID = reader["WorkOrderID"].ToString();
                        billedworkorder.Type = Convert.ToInt32(reader["Type"]);
                        billedworkorder.RequestBy = reader["RequestBy"].ToString();
                        billedworkorder.AppointmentDateTime = Convert.ToDateTime(reader["AppointmentDateTime"]);
                        billedworkorder.ArrivalDateTime = Convert.ToDateTime(reader["ArrivalDateTime"]);
                        billedworkorder.InMileage = Convert.ToInt32(reader["InMileage"]);
                        billedworkorder.PromisedCompletionDateTime = Convert.ToDateTime(reader["PromisedCompletionDateTime"]);
                        billedworkorder.WorkStage = reader["WorkStage"].ToString();
                        billedworkorder.StorageRate = Convert.ToDecimal(reader["StorageRate"]);
                        billedworkorder.FreeStorageDays = Convert.ToInt32(reader["FreeStorageDays"]);
                        billedworkorder.StorageDays = Convert.ToInt32(reader["StorageDays"]);
                        billedworkorder.CompletedDateTime = Convert.ToDateTime(reader["AppointmentDateTime"]);
                        billedworkorder.OutMileage = Convert.ToInt32(reader["OutMileage"]);
                        billedworkorder.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                        billedworkorder.FirstName = reader["FirstName"].ToString();
                        billedworkorder.LastName = reader["LastName"].ToString();
                        billedworkorder.MiddleName = reader["MiddleName"].ToString();
                        billedworkorder.CustomerCompanyName = reader["CustomerCompanyName"].ToString();
                        billedworkorder.NamePrefix = reader["NamePrefix"].ToString();
                        billedworkorder.NameSuffix = reader["NameSuffix"].ToString();
                        billedworkorder.Address1 = reader["Address1"].ToString();
                        billedworkorder.Address2 = reader["Address2"].ToString();
                        billedworkorder.City = reader["City"].ToString();
                        billedworkorder.State = reader["State"].ToString();
                        billedworkorder.PostalCode = reader["PostalCode"].ToString();
                        billedworkorder.PostalCodeSuffix = reader["PostalCodeSuffix"].ToString();
                        billedworkorder.County = reader["County"].ToString();
                        billedworkorder.Country = reader["Country"].ToString();
                        billedworkorder.EmailAddress = reader["EmailAddress"].ToString();
                        billedworkorder.HomePhone = reader["HomePhone"].ToString();
                        billedworkorder.MobilePhone = reader["MobilePhone"].ToString();
                        billedworkorder.WorkPhone = reader["WorkPhone"].ToString();
                        billedworkorder.WorkPhoneExt = reader["WorkPhoneExt"].ToString();
                        billedworkorder.PreferredContact = reader["PreferredContact"].ToString();
                        billedworkorder.DoNotContactPhone = Convert.ToBoolean(reader["DoNotContactPhone"]);
                        billedworkorder.DoNotContactEmail = Convert.ToBoolean(reader["DoNotContactEmail"]);
                        billedworkorder.DoNotContactLetter = Convert.ToBoolean(reader["DoNotContactLetter"]);
                        billedworkorder.VIN = reader["VIN"].ToString();
                        billedworkorder.Make = reader["Make"].ToString();
                        billedworkorder.Model = reader["Model"].ToString();
                        billedworkorder.ModelYear = Convert.ToInt32(reader["ModelYear"]);
                        billedworkorder.StockNumber = reader["StockNumber"].ToString();
                        billedworkorder.ServiceWriter_EmployeeCode = reader["ServiceWriter_EmployeeCode"].ToString();
                        billedworkorder.ServiceWriter_FirstName = reader["ServiceWriter_FirstName"].ToString();
                        billedworkorder.ServiceWriter_LastName = reader["ServiceWriter_LastName"].ToString();
                        billedworkorder.LaborRateAmount = Convert.ToDecimal(reader["LaborRateAmount"]);
                        billedworkorder.TotalNonRetailPartsAmount = Convert.ToDecimal(reader["TotalNonRetailPartsAmount"]);
                        billedworkorder.TotalNonRetailLaborAmount = Convert.ToDecimal(reader["TotalNonRetailLaborAmount"]);
                        billedworkorder.TotalPartsAmount = Convert.ToDecimal(reader["TotalPartsAmount"]);
                        billedworkorder.TotalLaborAmount = Convert.ToDecimal(reader["TotalLaborAmount"]);
                        billedworkorder.TotalContractLaborAmount = Convert.ToDecimal(reader["TotalContractLaborAmount"]);
                        billedworkorder.TotalESPDeductibleAmount = Convert.ToDecimal(reader["TotalESPDeductibleAmount"]);
                        billedworkorder.TotalShopSuppliesAmount = Convert.ToDecimal(reader["TotalShopSuppliesAmount"]);
                        billedworkorder.TotalStorageAmount = Convert.ToDecimal(reader["TotalStorageAmount"]);
                        billedworkorder.TotalTaxesFeesAmount = Convert.ToDecimal(reader["TotalTaxesFeesAmount"]);
                        billedworkorder.TotalWorkOrderAmount = Convert.ToDecimal(reader["TotalWorkOrderAmount"]);
                        billedworkorder.TotalDepositsApplied = Convert.ToDecimal(reader["TotalDepositsApplied"]);
                        billedworkorder.TotalDueAmount = Convert.ToDecimal(reader["TotalDueAmount"]);

                        billedworkorderlist.Add(billedworkorder);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBBilledWorkOrder in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetBilledWorkOrder Async");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of billed worked order. Check exception message for more details");

                
                     CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetBilledWorkOrder";
                logdata.base_message = "Get BilledWorkOrder list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return billedworkorderlist;
        }
        public List<BilledWorkOrder> GetBilledWorkOrder(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo, int count, int pageSize)
        {
            List<BilledWorkOrder> billedworkorderlist = new List<BilledWorkOrder>();
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery(count, pageSize);
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    } 
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;

                    SqlParameter param1 = new SqlParameter();
                    param1.ParameterName = "@PageNumber";
                    param1.Value = count;
                    param1.SqlDbType = System.Data.SqlDbType.Int;

                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@PageSize";
                    param2.Value = pageSize;
                    param2.SqlDbType = System.Data.SqlDbType.Int;

                    cmd.Parameters.Add(param);
                    cmd.Parameters.Add(param1);
                    cmd.Parameters.Add(param2);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        BilledWorkOrder billedworkorder = new BilledWorkOrder();
                        billedworkorder.WorkOrderID = reader["WorkOrderID"].ToString();
                        billedworkorder.Type = Convert.ToInt32(reader["Type"]);
                        billedworkorder.RequestBy = reader["RequestBy"].ToString();
                        billedworkorder.AppointmentDateTime = Convert.ToDateTime(reader["AppointmentDateTime"]);
                        billedworkorder.ArrivalDateTime = Convert.ToDateTime(reader["ArrivalDateTime"]);
                        billedworkorder.InMileage = Convert.ToInt32(reader["InMileage"]);
                        billedworkorder.PromisedCompletionDateTime = Convert.ToDateTime(reader["PromisedCompletionDateTime"]);
                        billedworkorder.WorkStage = reader["WorkStage"].ToString();
                        billedworkorder.StorageRate = Convert.ToDecimal(reader["StorageRate"]);
                        billedworkorder.FreeStorageDays = Convert.ToInt32(reader["FreeStorageDays"]);
                        billedworkorder.StorageDays = Convert.ToInt32(reader["StorageDays"]);
                        billedworkorder.CompletedDateTime = Convert.ToDateTime(reader["CompletedDateTime"]);
                        billedworkorder.OutMileage = Convert.ToInt32(reader["OutMileage"]);
                        billedworkorder.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                        billedworkorder.FirstName = reader["FirstName"].ToString();
                        billedworkorder.LastName = reader["LastName"].ToString();
                        billedworkorder.MiddleName = reader["MiddleName"].ToString();
                        billedworkorder.CustomerCompanyName = reader["CustomerCompanyName"].ToString();
                        billedworkorder.NamePrefix = reader["NamePrefix"].ToString();
                        billedworkorder.NameSuffix = reader["NameSuffix"].ToString();
                        billedworkorder.Address1 = reader["Address1"].ToString();
                        billedworkorder.Address2 = reader["Address2"].ToString();
                        billedworkorder.City = reader["City"].ToString();
                        billedworkorder.State = reader["State"].ToString();
                        billedworkorder.PostalCode = reader["PostalCode"].ToString();
                        billedworkorder.PostalCodeSuffix = reader["PostalCodeSuffix"].ToString();
                        billedworkorder.County = reader["County"].ToString();
                        billedworkorder.Country = reader["Country"].ToString();
                        billedworkorder.EmailAddress = reader["EmailAddress"].ToString();
                        billedworkorder.HomePhone = reader["HomePhone"].ToString();
                        billedworkorder.MobilePhone = reader["MobilePhone"].ToString();
                        billedworkorder.WorkPhone = reader["WorkPhone"].ToString();
                        billedworkorder.WorkPhoneExt = reader["WorkPhoneExt"].ToString();
                        billedworkorder.PreferredContact = reader["PreferredContact"].ToString();
                        billedworkorder.DoNotContactPhone = Convert.ToBoolean(reader["DoNotContactPhone"]);
                        billedworkorder.DoNotContactEmail = Convert.ToBoolean(reader["DoNotContactEmail"]);
                        billedworkorder.DoNotContactLetter = Convert.ToBoolean(reader["DoNotContactLetter"]);
                        billedworkorder.VIN = reader["VIN"].ToString();
                        billedworkorder.Make = reader["Make"].ToString();
                        billedworkorder.Model = reader["Model"].ToString();
                        billedworkorder.ModelYear = Convert.ToInt32(reader["ModelYear"]);
                        billedworkorder.StockNumber = reader["StockNumber"].ToString();
                        billedworkorder.ServiceWriter_EmployeeCode = reader["ServiceWriter_EmployeeCode"].ToString();
                        billedworkorder.ServiceWriter_FirstName = reader["ServiceWriter_FirstName"].ToString();
                        billedworkorder.ServiceWriter_LastName = reader["ServiceWriter_LastName"].ToString();
                        billedworkorder.LaborRateAmount = Convert.ToDecimal(reader["LaborRateAmount"]);
                        billedworkorder.TotalNonRetailPartsAmount = Convert.ToDecimal(reader["TotalNonRetailPartsAmount"]);
                        billedworkorder.TotalNonRetailLaborAmount = Convert.ToDecimal(reader["TotalNonRetailLaborAmount"]);
                        billedworkorder.TotalPartsAmount = Convert.ToDecimal(reader["TotalPartsAmount"]);
                        billedworkorder.TotalLaborAmount = Convert.ToDecimal(reader["TotalLaborAmount"]);
                        billedworkorder.TotalContractLaborAmount = Convert.ToDecimal(reader["TotalContractLaborAmount"]);
                        billedworkorder.TotalESPDeductibleAmount = Convert.ToDecimal(reader["TotalESPDeductibleAmount"]);
                        billedworkorder.TotalShopSuppliesAmount = Convert.ToDecimal(reader["TotalShopSuppliesAmount"]);
                        billedworkorder.TotalStorageAmount = Convert.ToDecimal(reader["TotalStorageAmount"]);
                        billedworkorder.TotalTaxesFeesAmount = Convert.ToDecimal(reader["TotalTaxesFeesAmount"]);
                        billedworkorder.TotalWorkOrderAmount = Convert.ToDecimal(reader["TotalWorkOrderAmount"]);
                        billedworkorder.TotalDepositsApplied = Convert.ToDecimal(reader["TotalDepositsApplied"]);
                        billedworkorder.TotalDueAmount = Convert.ToDecimal(reader["TotalDueAmount"]);

                        billedworkorderlist.Add(billedworkorder);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBBilledWorkOrder in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetBilledWorkOrder");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of billed worked order. Check exception message for more details");

                
                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetBilledWorkOrder";
                logdata.base_message = "Get BilledWorkOrder list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return billedworkorderlist;
        }
        private string CreateQueryCount()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();

            query.Append(" select count(*)");
            query.Append(" from view_Extract_BilledWorkOrder");
            query.Append(" where CompletedDateTime>@Date");

            return query.ToString();
        }
        private string CreateQuery()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(WorkOrderID,'')WorkOrderID,ISNULL(Type,0)Type,ISNULL(RequestBy,'')RequestBy,ISNULL(AppointmentDateTime,'1900-01-01')AppointmentDateTime,ISNULL(ArrivalDateTime,'1900-01-01')ArrivalDateTime,ISNULL(InMileage,0)InMileage,ISNULL(PromisedCompletionDateTime,'1900-01-01')PromisedCompletionDateTime");
            query.Append(",ISNULL(WorkStage,'')WorkStage,ISNULL(StorageRate,0)StorageRate,ISNULL(FreeStorageDays,0)FreeStorageDays,ISNULL(StorageDays,0)StorageDays,ISNULL(CompletedDateTime,'1900-01-01')CompletedDateTime,ISNULL(OutMileage,0)OutMileage,ISNULL(CustomerID,0)CustomerID,ISNULL(FirstName,'')FirstName ");
            query.Append(",ISNULL(LastName,'')LastName,ISNULL(MiddleName,'')MiddleName,ISNULL(CustomerCompanyName,'')CustomerCompanyName,ISNULL(NamePrefix,'')NamePrefix,ISNULL(NameSuffix,'')NameSuffix,ISNULL(Address1,'')Address1,ISNULL(Address2,'')Address2,ISNULL(City,'')City,ISNULL(State,'')State,ISNULL(PostalCode,'')PostalCode");
            query.Append(",ISNULL(PostalCodeSuffix,'')PostalCodeSuffix,ISNULL(County,'')County,ISNULL(Country,'')Country,ISNULL(EmailAddress,'')EmailAddress,ISNULL(HomePhone,'')HomePhone,ISNULL(MobilePhone,'')MobilePhone,ISNULL(WorkPhone,'')WorkPhone,ISNULL(WorkPhoneExt,'')WorkPhoneExt,ISNULL(PreferredContact,'')PreferredContact");
            query.Append(",ISNULL(DoNotContactPhone,0)DoNotContactPhone,ISNULL(DoNotContactEmail,0)DoNotContactEmail,ISNULL(DoNotContactLetter,0)DoNotContactLetter,ISNULL(VIN,'')VIN,ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(StockNumber,'')StockNumber,ISNULL(ServiceWriter_EmployeeCode,'')ServiceWriter_EmployeeCode");
            query.Append(",ISNULL(ServiceWriter_FirstName,'')ServiceWriter_FirstName,ISNULL(ServiceWriter_LastName,'')ServiceWriter_LastName,ISNULL(LaborRateAmount,0)LaborRateAmount,ISNULL(TotalNonRetailPartsAmount,0)TotalNonRetailPartsAmount,ISNULL(TotalNonRetailLaborAmount,0)TotalNonRetailLaborAmount");
            query.Append(",ISNULL(TotalPartsAmount,0)TotalPartsAmount,ISNULL(TotalLaborAmount,0)TotalLaborAmount,ISNULL(TotalContractLaborAmount,0)TotalContractLaborAmount,ISNULL(TotalESPDeductibleAmount,0)TotalESPDeductibleAmount,ISNULL(TotalShopSuppliesAmount,0)TotalShopSuppliesAmount");
            query.Append(",ISNULL(TotalStorageAmount,0)TotalStorageAmount,ISNULL(TotalTaxesFeesAmount,0)TotalTaxesFeesAmount,ISNULL(TotalWorkOrderAmount,0)TotalWorkOrderAmount,ISNULL(TotalDepositsApplied,0)TotalDepositsApplied,ISNULL(TotalDueAmount,0)TotalDueAmount");

            query.Append(" from view_Extract_BilledWorkOrder");
            // query.Append(" from [dbo].[BilledWordOrder]");
            query.Append(" where CompletedDateTime>@Date");

            return query.ToString();
        }
        private string CreateQuery(int count, int pageSize)
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(WorkOrderID,'')WorkOrderID,ISNULL(Type,0)Type,ISNULL(RequestBy,'')RequestBy,ISNULL(AppointmentDateTime,'1900-01-01')AppointmentDateTime,ISNULL(ArrivalDateTime,'1900-01-01')ArrivalDateTime,ISNULL(InMileage,0)InMileage,ISNULL(PromisedCompletionDateTime,'1900-01-01')PromisedCompletionDateTime");
            query.Append(",ISNULL(WorkStage,'')WorkStage,ISNULL(StorageRate,0)StorageRate,ISNULL(FreeStorageDays,0)FreeStorageDays,ISNULL(StorageDays,0)StorageDays,ISNULL(CompletedDateTime,'1900-01-01')CompletedDateTime,ISNULL(OutMileage,0)OutMileage,ISNULL(CustomerID,0)CustomerID,ISNULL(FirstName,'')FirstName ");
            query.Append(",ISNULL(LastName,'')LastName,ISNULL(MiddleName,'')MiddleName,ISNULL(CustomerCompanyName,'')CustomerCompanyName,ISNULL(NamePrefix,'')NamePrefix,ISNULL(NameSuffix,'')NameSuffix,ISNULL(Address1,'')Address1,ISNULL(Address2,'')Address2,ISNULL(City,'')City,ISNULL(State,'')State,ISNULL(PostalCode,'')PostalCode");
            query.Append(",ISNULL(PostalCodeSuffix,'')PostalCodeSuffix,ISNULL(County,'')County,ISNULL(Country,'')Country,ISNULL(EmailAddress,'')EmailAddress,ISNULL(HomePhone,'')HomePhone,ISNULL(MobilePhone,'')MobilePhone,ISNULL(WorkPhone,'')WorkPhone,ISNULL(WorkPhoneExt,'')WorkPhoneExt,ISNULL(PreferredContact,'')PreferredContact");
            query.Append(",ISNULL(DoNotContactPhone,0)DoNotContactPhone,ISNULL(DoNotContactEmail,0)DoNotContactEmail,ISNULL(DoNotContactLetter,0)DoNotContactLetter,ISNULL(VIN,'')VIN,ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(StockNumber,'')StockNumber,ISNULL(ServiceWriter_EmployeeCode,'')ServiceWriter_EmployeeCode");
            query.Append(",ISNULL(ServiceWriter_FirstName,'')ServiceWriter_FirstName,ISNULL(ServiceWriter_LastName,'')ServiceWriter_LastName,ISNULL(LaborRateAmount,0)LaborRateAmount,ISNULL(TotalNonRetailPartsAmount,0)TotalNonRetailPartsAmount,ISNULL(TotalNonRetailLaborAmount,0)TotalNonRetailLaborAmount");
            query.Append(",ISNULL(TotalPartsAmount,0)TotalPartsAmount,ISNULL(TotalLaborAmount,0)TotalLaborAmount,ISNULL(TotalContractLaborAmount,0)TotalContractLaborAmount,ISNULL(TotalESPDeductibleAmount,0)TotalESPDeductibleAmount,ISNULL(TotalShopSuppliesAmount,0)TotalShopSuppliesAmount");
            query.Append(",ISNULL(TotalStorageAmount,0)TotalStorageAmount,ISNULL(TotalTaxesFeesAmount,0)TotalTaxesFeesAmount,ISNULL(TotalWorkOrderAmount,0)TotalWorkOrderAmount,ISNULL(TotalDepositsApplied,0)TotalDepositsApplied,ISNULL(TotalDueAmount,0)TotalDueAmount");

            query.Append(" from view_Extract_BilledWorkOrder");
            // query.Append(" from [dbo].[BilledWordOrder]");
            query.Append(" where CompletedDateTime>@Date");
            query.Append(" ORDER BY WorkOrderID");
            query.Append(" OFFSET @PageSize * (@PageNumber - 1) ROWS");
            query.Append(" FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)");


            return query.ToString();
        }
    }
}
