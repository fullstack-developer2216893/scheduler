﻿using Common;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseCommunication
{
    public class DBInvoice
    {
        public int GetInvoiceCount(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            int count = 0;
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQueryCount();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    
                    cmd.CommandTimeout = 120;

                    count = Convert.ToInt32(cmd.ExecuteScalar());


                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInvoice in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInvoice");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching invoice. Check exception message for more details");


                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInvoiceCount";
                logdata.base_message = "Get Invoice count";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return count;
        }
        public async Task<List<Invoice>> GetInvoice(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            List<Invoice> invoiceList = new List<Invoice>();
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery();
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;
                    reader = await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        Invoice invoice = new Invoice();

                        invoice.InvoiceID = Convert.ToInt32(reader["InvoiceID"]);
                        invoice.TransactionType = reader["TransactionType"].ToString();
                        invoice.PointofSalesOrderID = Convert.ToInt32(reader["PointofSalesOrderID"]);
                        invoice.WorkOrderID = reader["WorkOrderID"].ToString();
                        invoice.DealID = Convert.ToInt32(reader["DealID"]);
                        invoice.EComOrderNum = Convert.ToInt32(reader["EComOrderNum"]);
                        invoice.InvoiceDateTime = Convert.ToDateTime(reader["InvoiceDateTime"]);
                        invoice.TotalAmount = Convert.ToDecimal(reader["TotalAmount"]);
                        invoice.TotalTenderedAmount = Convert.ToDecimal(reader["TotalTenderedAmount"]);
                        invoice.TotalShippingAmount = Convert.ToDecimal(reader["TotalShippingAmount"]);
                        invoice.TotalSalesTaxAmount = Convert.ToDecimal(reader["TotalSalesTaxAmount"]);
                        invoice.ChangeGivenAmount = Convert.ToDecimal(reader["ChangeGivenAmount"]);
                        invoice.PaidByCashAmount = Convert.ToDecimal(reader["PaidByCashAmount"]);
                        invoice.PaidByCheckAmount = Convert.ToDecimal(reader["PaidByCheckAmount"]);
                        invoice.PaidByCreditCardAmount = Convert.ToDecimal(reader["PaidByCreditCardAmount"]);
                        invoice.PaidByCustomerAccountAmount = Convert.ToDecimal(reader["PaidByCustomerAccountAmount"]);
                        invoice.PaidByGiftCertificateAmount = Convert.ToDecimal(reader["PaidByGiftCertificateAmount"]);
                        invoice.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                        invoice.CustomerFirstName = reader["CustomerFirstName"].ToString();
                        invoice.CustomerLastName = reader["CustomerLastName"].ToString();
                        invoice.CustomerMiddleName = reader["CustomerMiddleName"].ToString();
                        invoice.CustomerCompanyName = reader["CustomerCompanyName"].ToString();
                        invoice.CustomerPrefixCode = ""; // reader["CustomerPrefixCode"].ToString();
                        invoice.CustomerNameSuffix = reader["CustomerNameSuffix"].ToString();
                        invoice.Address1 = reader["Address1"].ToString();
                        invoice.Address2 = reader["Address2"].ToString();
                        invoice.CustomerCity = reader["CustomerCity"].ToString();
                        invoice.CustomerState = reader["CustomerState"].ToString();
                        invoice.CustomerPostalCode = reader["CustomerPostalCode"].ToString();
                        invoice.CustomerPostalCodeSuffix = reader["CustomerPostalCodeSuffix"].ToString();
                        invoice.CustomerCounty = reader["CustomerCounty"].ToString();
                        invoice.CustomerCountry = reader["CustomerCountry"].ToString();
                        invoice.CustomerEmailAddress = reader["CustomerEmailAddress"].ToString();
                        invoice.CustomerHomePhone = reader["CustomerHomePhone"].ToString();
                        invoice.CustomerMobilePhone = reader["CustomerMobilePhone"].ToString();
                        invoice.CustomerWorkPhone = reader["CustomerWorkPhone"].ToString();
                        invoice.CustomerWorkPhoneExt = reader["CustomerWorkPhoneExt"].ToString();
                        invoice.PreferredContact = reader["PreferredContact"].ToString();
                        invoice.DoNotContactPhone = reader["DoNotContactPhone"].ToString();
                        invoice.DoNotContactEmail = reader["DoNotContactEmail"].ToString();
                        invoice.DoNotContactLetter = reader["DoNotContactLetter"].ToString();
                        invoice.Salesperson_EmployeeCode = reader["Salesperson_EmployeeCode"].ToString();
                        invoice.Salesperson_FirstName = reader["Salesperson_FirstName"].ToString();
                        invoice.Salesperson_LastName = reader["Salesperson_LastName"].ToString();
                        invoice.LineItemID = reader["LineItemID"].ToString();
                        invoice.LineItemName = reader["LineItemName"].ToString();
                        invoice.LineItemVendorID = reader["LineItemVendorID"].ToString();
                        invoice.LineItemStatus = reader["LineItemStatus"].ToString();
                        invoice.LineItemUnitPriceAmount = Convert.ToDecimal(reader["LineItemUnitPriceAmount"]);
                        invoice.LineItemQuantity = Convert.ToDecimal(reader["LineItemQuantity"]);
                        invoice.LineItemListPriceAmount = Convert.ToDecimal(reader["LineItemListPriceAmount"]);
                        invoice.LineItemDiscountAmount = Convert.ToDecimal(reader["LineItemDiscountAmount"]);
                        invoice.LineItemDiscountRate = Convert.ToDecimal(reader["LineItemDiscountRate"]);
                        invoice.LineItemDeposit = Convert.ToDecimal(reader["LineItemDeposit"]);
                        invoice.LineItemSalesTaxAmount = Convert.ToDecimal(reader["LineItemSalesTaxAmount"]);
                        invoiceList.Add(invoice);
                    }
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInvoice in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInvoice Async");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching invoice. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInvoice";
                logdata.base_message = "Get Invoice";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return invoiceList;
        }
        public List<Invoice> GetInvoice(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo, int count, int pageSize)
        {
            List<Invoice> invoiceList = new List<Invoice>();
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);

                string query = CreateQuery(count, pageSize);
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;

                    SqlParameter param1 = new SqlParameter();
                    param1.ParameterName = "@PageNumber";
                    param1.Value = count;
                    param1.SqlDbType = System.Data.SqlDbType.Int;

                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@PageSize";
                    param2.Value = pageSize;
                    param2.SqlDbType = System.Data.SqlDbType.Int;

                    cmd.Parameters.Add(param);
                    cmd.Parameters.Add(param1);
                    cmd.Parameters.Add(param2);

                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Invoice invoice = new Invoice();

                        invoice.InvoiceID = Convert.ToInt32(reader["InvoiceID"]);
                        invoice.TransactionType = reader["TransactionType"].ToString();
                        invoice.PointofSalesOrderID = Convert.ToInt32(reader["PointofSalesOrderID"]);
                        invoice.WorkOrderID = reader["WorkOrderID"].ToString();
                        invoice.DealID = Convert.ToInt32(reader["DealID"]);
                        invoice.EComOrderNum = Convert.ToInt32(reader["EComOrderNum"]);
                        invoice.InvoiceDateTime = Convert.ToDateTime(reader["InvoiceDateTime"]);
                        invoice.TotalAmount = Convert.ToDecimal(reader["TotalAmount"]);
                        invoice.TotalTenderedAmount = Convert.ToDecimal(reader["TotalTenderedAmount"]);
                        invoice.TotalShippingAmount = Convert.ToDecimal(reader["TotalShippingAmount"]);
                        invoice.TotalSalesTaxAmount = Convert.ToDecimal(reader["TotalSalesTaxAmount"]);
                        invoice.ChangeGivenAmount = Convert.ToDecimal(reader["ChangeGivenAmount"]);
                        invoice.PaidByCashAmount = Convert.ToDecimal(reader["PaidByCashAmount"]);
                        invoice.PaidByCheckAmount = Convert.ToDecimal(reader["PaidByCheckAmount"]);
                        invoice.PaidByCreditCardAmount = Convert.ToDecimal(reader["PaidByCreditCardAmount"]);
                        invoice.PaidByCustomerAccountAmount = Convert.ToDecimal(reader["PaidByCustomerAccountAmount"]);
                        invoice.PaidByGiftCertificateAmount = Convert.ToDecimal(reader["PaidByGiftCertificateAmount"]);
                        invoice.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                        invoice.CustomerFirstName = reader["CustomerFirstName"].ToString();
                        invoice.CustomerLastName = reader["CustomerLastName"].ToString();
                        invoice.CustomerMiddleName = reader["CustomerMiddleName"].ToString();
                        invoice.CustomerCompanyName = reader["CustomerCompanyName"].ToString();
                        invoice.CustomerPrefixCode = ""; // reader["CustomerPrefixCode"].ToString();
                        invoice.CustomerNameSuffix = reader["CustomerNameSuffix"].ToString();
                        invoice.Address1 = reader["Address1"].ToString();
                        invoice.Address2 = reader["Address2"].ToString();
                        invoice.CustomerCity = reader["CustomerCity"].ToString();
                        invoice.CustomerState = reader["CustomerState"].ToString();
                        invoice.CustomerPostalCode = reader["CustomerPostalCode"].ToString();
                        invoice.CustomerPostalCodeSuffix = reader["CustomerPostalCodeSuffix"].ToString();
                        invoice.CustomerCounty = reader["CustomerCounty"].ToString();
                        invoice.CustomerCountry = reader["CustomerCountry"].ToString();
                        invoice.CustomerEmailAddress = reader["CustomerEmailAddress"].ToString();
                        invoice.CustomerHomePhone = reader["CustomerHomePhone"].ToString();
                        invoice.CustomerMobilePhone = reader["CustomerMobilePhone"].ToString();
                        invoice.CustomerWorkPhone = reader["CustomerWorkPhone"].ToString();
                        invoice.CustomerWorkPhoneExt = reader["CustomerWorkPhoneExt"].ToString();
                        invoice.PreferredContact = reader["PreferredContact"].ToString();
                        invoice.DoNotContactPhone = reader["DoNotContactPhone"].ToString();
                        invoice.DoNotContactEmail = reader["DoNotContactEmail"].ToString();
                        invoice.DoNotContactLetter = reader["DoNotContactLetter"].ToString();
                        invoice.Salesperson_EmployeeCode = reader["Salesperson_EmployeeCode"].ToString();
                        invoice.Salesperson_FirstName = reader["Salesperson_FirstName"].ToString();
                        invoice.Salesperson_LastName = reader["Salesperson_LastName"].ToString();
                        invoice.LineItemID = reader["LineItemID"].ToString();
                        invoice.LineItemName = reader["LineItemName"].ToString();
                        invoice.LineItemVendorID = reader["LineItemVendorID"].ToString();
                        invoice.LineItemStatus = reader["LineItemStatus"].ToString();
                        invoice.LineItemUnitPriceAmount = Convert.ToDecimal(reader["LineItemUnitPriceAmount"]);
                        invoice.LineItemQuantity = Convert.ToDecimal(reader["LineItemQuantity"]);
                        invoice.LineItemListPriceAmount = Convert.ToDecimal(reader["LineItemListPriceAmount"]);
                        invoice.LineItemDiscountAmount = Convert.ToDecimal(reader["LineItemDiscountAmount"]);
                        invoice.LineItemDiscountRate = Convert.ToDecimal(reader["LineItemDiscountRate"]);
                        invoice.LineItemDeposit = Convert.ToDecimal(reader["LineItemDeposit"]);
                        invoice.LineItemSalesTaxAmount = Convert.ToDecimal(reader["LineItemSalesTaxAmount"]);
                        invoiceList.Add(invoice);
                    }
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBInvoice in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetInvoice");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching invoice. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetInvoice";
                logdata.base_message = "Get Invoice list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return invoiceList;
        }

        private string CreateQueryCount()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();

            query.Append(" select count(*)");
            query.Append(" from view_Extract_Invoice");
            // query.Append(" from [dbo].[Invoice]");
            query.Append(" where InvoiceDateTime>@Date");

            return query.ToString(); 
        }
        private string CreateQuery()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(InvoiceID,0) InvoiceID,ISNULL(TransactionType,'')TransactionType,ISNULL(PointofSaleOrderID,0)PointofSalesOrderID,ISNULL(WorkOrderID,'')WorkOrderID,ISNULL(DealID,0)DealID,ISNULL(EComOrderNum,0)EComOrderNum,ISNULL(InvoiceDateTime,'1900-01-01')InvoiceDateTime,ISNULL(TotalAmount,0)TotalAmount");
            query.Append(",ISNULL(TotalTenderedAmount,0)TotalTenderedAmount,ISNULL(TotalShippingAmount,0)TotalShippingAmount,ISNULL(TotalSalesTaxAmount,0)TotalSalesTaxAmount,ISNULL(ChangeGivenAmount,0)ChangeGivenAmount,ISNULL(PaidByCashAmount,0)PaidByCashAmount,ISNULL(PaidByCheckAmount,0)PaidByCheckAmount ");
            query.Append(",ISNULL(PaidByCreditCardAmount,0)PaidByCreditCardAmount,ISNULL(PaidByCustomerAccountAmount,0)PaidByCustomerAccountAmount,ISNULL(PaidByGiftCertificateAmount,0)PaidByGiftCertificateAmount,ISNULL(CustomerID,0)CustomerID,ISNULL(CustomerFirstName,'')CustomerFirstName ");
            query.Append(",ISNULL(CustomerLastName,'')CustomerLastName,ISNULL(CustomerMiddleName,'')CustomerMiddleName,ISNULL(CustomerCompanyName,'')CustomerCompanyName,'' CustomerPrefixCode,ISNULL(CustomerNameSuffix,'')CustomerNameSuffix,ISNULL(CustomerAddress1,'')Address1,ISNULL(CustomerAddress2,'')Address2 ");
            query.Append(",ISNULL(CustomerCity,'')CustomerCity,ISNULL(CustomerState,'')CustomerState,ISNULL(CustomerPostalCode,'')CustomerPostalCode,ISNULL(CustomerPostalCodeSuffix,'')CustomerPostalCodeSuffix,ISNULL(CustomerCounty,'')CustomerCounty,ISNULL(CustomerCountry,'')CustomerCountry");
            query.Append(",ISNULL(CustomerEmailAddress,'')CustomerEmailAddress,ISNULL(CustomerHomePhone,'')CustomerHomePhone,ISNULL(CustomerMobilePhone,'')CustomerMobilePhone,ISNULL(CustomerWorkPhone,'')CustomerWorkPhone,ISNULL(CustomerWorkPhoneExt,'')CustomerWorkPhoneExt,ISNULL(PreferredContact,'')PreferredContact");
            query.Append(",ISNULL(DoNotContactPhone,0)DoNotContactPhone,ISNULL(DoNotContactEmail,0)DoNotContactEmail,ISNULL(DoNotContactLetter,0)DoNotContactLetter,ISNULL(Salesperson_EmployeeCode,'')Salesperson_EmployeeCode,ISNULL(Salesperson_FirstName,'')Salesperson_FirstName");
            query.Append(",ISNULL(Salesperson_LastName,'')Salesperson_LastName,ISNULL(LineItemID,'')LineItemID,ISNULL(LineItemName,'')LineItemName,ISNULL(LineItemVendorID,'')LineItemVendorID,ISNULL(LineItemStatus,'')LineItemStatus,ISNULL(LineItemUnitPriceAmount,'')LineItemUnitPriceAmount,ISNULL(LineItemQuantity,'0')LineItemQuantity");
            query.Append(",ISNULL(LineItemListPriceAmount,0)LineItemListPriceAmount,ISNULL(LineItemDiscountAmount,'')LineItemDiscountAmount,ISNULL(LineItemDiscountRate,0)LineItemDiscountRate,ISNULL(LineItemDeposit,0)LineItemDeposit,ISNULL(LineItemSalesTaxAmount,0)LineItemSalesTaxAmount");
            query.Append(" from view_Extract_Invoice");
            // query.Append(" from [dbo].[Invoice]");
            query.Append(" where InvoiceDateTime>@Date");
            return query.ToString();
        }
        private string CreateQuery(int count, int pageSize)
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(InvoiceID,0) InvoiceID,ISNULL(TransactionType,'')TransactionType,ISNULL(PointofSaleOrderID,0)PointofSalesOrderID,ISNULL(WorkOrderID,'')WorkOrderID,ISNULL(DealID,0)DealID,ISNULL(EComOrderNum,0)EComOrderNum,ISNULL(InvoiceDateTime,'1900-01-01')InvoiceDateTime,ISNULL(TotalAmount,0)TotalAmount");
            query.Append(",ISNULL(TotalTenderedAmount,0)TotalTenderedAmount,ISNULL(TotalShippingAmount,0)TotalShippingAmount,ISNULL(TotalSalesTaxAmount,0)TotalSalesTaxAmount,ISNULL(ChangeGivenAmount,0)ChangeGivenAmount,ISNULL(PaidByCashAmount,0)PaidByCashAmount,ISNULL(PaidByCheckAmount,0)PaidByCheckAmount ");
            query.Append(",ISNULL(PaidByCreditCardAmount,0)PaidByCreditCardAmount,ISNULL(PaidByCustomerAccountAmount,0)PaidByCustomerAccountAmount,ISNULL(PaidByGiftCertificateAmount,0)PaidByGiftCertificateAmount,ISNULL(CustomerID,0)CustomerID,ISNULL(CustomerFirstName,'')CustomerFirstName ");
            query.Append(",ISNULL(CustomerLastName,'')CustomerLastName,ISNULL(CustomerMiddleName,'')CustomerMiddleName,ISNULL(CustomerCompanyName,'')CustomerCompanyName,'' CustomerPrefixCode,ISNULL(CustomerNameSuffix,'')CustomerNameSuffix,ISNULL(CustomerAddress1,'')Address1,ISNULL(CustomerAddress2,'')Address2 ");
            query.Append(",ISNULL(CustomerCity,'')CustomerCity,ISNULL(CustomerState,'')CustomerState,ISNULL(CustomerPostalCode,'')CustomerPostalCode,ISNULL(CustomerPostalCodeSuffix,'')CustomerPostalCodeSuffix,ISNULL(CustomerCounty,'')CustomerCounty,ISNULL(CustomerCountry,'')CustomerCountry");
            query.Append(",ISNULL(CustomerEmailAddress,'')CustomerEmailAddress,ISNULL(CustomerHomePhone,'')CustomerHomePhone,ISNULL(CustomerMobilePhone,'')CustomerMobilePhone,ISNULL(CustomerWorkPhone,'')CustomerWorkPhone,ISNULL(CustomerWorkPhoneExt,'')CustomerWorkPhoneExt,ISNULL(PreferredContact,'')PreferredContact");
            query.Append(",ISNULL(DoNotContactPhone,0)DoNotContactPhone,ISNULL(DoNotContactEmail,0)DoNotContactEmail,ISNULL(DoNotContactLetter,0)DoNotContactLetter,ISNULL(Salesperson_EmployeeCode,'')Salesperson_EmployeeCode,ISNULL(Salesperson_FirstName,'')Salesperson_FirstName");
            query.Append(",ISNULL(Salesperson_LastName,'')Salesperson_LastName,ISNULL(LineItemID,'')LineItemID,ISNULL(LineItemName,'')LineItemName,ISNULL(LineItemVendorID,'')LineItemVendorID,ISNULL(LineItemStatus,'')LineItemStatus,ISNULL(LineItemUnitPriceAmount,'')LineItemUnitPriceAmount,ISNULL(LineItemQuantity,'0')LineItemQuantity");
            query.Append(",ISNULL(LineItemListPriceAmount,0)LineItemListPriceAmount,ISNULL(LineItemDiscountAmount,'')LineItemDiscountAmount,ISNULL(LineItemDiscountRate,0)LineItemDiscountRate,ISNULL(LineItemDeposit,0)LineItemDeposit,ISNULL(LineItemSalesTaxAmount,0)LineItemSalesTaxAmount");

            query.Append(" from view_Extract_Invoice");
            // query.Append(" from [dbo].[Invoice]");
           query.Append(" where InvoiceDateTime>@Date");
            query.Append(" ORDER BY InvoiceID");
            query.Append(" OFFSET @PageSize * (@PageNumber - 1) ROWS");
            query.Append(" FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)");



            return query.ToString();
        }
    }
}
