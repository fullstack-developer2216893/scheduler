﻿using Common;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseCommunication
{
    public class DBClosedVehicleDeals
    {
        public int GetClosedVehicleDealsCount(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            int count = 0;
            try
            {
                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQueryCount();

                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    cmd.CommandTimeout = 120;

                    count = Convert.ToInt32(cmd.ExecuteScalar());


                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBClosedVehicleDeals in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetClosedVehicleDealsCount");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of closed vehicle deals. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetClosedVehicleDealsCount";
                logdata.base_message = "Get ClosedVehicleDeals count";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return count;
        }
        public async Task<List<ClosedVehicleDeals>> GetClosedVehicleDeals(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            List<ClosedVehicleDeals> CVDlist = new List<ClosedVehicleDeals>();
            try
            {


                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery();
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;
                    cmd.Parameters.Add(param);
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    reader = await cmd.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        ClosedVehicleDeals CVDeals = new ClosedVehicleDeals();
                        CVDeals.DealID = Convert.ToInt32(reader["DealID"]);
                        CVDeals.DealStatus = reader["DealStatus"].ToString();
                        CVDeals.CreateDAteTime = Convert.ToDateTime(reader["CreateDateTime"]);
                        CVDeals.ContractDateTime = Convert.ToDateTime(reader["ContractDateTime"]);
                        CVDeals.ClosedDateTime = Convert.ToDateTime(reader["ClosedDateTime"]);
                        CVDeals.DeliveryDateTime = Convert.ToDateTime(reader["DeliveryDateTime"]);
                        CVDeals.InvoiceDateTime = Convert.ToDateTime(reader["InvoiceDateTime"]);
                        CVDeals.InactiveDateTime = Convert.ToDateTime(reader["InactiveDateTime"]);
                        CVDeals.VIN = reader["VIN"].ToString();
                        CVDeals.Make = reader["Make"].ToString();
                        CVDeals.Model = reader["Model"].ToString();
                        CVDeals.ModelYear = Convert.ToInt32(reader["ModelYear"]);
                        CVDeals.Mileage = Convert.ToInt32(reader["Mileage"]);
                        CVDeals.Color = reader["Color"].ToString();
                        // CVDeals.EngineCylinderCount= Convert.ToInt32(reader["EngineCylinderCount"]);
                        CVDeals.FactoryInstalledOptions = reader["FactoryInstalledOptions"].ToString();
                        CVDeals.PreOwnedFlag = reader["PreOwnedFlag"].ToString();
                        CVDeals.StockNumber = reader["StockNumber"].ToString();
                        CVDeals.BuyerCustomerID = Convert.ToInt32(reader["BuyerCustomerID"]);
                        CVDeals.BuyerFirstName = reader["BuyerFirstName"].ToString();
                        CVDeals.BuyerLastName = reader["BuyerLastName"].ToString();
                        CVDeals.BuyerMiddleName = reader["BuyerMiddleName"].ToString();
                        CVDeals.BuyerCompanyName = reader["BuyerCompanyName"].ToString();
                        CVDeals.BuyerNamePrefix = reader["BuyerNamePrefix"].ToString();
                        CVDeals.BuyerNameSuffix = reader["BuyerNameSuffix"].ToString();
                        CVDeals.BuyerAddress1 = reader["BuyerAddress1"].ToString();
                        CVDeals.BuyerAddress2 = reader["BuyerAddress2"].ToString();
                        CVDeals.BuyerCity = reader["BuyerCity"].ToString();
                        CVDeals.BuyerState = reader["BuyerState"].ToString();
                        CVDeals.BuyerPostalCode = reader["BuyerPostalCode"].ToString();
                        CVDeals.BuyerPostalCodeSuffix = reader["BuyerPostalCodeSuffix"].ToString();
                        CVDeals.BuyerCounty = reader["BuyerCounty"].ToString();
                        CVDeals.BuyerCountry = reader["BuyerCountry"].ToString();
                        CVDeals.BuyerEmailAddress = reader["BuyerEmailAddress"].ToString();
                        CVDeals.BuyerHomePhone = reader["BuyerHomePhone"].ToString();
                        CVDeals.BuyerMobilePhone = reader["BuyerMobilePhone"].ToString();
                        CVDeals.BuyerWorkPhone = reader["BuyerWorkPhone"].ToString();
                        CVDeals.BuyerWorkPhoneExt = reader["BuyerWorkPhoneExt"].ToString();
                        CVDeals.BuyerPreferredContact = reader["BuyerPreferredContact"].ToString();
                        CVDeals.BuyerDoNotContactPhone = Convert.ToBoolean(reader["BuyerDoNotContactPhone"]);
                        CVDeals.BuyerDoNotContactEmail = Convert.ToBoolean(reader["BuyerDoNotContactEmail"]);
                        CVDeals.BuyerDoNotContactLetter = Convert.ToBoolean(reader["BuyerDoNotContactLetter"]);
                        CVDeals.CoBuyerCustomerID = Convert.ToInt32(reader["CoBuyerCustomerID"]);
                        CVDeals.CoBuyerFirstName = reader["CoBuyerFirstName"].ToString();
                        CVDeals.CoBuyerLastName = reader["CoBuyerLastName"].ToString();
                        CVDeals.CoBuyerMiddleName = reader["CoBuyerMiddleName"].ToString();
                        CVDeals.CoBuyerCompanyName = reader["CoBuyerCompanyName"].ToString();
                        CVDeals.CoBuyerNamePrefix = reader["CoBuyerNamePrefix"].ToString();
                        CVDeals.CoBuyerNameSuffix = reader["CoBuyerNameSuffix"].ToString();
                        CVDeals.CoBuyerAddress1 = reader["CoBuyerAddress1"].ToString();
                        CVDeals.CoBuyerAddress2 = reader["CoBuyerAddress2"].ToString();
                        CVDeals.CoBuyerCity = reader["CoBuyerCity"].ToString();
                        CVDeals.CoBuyerState = reader["CoBuyerState"].ToString();
                        CVDeals.CoBuyerPostalCode = reader["CoBuyerPostalCode"].ToString();
                        CVDeals.CoBuyerPostalCodeSuffix = reader["CoBuyerPostalCodeSuffix"].ToString();
                        CVDeals.CoBuyerCounty = reader["CoBuyerCounty"].ToString();
                        CVDeals.CoBuyerCountry = reader["CoBuyerCountry"].ToString();
                        CVDeals.CoBuyerEmailAddress = reader["CoBuyerEmailAddress"].ToString();
                        CVDeals.CoBuyerHomePhone = reader["CoBuyerHomePhone"].ToString();
                        CVDeals.CoBuyerMobilePhone = reader["CoBuyerMobilePhone"].ToString();
                        CVDeals.CoBuyerWorkPhone = reader["CoBuyerWorkPhone"].ToString();
                        CVDeals.CoBuyerWorkPhoneExt = reader["CoBuyerWorkPhoneExt"].ToString();
                        CVDeals.CoBuyerPreferredContact = reader["CoBuyerPreferredContact"].ToString();
                        CVDeals.CoBuyerDoNotContactPhone = Convert.ToBoolean(reader["CoBuyerDoNotContactPhone"]);
                        CVDeals.CoBuyerDoNotContactEmail = Convert.ToBoolean(reader["CoBuyerDoNotContactEmail"]);
                        CVDeals.CoBuyerDoNotContactLetter = Convert.ToBoolean(reader["CoBuyerDoNotContactLetter"]);
                        CVDeals.VehicleSalesperson1_EmployeeCode = reader["VehicleSalesperson1_EmployeeCode"].ToString();
                        CVDeals.VehicleSalesperson1_FirstName = reader["VehicleSalesperson1_FirstName"].ToString();
                        CVDeals.VehicleSalesperson1_LastName = reader["VehicleSalesperson1_LastName"].ToString();
                        CVDeals.VehicleSalesperson2_EmployeeCode = reader["VehicleSalesperson2_EmployeeCode"].ToString();
                        CVDeals.VehicleSalesperson2_FirstName = reader["VehicleSalesperson2_FirstName"].ToString();
                        CVDeals.VehicleSalesperson2_LastName = reader["VehicleSalesperson2_LastName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson1_EmployeeCode = reader["FinanceInsuranceSalesperson1_EmployeeCode"].ToString();
                        CVDeals.FinanceInsuranceSalesperson1_FirstName = reader["FinanceInsuranceSalesperson1_FirstName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson1_LastName = reader["FinanceInsuranceSalesperson1_LastName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson2_EmployeeCode = reader["FinanceInsuranceSalesperson2_EmployeeCode"].ToString();
                        CVDeals.FinanceInsuranceSalesperson2_FirstName = reader["FinanceInsuranceSalesperson2_FirstName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson2_LastName = reader["FinanceInsuranceSalesperson2_LastName"].ToString();
                        CVDeals.SalesManager_EmployeeCode = reader["SalesManager_EmployeeCode"].ToString();
                        CVDeals.SalesManager_FirstName = reader["SalesManager_FirstName"].ToString();
                        CVDeals.SalesManager_LastName = reader["SalesManager_LastName"].ToString();
                        CVDeals.Lienholder = reader["Lienholder"].ToString();
                        CVDeals.LoanAPR = Convert.ToDecimal(reader["LoanAPR"]);
                        CVDeals.LoanTerm = Convert.ToInt32(reader["LoanTerm"]);
                        CVDeals.LoanFirstPayDate = Convert.ToDateTime(reader["LoanFirstPayDate"]);
                        CVDeals.LoanPaymentsPerYear = Convert.ToInt32(reader["LoanPaymentsPerYear"]);
                        CVDeals.LoanPaymentAmount = Convert.ToDecimal(reader["LoanPaymentAmount"]);
                        CVDeals.DeallinkApplicationID = reader["DeallinkApplicationID"].ToString();
                        CVDeals.TotalRetailPrice = Convert.ToDecimal(reader["TotalRetailPrice"]);
                        CVDeals.TotalAccessoriesandLaborAmount = Convert.ToDecimal(reader["TotalAccessoriesandLaborAmount"]);
                        CVDeals.TotalAddOnAmount = Convert.ToDecimal(reader["TotalAddOnAmount"]);
                        CVDeals.TotalESPAmount = Convert.ToDecimal(reader["TotalESPAmount"]);
                        CVDeals.TotalPPMAmount = Convert.ToDecimal(reader["TotalPPMAmount"]);
                        CVDeals.TotalAccidentHealthAmount = Convert.ToDecimal(reader["TotalAccidentHealthAmount"]);
                        CVDeals.TotalCreditLifeAmount = Convert.ToDecimal(reader["TotalCreditLifeAmount"]);
                        CVDeals.TotalChargesandFeesAmount = Convert.ToDecimal(reader["TotalChargesandFeesAmount"]);
                        CVDeals.TotalVehicleTaxAmount = Convert.ToDecimal(reader["TotalVehicleTaxAmount"]);
                        CVDeals.TotalAccessoriesandLaborTaxAmount = Convert.ToDecimal(reader["TotalAccessoriesandLaborTaxAmount"]);
                        CVDeals.TotalSupplementalChargesTaxAmount = Convert.ToDecimal(reader["TotalSupplementalChargesTaxAmount"]);
                        CVDeals.TotalAccessoryFeesAmount = Convert.ToDecimal(reader["TotalAccessoryFeesAmount"]);
                        CVDeals.TotalDocStampsAmount = Convert.ToDecimal(reader["TotalDocStampsAmount"]);
                        CVDeals.TotalTaxAmount = Convert.ToDecimal(reader["TotalTaxAmount"]);
                        CVDeals.TotalTradeInAllowanceAmount = Convert.ToDecimal(reader["TotalTradeInAllowanceAmount"]);
                        CVDeals.TotalTradeInPayoffAmount = Convert.ToDecimal(reader["TotalTradeInPayoffAmount"]);
                        CVDeals.TotalTradeInActualCashValueAmount = Convert.ToDecimal(reader["TotalTradeInActualCashValueAmount"]);
                        CVDeals.TotalDepositAmount = Convert.ToDecimal(reader["TotalDepositAmount"]);
                        CVDeals.TotalDownPaymentAmount = Convert.ToDecimal(reader["TotalDownPaymentAmount"]);
                        CVDeals.TotalRebateAmount = Convert.ToDecimal(reader["TotalRebateAmount"]);
                        CVDeals.TotalFinancedAmount = Convert.ToDecimal(reader["TotalFinancedAmount"]);
                        CVDeals.TotalDueAmount = Convert.ToDecimal(reader["TotalDueAmount"]);
                        CVDeals.AddOnItemID = reader["AddOnItemID"].ToString();
                        CVDeals.AddOnItemName = reader["AddOnItemName"].ToString();
                        CVDeals.AddOnItemVendorID = reader["AddOnItemVendorID"].ToString();
                        CVDeals.AddOnItemStatus = reader["AddOnItemStatus"].ToString();
                        CVDeals.AddOnItemUnitPriceAmount = reader["AddOnItemUnitPriceAmount"].ToString();
                        CVDeals.AddOnItemQuantity = Convert.ToDecimal(reader["AddOnItemQuantity"]);
                        CVDeals.AddOnItemListPriceAmount = Convert.ToDecimal(reader["AddOnItemListPriceAmount"]);
                        CVDeals.AddOnItemDiscountAmount = Convert.ToDecimal(reader["AddOnItemDiscountAmount"]);
                        CVDeals.AddOnItemDiscountRate = Convert.ToDecimal(reader["AddOnItemDiscountRate"]);
                        CVDeals.AddOnItemSalesTaxAmount = Convert.ToDecimal(reader["AddOnItemSalesTaxAmount"]);
                        CVDeals.ChargeID = Convert.ToInt32(reader["ChargeID"]);
                        CVDeals.ChargeName = reader["ChargeName"].ToString();
                        CVDeals.ChargeVendorID = reader["ChargeVendorID"].ToString();
                        CVDeals.ChargeCostAmount = Convert.ToDecimal(reader["ChargeCostAmount"]);
                        CVDeals.ChargeListPriceAmount = Convert.ToDecimal(reader["ChargeListPriceAmount"]);
                        CVDeals.ChargeSalesTaxAmount = Convert.ToDecimal(reader["ChargeSalesTaxAmount"]);
                        CVDeals.TradeInVIN = reader["TradeInVIN"].ToString();
                        CVDeals.TradeInMake = reader["TradeInMake"].ToString();
                        CVDeals.TradeInModel = reader["TradeInModel"].ToString();
                        CVDeals.TradeInModelYear = Convert.ToInt32(reader["TradeInModelYear"]);
                        CVDeals.TradeInMileage = Convert.ToInt32(reader["TradeInMileage"]);
                        CVDeals.TradeInColor = reader["TradeInColor"].ToString();
                        CVDeals.TradeInFactoryInstalledOptions = reader["TradeInFactoryInstalledOptions"].ToString();
                        CVDeals.TradeInStockNumber = reader["TradeInStockNumber"].ToString();
                        CVDeals.TradeInAllowanceAmount = Convert.ToDecimal(reader["TradeInAllowanceAmount"]);
                        CVDeals.TradeInPayoffAmount = Convert.ToDecimal(reader["TradeInPayoffAmount"]);
                        CVDeals.TradeInActualCashValueAmount = Convert.ToDecimal(reader["TradeInActualCashValueAmount"]);

                        CVDlist.Add(CVDeals);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBClosedVehicleDeals in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetClosedVehicleDeals Async");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of closed vehicle deals. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetClosedVehicleDeals";
                logdata.base_message = "Get ClosedVehicleDeals list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return CVDlist;
        }
        public List<ClosedVehicleDeals> GetClosedVehicleDeals(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo, int count, int pageSize)
        {
            List<ClosedVehicleDeals> CVDlist = new List<ClosedVehicleDeals>();
            try
            {


                string conString = Helper.DBConnection.CreateConnectionString(Credentials);
                string query = CreateQuery(count, pageSize);
                SqlDataReader reader = null;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Date";
                    if (LastExecutionInfo != null && LastExecutionInfo.execution_timestamp != null && LastExecutionInfo.execution_timestamp != "0001-01-01T00:00:00+00:00")
                    {
                        string s = LastExecutionInfo.execution_timestamp;
                        s = s.Substring(0, s.IndexOf('+'));
                        string dt = Convert.ToDateTime(s).ToString("yyyy-MM-dd HH:mm:ss");
                        param.Value = dt;
                    }
                    else
                    {
                        param.Value = "1900-01-01";
                    }
                    param.SqlDbType = System.Data.SqlDbType.DateTime;

                    SqlParameter param1 = new SqlParameter();
                    param1.ParameterName = "@PageNumber";
                    param1.Value = count;
                    param1.SqlDbType = System.Data.SqlDbType.Int;

                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@PageSize";
                    param2.Value = pageSize;
                    param2.SqlDbType = System.Data.SqlDbType.Int;

                    cmd.Parameters.Add(param);
                    cmd.Parameters.Add(param1);
                    cmd.Parameters.Add(param2);

                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = query;
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ClosedVehicleDeals CVDeals = new ClosedVehicleDeals();
                        CVDeals.DealID = Convert.ToInt32(reader["DealID"]);
                        CVDeals.DealStatus = reader["DealStatus"].ToString();
                        CVDeals.CreateDAteTime = Convert.ToDateTime(reader["CreateDAteTime"]);
                        CVDeals.ContractDateTime = Convert.ToDateTime(reader["ContractDateTime"]);
                        CVDeals.ClosedDateTime = Convert.ToDateTime(reader["ClosedDateTime"]);
                        CVDeals.DeliveryDateTime = Convert.ToDateTime(reader["DeliveryDateTime"]);
                        CVDeals.InvoiceDateTime = Convert.ToDateTime(reader["InvoiceDateTime"]);
                        CVDeals.InactiveDateTime = Convert.ToDateTime(reader["InactiveDateTime"]);
                        CVDeals.VIN = reader["VIN"].ToString();
                        CVDeals.Make = reader["Make"].ToString();
                        CVDeals.Model = reader["Model"].ToString();
                        CVDeals.ModelYear = Convert.ToInt32(reader["ModelYear"]);
                        CVDeals.Mileage = Convert.ToInt32(reader["Mileage"]);
                        CVDeals.Color = reader["Color"].ToString();
                        // CVDeals.EngineCylinderCount= Convert.ToInt32(reader["EngineCylinderCount"]);
                        CVDeals.FactoryInstalledOptions = reader["FactoryInstalledOptions"].ToString();
                        CVDeals.PreOwnedFlag = reader["PreOwnedFlag"].ToString();
                        CVDeals.StockNumber = reader["StockNumber"].ToString();
                        CVDeals.BuyerCustomerID = Convert.ToInt32(reader["BuyerCustomerID"]);
                        CVDeals.BuyerFirstName = reader["BuyerFirstName"].ToString();
                        CVDeals.BuyerLastName = reader["BuyerLastName"].ToString();
                        CVDeals.BuyerMiddleName = reader["BuyerMiddleName"].ToString();
                        CVDeals.BuyerCompanyName = reader["BuyerCompanyName"].ToString();
                        CVDeals.BuyerNamePrefix = reader["BuyerNamePrefix"].ToString();
                        CVDeals.BuyerNameSuffix = reader["BuyerNameSuffix"].ToString();
                        CVDeals.BuyerAddress1 = reader["BuyerAddress1"].ToString();
                        CVDeals.BuyerAddress2 = reader["BuyerAddress2"].ToString();
                        CVDeals.BuyerCity = reader["BuyerCity"].ToString();
                        CVDeals.BuyerState = reader["BuyerState"].ToString();
                        CVDeals.BuyerPostalCode = reader["BuyerPostalCode"].ToString();
                        CVDeals.BuyerPostalCodeSuffix = reader["BuyerPostalCodeSuffix"].ToString();
                        CVDeals.BuyerCounty = reader["BuyerCounty"].ToString();
                        CVDeals.BuyerCountry = reader["BuyerCountry"].ToString();
                        CVDeals.BuyerEmailAddress = reader["BuyerEmailAddress"].ToString();
                        CVDeals.BuyerHomePhone = reader["BuyerHomePhone"].ToString();
                        CVDeals.BuyerMobilePhone = reader["BuyerMobilePhone"].ToString();
                        CVDeals.BuyerWorkPhone = reader["BuyerWorkPhone"].ToString();
                        CVDeals.BuyerWorkPhoneExt = reader["BuyerWorkPhoneExt"].ToString();
                        CVDeals.BuyerPreferredContact = reader["BuyerPreferredContact"].ToString();
                        CVDeals.BuyerDoNotContactPhone = Convert.ToBoolean(reader["BuyerDoNotContactPhone"]);
                        CVDeals.BuyerDoNotContactEmail = Convert.ToBoolean(reader["BuyerDoNotContactEmail"]);
                        CVDeals.BuyerDoNotContactLetter = Convert.ToBoolean(reader["BuyerDoNotContactLetter"]);
                        CVDeals.CoBuyerCustomerID = Convert.ToInt32(reader["CoBuyerCustomerID"]);
                        CVDeals.CoBuyerFirstName = reader["CoBuyerFirstName"].ToString();
                        CVDeals.CoBuyerLastName = reader["CoBuyerLastName"].ToString();
                        CVDeals.CoBuyerMiddleName = reader["CoBuyerMiddleName"].ToString();
                        CVDeals.CoBuyerCompanyName = reader["CoBuyerCompanyName"].ToString();
                        CVDeals.CoBuyerNamePrefix = reader["CoBuyerNamePrefix"].ToString();
                        CVDeals.CoBuyerNameSuffix = reader["CoBuyerNameSuffix"].ToString();
                        CVDeals.CoBuyerAddress1 = reader["CoBuyerAddress1"].ToString();
                        CVDeals.CoBuyerAddress2 = reader["CoBuyerAddress2"].ToString();
                        CVDeals.CoBuyerCity = reader["CoBuyerCity"].ToString();
                        CVDeals.CoBuyerState = reader["CoBuyerState"].ToString();
                        CVDeals.CoBuyerPostalCode = reader["CoBuyerPostalCode"].ToString();
                        CVDeals.CoBuyerPostalCodeSuffix = reader["CoBuyerPostalCodeSuffix"].ToString();
                        CVDeals.CoBuyerCounty = reader["CoBuyerCounty"].ToString();
                        CVDeals.CoBuyerCountry = reader["CoBuyerCountry"].ToString();
                        CVDeals.CoBuyerEmailAddress = reader["CoBuyerEmailAddress"].ToString();
                        CVDeals.CoBuyerHomePhone = reader["CoBuyerHomePhone"].ToString();
                        CVDeals.CoBuyerMobilePhone = reader["CoBuyerMobilePhone"].ToString();
                        CVDeals.CoBuyerWorkPhone = reader["CoBuyerWorkPhone"].ToString();
                        CVDeals.CoBuyerWorkPhoneExt = reader["CoBuyerWorkPhoneExt"].ToString();
                        CVDeals.CoBuyerPreferredContact = reader["CoBuyerPreferredContact"].ToString();
                        CVDeals.CoBuyerDoNotContactPhone = Convert.ToBoolean(reader["CoBuyerDoNotContactPhone"]);
                        CVDeals.CoBuyerDoNotContactEmail = Convert.ToBoolean(reader["CoBuyerDoNotContactEmail"]);
                        CVDeals.CoBuyerDoNotContactLetter = Convert.ToBoolean(reader["CoBuyerDoNotContactLetter"]);
                        CVDeals.VehicleSalesperson1_EmployeeCode = reader["VehicleSalesperson1_EmployeeCode"].ToString();
                        CVDeals.VehicleSalesperson1_FirstName = reader["VehicleSalesperson1_FirstName"].ToString();
                        CVDeals.VehicleSalesperson1_LastName = reader["VehicleSalesperson1_LastName"].ToString();
                        CVDeals.VehicleSalesperson2_EmployeeCode = reader["VehicleSalesperson2_EmployeeCode"].ToString();
                        CVDeals.VehicleSalesperson2_FirstName = reader["VehicleSalesperson2_FirstName"].ToString();
                        CVDeals.VehicleSalesperson2_LastName = reader["VehicleSalesperson2_LastName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson1_EmployeeCode = reader["FinanceInsuranceSalesperson1_EmployeeCode"].ToString();
                        CVDeals.FinanceInsuranceSalesperson1_FirstName = reader["FinanceInsuranceSalesperson1_FirstName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson1_LastName = reader["FinanceInsuranceSalesperson1_LastName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson2_EmployeeCode = reader["FinanceInsuranceSalesperson2_EmployeeCode"].ToString();
                        CVDeals.FinanceInsuranceSalesperson2_FirstName = reader["FinanceInsuranceSalesperson2_FirstName"].ToString();
                        CVDeals.FinanceInsuranceSalesperson2_LastName = reader["FinanceInsuranceSalesperson2_LastName"].ToString();
                        CVDeals.SalesManager_EmployeeCode = reader["SalesManager_EmployeeCode"].ToString();
                        CVDeals.SalesManager_FirstName = reader["SalesManager_FirstName"].ToString();
                        CVDeals.SalesManager_LastName = reader["SalesManager_LastName"].ToString();
                        CVDeals.Lienholder = reader["Lienholder"].ToString();
                        CVDeals.LoanAPR = Convert.ToDecimal(reader["LoanAPR"]);
                        CVDeals.LoanTerm = Convert.ToInt32(reader["LoanTerm"]);
                        CVDeals.LoanFirstPayDate = Convert.ToDateTime(reader["LoanFirstPayDate"]);
                        CVDeals.LoanPaymentsPerYear = Convert.ToInt32(reader["LoanPaymentsPerYear"]);
                        CVDeals.LoanPaymentAmount = Convert.ToDecimal(reader["LoanPaymentAmount"]);
                        CVDeals.DeallinkApplicationID = reader["DeallinkApplicationID"].ToString();
                        CVDeals.TotalRetailPrice = Convert.ToDecimal(reader["TotalRetailPrice"]);
                        CVDeals.TotalAccessoriesandLaborAmount = Convert.ToDecimal(reader["TotalAccessoriesandLaborAmount"]);
                        CVDeals.TotalAddOnAmount = Convert.ToDecimal(reader["TotalAddOnAmount"]);
                        CVDeals.TotalESPAmount = Convert.ToDecimal(reader["TotalESPAmount"]);
                        CVDeals.TotalPPMAmount = Convert.ToDecimal(reader["TotalPPMAmount"]);
                        CVDeals.TotalAccidentHealthAmount = Convert.ToDecimal(reader["TotalAccidentHealthAmount"]);
                        CVDeals.TotalCreditLifeAmount = Convert.ToDecimal(reader["TotalCreditLifeAmount"]);
                        CVDeals.TotalChargesandFeesAmount = Convert.ToDecimal(reader["TotalChargesandFeesAmount"]);
                        CVDeals.TotalVehicleTaxAmount = Convert.ToDecimal(reader["TotalVehicleTaxAmount"]);
                        CVDeals.TotalAccessoriesandLaborTaxAmount = Convert.ToDecimal(reader["TotalAccessoriesandLaborTaxAmount"]);
                        CVDeals.TotalSupplementalChargesTaxAmount = Convert.ToDecimal(reader["TotalSupplementalChargesTaxAmount"]);
                        CVDeals.TotalAccessoryFeesAmount = Convert.ToDecimal(reader["TotalAccessoryFeesAmount"]);
                        CVDeals.TotalDocStampsAmount = Convert.ToDecimal(reader["TotalDocStampsAmount"]);
                        CVDeals.TotalTaxAmount = Convert.ToDecimal(reader["TotalTaxAmount"]);
                        CVDeals.TotalTradeInAllowanceAmount = Convert.ToDecimal(reader["TotalTradeInAllowanceAmount"]);
                        CVDeals.TotalTradeInPayoffAmount = Convert.ToDecimal(reader["TotalTradeInPayoffAmount"]);
                        CVDeals.TotalTradeInActualCashValueAmount = Convert.ToDecimal(reader["TotalTradeInActualCashValueAmount"]);
                        CVDeals.TotalDepositAmount = Convert.ToDecimal(reader["TotalDepositAmount"]);
                        CVDeals.TotalDownPaymentAmount = Convert.ToDecimal(reader["TotalDownPaymentAmount"]);
                        CVDeals.TotalRebateAmount = Convert.ToDecimal(reader["TotalRebateAmount"]);
                        CVDeals.TotalFinancedAmount = Convert.ToDecimal(reader["TotalFinancedAmount"]);
                        CVDeals.TotalDueAmount = Convert.ToDecimal(reader["TotalDueAmount"]);
                        CVDeals.AddOnItemID = reader["AddOnItemID"].ToString();
                        CVDeals.AddOnItemName = reader["AddOnItemName"].ToString();
                        CVDeals.AddOnItemVendorID = reader["AddOnItemVendorID"].ToString();
                        CVDeals.AddOnItemStatus = reader["AddOnItemStatus"].ToString();
                        CVDeals.AddOnItemUnitPriceAmount = reader["AddOnItemUnitPriceAmount"].ToString();
                        CVDeals.AddOnItemQuantity = Convert.ToDecimal(reader["AddOnItemQuantity"]);
                        CVDeals.AddOnItemListPriceAmount = Convert.ToDecimal(reader["AddOnItemListPriceAmount"]);
                        CVDeals.AddOnItemDiscountAmount = Convert.ToDecimal(reader["AddOnItemDiscountAmount"]);
                        CVDeals.AddOnItemDiscountRate = Convert.ToDecimal(reader["AddOnItemDiscountRate"]);
                        CVDeals.AddOnItemSalesTaxAmount = Convert.ToDecimal(reader["AddOnItemSalesTaxAmount"]);
                        CVDeals.ChargeID = Convert.ToInt32(reader["ChargeID"]);
                        CVDeals.ChargeName = reader["ChargeName"].ToString();
                        CVDeals.ChargeVendorID = reader["ChargeVendorID"].ToString();
                        CVDeals.ChargeCostAmount = Convert.ToDecimal(reader["ChargeCostAmount"]);
                        CVDeals.ChargeListPriceAmount = Convert.ToDecimal(reader["ChargeListPriceAmount"]);
                        CVDeals.ChargeSalesTaxAmount = Convert.ToDecimal(reader["ChargeSalesTaxAmount"]);
                        CVDeals.TradeInVIN = reader["TradeInVIN"].ToString();
                        CVDeals.TradeInMake = reader["TradeInMake"].ToString();
                        CVDeals.TradeInModel = reader["TradeInModel"].ToString();
                        CVDeals.TradeInModelYear = Convert.ToInt32(reader["TradeInModelYear"]);
                        CVDeals.TradeInMileage = Convert.ToInt32(reader["TradeInMileage"]);
                        CVDeals.TradeInColor = reader["TradeInColor"].ToString();
                        CVDeals.TradeInFactoryInstalledOptions = reader["TradeInFactoryInstalledOptions"].ToString();
                        CVDeals.TradeInStockNumber = reader["TradeInStockNumber"].ToString();
                        CVDeals.TradeInAllowanceAmount = Convert.ToDecimal(reader["TradeInAllowanceAmount"]);
                        CVDeals.TradeInPayoffAmount = Convert.ToDecimal(reader["TradeInPayoffAmount"]);
                        CVDeals.TradeInActualCashValueAmount = Convert.ToDecimal(reader["TradeInActualCashValueAmount"]);

                        CVDlist.Add(CVDeals);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBClosedVehicleDeals in Database Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetClosedVehicleDeals list");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching of closed vehicle deals. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetClosedVehicleDeals";
                logdata.base_message = "Get ClosedVehicleDeals list";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return CVDlist;
        }
        private string CreateQueryCount()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();

            query.Append(" select count(*)");
            query.Append(" from view_Extract_ClosedVehicleDeals");
            query.Append(" where ClosedDateTime>@Date");

            return query.ToString();
        } 
        private string CreateQuery()
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select  ISNULL(DealID,0) DealID,ISNULL(DealStatus,'')DealStatus,ISNULL(CreateDAteTime,'1900-01-01')CreateDAteTime,ISNULL(ContractDateTime,'1900-01-01')ContractDateTime,ISNULL(ClosedDateTime,'1900-01-01')ClosedDateTime,ISNULL(DeliveryDateTime,'1900-01-01')DeliveryDateTime,ISNULL(InvoiceDateTime,'1900-01-01')InvoiceDateTime,ISNULL(InactiveDateTime,'1900-01-01')InactiveDateTime,ISNULL(VIN,'')VIN");
            query.Append(",ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(Mileage,0)Mileage,ISNULL(Color,'')Color,ISNULL(FactoryInstalledOptions,'')FactoryInstalledOptions,ISNULL([Pre-OwnedFlag],'')PreOwnedFlag,ISNULL(StockNumber,'')StockNumber,ISNULL(BuyerCustomerID,0)BuyerCustomerID ");
            query.Append(",ISNULL(BuyerFirstName,'')BuyerFirstName,ISNULL(BuyerLastName,'')BuyerLastName,ISNULL(BuyerMiddleName,'')BuyerMiddleName,ISNULL(BuyerCompanyName,'')BuyerCompanyName,ISNULL(BuyerNamePrefix,'')BuyerNamePrefix,ISNULL(BuyerNameSuffix,'')BuyerNameSuffix,ISNULL(BuyerAddress1,'')BuyerAddress1,ISNULL(BuyerAddress2,'')BuyerAddress2 ");
            query.Append(",ISNULL(BuyerCity,'')BuyerCity,ISNULL(BuyerState,'')BuyerState,ISNULL(BuyerPostalCode,'')BuyerPostalCode,ISNULL(BuyerPostalCodeSuffix,'')BuyerPostalCodeSuffix,ISNULL(BuyerCounty,'')BuyerCounty,ISNULL(BuyerCountry,'')BuyerCountry,ISNULL(BuyerEmailAddress,'')BuyerEmailAddress,ISNULL(BuyerHomePhone,'')BuyerHomePhone ");
            query.Append(",ISNULL(BuyerMobilePhone,'')BuyerMobilePhone,ISNULL(BuyerWorkPhone,'')BuyerWorkPhone,ISNULL(BuyerWorkPhoneExt,'')BuyerWorkPhoneExt,ISNULL(BuyerPreferredContact,'')BuyerPreferredContact,ISNULL(BuyerDoNotContactPhone,0)BuyerDoNotContactPhone,ISNULL(BuyerDoNotContactEmail,0)BuyerDoNotContactEmail");
            query.Append(",ISNULL(BuyerDoNotContactLetter,0)BuyerDoNotContactLetter,ISNULL(CoBuyerCustomerID,0)CoBuyerCustomerID,ISNULL(CoBuyerFirstName,'')CoBuyerFirstName,ISNULL(CoBuyerLastName,'')CoBuyerLastName,ISNULL(CoBuyerMiddleName,'')CoBuyerMiddleName,ISNULL(CoBuyerCompanyName,'')CoBuyerCompanyName");
            query.Append(",ISNULL(CoBuyerNamePrefix,'')CoBuyerNamePrefix,ISNULL(CoBuyerNameSuffix,'')CoBuyerNameSuffix,ISNULL(CoBuyerAddress1,'')CoBuyerAddress1,ISNULL(CoBuyerAddress2,'')CoBuyerAddress2,ISNULL(CoBuyerCity,'')CoBuyerCity,ISNULL(CoBuyerState,'')CoBuyerState,ISNULL(CoBuyerPostalCode,'')CoBuyerPostalCode");
            query.Append(",ISNULL(CoBuyerPostalCodeSuffix,'')CoBuyerPostalCodeSuffix,ISNULL(CoBuyerCounty,'')CoBuyerCounty,ISNULL(CoBuyerCountry,'')CoBuyerCountry,ISNULL(CoBuyerEmailAddress,'')CoBuyerEmailAddress,ISNULL(CoBuyerHomePhone,'')CoBuyerHomePhone,ISNULL(CoBuyerMobilePhone,'')CoBuyerMobilePhone");
            query.Append(",ISNULL(CoBuyerWorkPhone,'')CoBuyerWorkPhone,ISNULL(CoBuyerWorkPhoneExt,'')CoBuyerWorkPhoneExt,ISNULL(CoBuyerPreferredContact,'')CoBuyerPreferredContact,ISNULL(CoBuyerDoNotContactPhone,0)CoBuyerDoNotContactPhone,ISNULL(CoBuyerDoNotContactEmail,0)CoBuyerDoNotContactEmail");
            query.Append(",ISNULL(CoBuyerDoNotContactLetter,0)CoBuyerDoNotContactLetter,ISNULL(VehicleSalesperson1_EmployeeCode,'')VehicleSalesperson1_EmployeeCode,ISNULL(VehicleSalesperson1_FirstName,'')VehicleSalesperson1_FirstName,ISNULL(VehicleSalesperson1_LastName,'')VehicleSalesperson1_LastName");
            query.Append(",ISNULL(VehicleSalesperson2_EmployeeCode,'')VehicleSalesperson2_EmployeeCode,ISNULL(VehicleSalesperson2_FirstName,'')VehicleSalesperson2_FirstName,ISNULL(VehicleSalesperson2_LastName,'')VehicleSalesperson2_LastName,ISNULL(FinanceInsuranceSalesperson1_EmployeeCode,'')FinanceInsuranceSalesperson1_EmployeeCode");
            query.Append(",ISNULL(FinanceInsuranceSalesperson1_FirstName,'')FinanceInsuranceSalesperson1_FirstName,ISNULL(FinanceInsuranceSalesperson1_LastName,'')FinanceInsuranceSalesperson1_LastName,ISNULL(FinanceInsuranceSalesperson2_EmployeeCode,'')FinanceInsuranceSalesperson2_EmployeeCode");
            query.Append(",ISNULL(FinanceInsuranceSalesperson2_FirstName,'')FinanceInsuranceSalesperson2_FirstName,ISNULL(FinanceInsuranceSalesperson2_LastName,'')FinanceInsuranceSalesperson2_LastName,ISNULL(SalesManager_EmployeeCode,'')SalesManager_EmployeeCode");
            query.Append(",ISNULL(SalesManager_FirstName,'')SalesManager_FirstName,ISNULL(SalesManager_LastName,'')SalesManager_LastName,ISNULL(Leinholder,'')Lienholder,ISNULL(LoanAPR,0)LoanAPR,ISNULL(LoanTerm,0)LoanTerm,ISNULL(LoanFirstPayDate,'1900-01-01')LoanFirstPayDate,ISNULL(LoanPaymentsPerYear,0)LoanPaymentsPerYear");
            query.Append(",ISNULL(LoanPaymentAmount,0)LoanPaymentAmount,ISNULL(DeallinkApplicationID,'')DeallinkApplicationID,ISNULL(TotalRetailPrice,0)TotalRetailPrice,ISNULL(TotalAccessoriesandLaborAmount,0)TotalAccessoriesandLaborAmount,ISNULL(TotalAddOnAmount,0)TotalAddOnAmount");
            query.Append(",ISNULL(TotalESPAmount,0)TotalESPAmount,ISNULL(TotalPPMAmount,0)TotalPPMAmount,ISNULL(TotalAccidentHealthAmount,0)TotalAccidentHealthAmount,ISNULL(TotalCreditLifeAmount,0)TotalCreditLifeAmount,ISNULL(TotalChargesandFeesAmount,0)TotalChargesandFeesAmount,ISNULL(TotalVehicleTaxAmount,0)TotalVehicleTaxAmount");
            query.Append(",ISNULL(TotalAccessoriesandLaborTaxAmount,0)TotalAccessoriesandLaborTaxAmount,ISNULL(TotalSupplementalChargesTaxAmount,0)TotalSupplementalChargesTaxAmount,ISNULL(TotalAccessoryFeesAmount,0)TotalAccessoryFeesAmount,ISNULL(TotalDocStampsAmount,0)TotalDocStampsAmount");
            query.Append(",ISNULL(TotalTaxAmount,0)TotalTaxAmount,ISNULL(TotalTradeInAllowanceAmount,0)TotalTradeInAllowanceAmount,ISNULL(TotalTradeInPayoffAmount,0)TotalTradeInPayoffAmount,ISNULL(TotalTradeInActualCashValueAmount,0)TotalTradeInActualCashValueAmount,ISNULL(TotalDepositAmount,0)TotalDepositAmount");
            query.Append(",ISNULL(TotalDownPaymentAmount,0)TotalDownPaymentAmount,ISNULL(TotalRebateAmount,0)TotalRebateAmount,ISNULL(TotalFinancedAmount,0)TotalFinancedAmount,ISNULL(TotalDueAmount,0)TotalDueAmount,ISNULL(AddOnItemID,'')AddOnItemID,ISNULL(AddOnItemName,'')AddOnItemName,ISNULL(AddOnItemVendorID,'')AddOnItemVendorID");
            query.Append(",ISNULL(AddOnItemStatus,'')AddOnItemStatus,ISNULL(AddOnItemUnitPriceAmount,'')AddOnItemUnitPriceAmount,ISNULL(AddOnItemQuantity,0)AddOnItemQuantity,ISNULL(AddOnItemListPriceAmount,'')AddOnItemListPriceAmount,ISNULL(AddOnItemDiscountAmount,0)AddOnItemDiscountAmount");
            query.Append(",ISNULL(AddOnItemDiscountRate,0)AddOnItemDiscountRate,ISNULL(AddOnItemSalesTaxAmount,0)AddOnItemSalesTaxAmount,ISNULL(ChargeID,0)ChargeID,ISNULL(ChargeName,'')ChargeName,ISNULL(ChargeVendorID,'')ChargeVendorID,ISNULL(ChargeCostAmount,0)ChargeCostAmount,ISNULL(ChargeListPriceAmount,0)ChargeListPriceAmount");
            query.Append(",ISNULL(ChargeSalesTaxAmount,0)ChargeSalesTaxAmount,ISNULL(TradeInVIN,'')TradeInVIN,ISNULL(TradeInMake,'')TradeInMake,ISNULL(TradeInModel,'')TradeInModel,ISNULL(TradeInModelYear,0)TradeInModelYear,ISNULL(TradeInMileage,0)TradeInMileage,ISNULL(TradeInColor,'')TradeInColor");
            query.Append(",ISNULL(TradeInFactoryInstalledOptions,'')TradeInFactoryInstalledOptions,ISNULL(TradeInStockNumber,'')TradeInStockNumber,ISNULL(TradeInAllowanceAmount,0)TradeInAllowanceAmount,ISNULL(TradeInPayoffAmount,0)TradeInPayoffAmount,ISNULL(TradeInActualCashValueAmount,0)TradeInActualCashValueAmount");
            query.Append(" from view_Extract_ClosedVehicleDeals");
            // query.Append(" from [dbo].[ClosedVDeals]");
            query.Append(" where ClosedDateTime>@Date");

            return query.ToString();
        }
        private string CreateQuery(int count, int pageSize)
        {
            //Prepare query from views as required
            StringBuilder query = new StringBuilder();
            query.Append("select ISNULL(DealID,0) DealID,ISNULL(DealStatus,'')DealStatus,ISNULL(CreateDAteTime,'1900-01-01')CreateDAteTime,ISNULL(ContractDateTime,'1900-01-01')ContractDateTime,ISNULL(ClosedDateTime,'1900-01-01')ClosedDateTime,ISNULL(DeliveryDateTime,'1900-01-01')DeliveryDateTime,ISNULL(InvoiceDateTime,'1900-01-01')InvoiceDateTime,ISNULL(InactiveDateTime,'1900-01-01')InactiveDateTime,ISNULL(VIN,'')VIN");
            query.Append(",ISNULL(Make,'')Make,ISNULL(Model,'')Model,ISNULL(ModelYear,0)ModelYear,ISNULL(Mileage,0)Mileage,ISNULL(Color,'')Color,ISNULL(FactoryInstalledOptions,'')FactoryInstalledOptions,ISNULL([Pre-OwnedFlag],'')PreOwnedFlag,ISNULL(StockNumber,'')StockNumber,ISNULL(BuyerCustomerID,0)BuyerCustomerID ");
            query.Append(",ISNULL(BuyerFirstName,'')BuyerFirstName,ISNULL(BuyerLastName,'')BuyerLastName,ISNULL(BuyerMiddleName,'')BuyerMiddleName,ISNULL(BuyerCompanyName,'')BuyerCompanyName,ISNULL(BuyerNamePrefix,'')BuyerNamePrefix,ISNULL(BuyerNameSuffix,'')BuyerNameSuffix,ISNULL(BuyerAddress1,'')BuyerAddress1,ISNULL(BuyerAddress2,'')BuyerAddress2 ");
            query.Append(",ISNULL(BuyerCity,'')BuyerCity,ISNULL(BuyerState,'')BuyerState,ISNULL(BuyerPostalCode,'')BuyerPostalCode,ISNULL(BuyerPostalCodeSuffix,'')BuyerPostalCodeSuffix,ISNULL(BuyerCounty,'')BuyerCounty,ISNULL(BuyerCountry,'')BuyerCountry,ISNULL(BuyerEmailAddress,'')BuyerEmailAddress,ISNULL(BuyerHomePhone,'')BuyerHomePhone ");
            query.Append(",ISNULL(BuyerMobilePhone,'')BuyerMobilePhone,ISNULL(BuyerWorkPhone,'')BuyerWorkPhone,ISNULL(BuyerWorkPhoneExt,'')BuyerWorkPhoneExt,ISNULL(BuyerPreferredContact,'')BuyerPreferredContact,ISNULL(BuyerDoNotContactPhone,0)BuyerDoNotContactPhone,ISNULL(BuyerDoNotContactEmail,0)BuyerDoNotContactEmail");
            query.Append(",ISNULL(BuyerDoNotContactLetter,0)BuyerDoNotContactLetter,ISNULL(CoBuyerCustomerID,0)CoBuyerCustomerID,ISNULL(CoBuyerFirstName,'')CoBuyerFirstName,ISNULL(CoBuyerLastName,'')CoBuyerLastName,ISNULL(CoBuyerMiddleName,'')CoBuyerMiddleName,ISNULL(CoBuyerCompanyName,'')CoBuyerCompanyName");
            query.Append(",ISNULL(CoBuyerNamePrefix,'')CoBuyerNamePrefix,ISNULL(CoBuyerNameSuffix,'')CoBuyerNameSuffix,ISNULL(CoBuyerAddress1,'')CoBuyerAddress1,ISNULL(CoBuyerAddress2,'')CoBuyerAddress2,ISNULL(CoBuyerCity,'')CoBuyerCity,ISNULL(CoBuyerState,'')CoBuyerState,ISNULL(CoBuyerPostalCode,'')CoBuyerPostalCode");
            query.Append(",ISNULL(CoBuyerPostalCodeSuffix,'')CoBuyerPostalCodeSuffix,ISNULL(CoBuyerCounty,'')CoBuyerCounty,ISNULL(CoBuyerCountry,'')CoBuyerCountry,ISNULL(CoBuyerEmailAddress,'')CoBuyerEmailAddress,ISNULL(CoBuyerHomePhone,'')CoBuyerHomePhone,ISNULL(CoBuyerMobilePhone,'')CoBuyerMobilePhone");
            query.Append(",ISNULL(CoBuyerWorkPhone,'')CoBuyerWorkPhone,ISNULL(CoBuyerWorkPhoneExt,'')CoBuyerWorkPhoneExt,ISNULL(CoBuyerPreferredContact,'')CoBuyerPreferredContact,ISNULL(CoBuyerDoNotContactPhone,0)CoBuyerDoNotContactPhone,ISNULL(CoBuyerDoNotContactEmail,0)CoBuyerDoNotContactEmail");
            query.Append(",ISNULL(CoBuyerDoNotContactLetter,0)CoBuyerDoNotContactLetter,ISNULL(VehicleSalesperson1_EmployeeCode,'')VehicleSalesperson1_EmployeeCode,ISNULL(VehicleSalesperson1_FirstName,'')VehicleSalesperson1_FirstName,ISNULL(VehicleSalesperson1_LastName,'')VehicleSalesperson1_LastName");
            query.Append(",ISNULL(VehicleSalesperson2_EmployeeCode,'')VehicleSalesperson2_EmployeeCode,ISNULL(VehicleSalesperson2_FirstName,'')VehicleSalesperson2_FirstName,ISNULL(VehicleSalesperson2_LastName,'')VehicleSalesperson2_LastName,ISNULL(FinanceInsuranceSalesperson1_EmployeeCode,'')FinanceInsuranceSalesperson1_EmployeeCode");
            query.Append(",ISNULL(FinanceInsuranceSalesperson1_FirstName,'')FinanceInsuranceSalesperson1_FirstName,ISNULL(FinanceInsuranceSalesperson1_LastName,'')FinanceInsuranceSalesperson1_LastName,ISNULL(FinanceInsuranceSalesperson2_EmployeeCode,'')FinanceInsuranceSalesperson2_EmployeeCode");
            query.Append(",ISNULL(FinanceInsuranceSalesperson2_FirstName,'')FinanceInsuranceSalesperson2_FirstName,ISNULL(FinanceInsuranceSalesperson2_LastName,'')FinanceInsuranceSalesperson2_LastName,ISNULL(SalesManager_EmployeeCode,'')SalesManager_EmployeeCode");
            query.Append(",ISNULL(SalesManager_FirstName,'')SalesManager_FirstName,ISNULL(SalesManager_LastName,'')SalesManager_LastName,ISNULL(Leinholder,'')Lienholder,ISNULL(LoanAPR,0)LoanAPR,ISNULL(LoanTerm,0)LoanTerm,ISNULL(LoanFirstPayDate,'1900-01-01')LoanFirstPayDate,ISNULL(LoanPaymentsPerYear,0)LoanPaymentsPerYear");
            query.Append(",ISNULL(LoanPaymentAmount,0)LoanPaymentAmount,ISNULL(DeallinkApplicationID,'')DeallinkApplicationID,ISNULL(TotalRetailPrice,0)TotalRetailPrice,ISNULL(TotalAccessoriesandLaborAmount,0)TotalAccessoriesandLaborAmount,ISNULL(TotalAddOnAmount,0)TotalAddOnAmount");
            query.Append(",ISNULL(TotalESPAmount,0)TotalESPAmount,ISNULL(TotalPPMAmount,0)TotalPPMAmount,ISNULL(TotalAccidentHealthAmount,0)TotalAccidentHealthAmount,ISNULL(TotalCreditLifeAmount,0)TotalCreditLifeAmount,ISNULL(TotalChargesandFeesAmount,0)TotalChargesandFeesAmount,ISNULL(TotalVehicleTaxAmount,0)TotalVehicleTaxAmount");
            query.Append(",ISNULL(TotalAccessoriesandLaborTaxAmount,0)TotalAccessoriesandLaborTaxAmount,ISNULL(TotalSupplementalChargesTaxAmount,0)TotalSupplementalChargesTaxAmount,ISNULL(TotalAccessoryFeesAmount,0)TotalAccessoryFeesAmount,ISNULL(TotalDocStampsAmount,0)TotalDocStampsAmount");
            query.Append(",ISNULL(TotalTaxAmount,0)TotalTaxAmount,ISNULL(TotalTradeInAllowanceAmount,0)TotalTradeInAllowanceAmount,ISNULL(TotalTradeInPayoffAmount,0)TotalTradeInPayoffAmount,ISNULL(TotalTradeInActualCashValueAmount,0)TotalTradeInActualCashValueAmount,ISNULL(TotalDepositAmount,0)TotalDepositAmount");
            query.Append(",ISNULL(TotalDownPaymentAmount,0)TotalDownPaymentAmount,ISNULL(TotalRebateAmount,0)TotalRebateAmount,ISNULL(TotalFinancedAmount,0)TotalFinancedAmount,ISNULL(TotalDueAmount,0)TotalDueAmount,ISNULL(AddOnItemID,'')AddOnItemID,ISNULL(AddOnItemName,'')AddOnItemName,ISNULL(AddOnItemVendorID,'')AddOnItemVendorID");
            query.Append(",ISNULL(AddOnItemStatus,'')AddOnItemStatus,ISNULL(AddOnItemUnitPriceAmount,'')AddOnItemUnitPriceAmount,ISNULL(AddOnItemQuantity,0)AddOnItemQuantity,ISNULL(AddOnItemListPriceAmount,'')AddOnItemListPriceAmount,ISNULL(AddOnItemDiscountAmount,0)AddOnItemDiscountAmount");
            query.Append(",ISNULL(AddOnItemDiscountRate,0)AddOnItemDiscountRate,ISNULL(AddOnItemSalesTaxAmount,0)AddOnItemSalesTaxAmount,ISNULL(ChargeID,0)ChargeID,ISNULL(ChargeName,'')ChargeName,ISNULL(ChargeVendorID,'')ChargeVendorID,ISNULL(ChargeCostAmount,0)ChargeCostAmount,ISNULL(ChargeListPriceAmount,0)ChargeListPriceAmount");
            query.Append(",ISNULL(ChargeSalesTaxAmount,0)ChargeSalesTaxAmount,ISNULL(TradeInVIN,'')TradeInVIN,ISNULL(TradeInMake,'')TradeInMake,ISNULL(TradeInModel,'')TradeInModel,ISNULL(TradeInModelYear,0)TradeInModelYear,ISNULL(TradeInMileage,0)TradeInMileage,ISNULL(TradeInColor,'')TradeInColor");
            query.Append(",ISNULL(TradeInFactoryInstalledOptions,'')TradeInFactoryInstalledOptions,ISNULL(TradeInStockNumber,'')TradeInStockNumber,ISNULL(TradeInAllowanceAmount,0)TradeInAllowanceAmount,ISNULL(TradeInPayoffAmount,0)TradeInPayoffAmount,ISNULL(TradeInActualCashValueAmount,0)TradeInActualCashValueAmount");

            query.Append(" from view_Extract_ClosedVehicleDeals");
            // query.Append(" from [dbo].[ClosedVDeals]"); 
            query.Append(" where ClosedDateTime>@Date");
            query.Append(" ORDER BY DealID");
            query.Append(" OFFSET @PageSize * (@PageNumber - 1) ROWS");
            query.Append(" FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)");


            return query.ToString();
        }

    }
}
