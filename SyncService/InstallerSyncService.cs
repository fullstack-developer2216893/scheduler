﻿using Common;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32.TaskScheduler;

namespace SyncService
{
    [RunInstaller(true)]
    public partial class InstallerSyncService : System.Configuration.Install.Installer
    {
        public InstallerSyncService()
        {
            InitializeComponent();
        }
        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);

            try
            {

                AddConfigurationFileDetails();
                TaskScheduler();
            }
            catch (Exception e)
            {
                //   MessageBox.Show("Falha ao atualizar o arquivo de configuração da aplicação: " + e.Message);
                DataHandler DH = new DataHandler();
                DH.SetErrorLog(e.Message);
                base.Rollback(savedState);
            }
        }

        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }

        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);

            using (TaskService ts = new TaskService())
            {
                //Delete the scheduled task

                if (ts.GetTask("Talon_SyncService_Scheduler") != null)
                {
                    ts.RootFolder.DeleteTask("Talon_SyncService_Scheduler");
                }

            }
        }

        private void showParameters()
        {
            StringBuilder sb = new StringBuilder();
            StringDictionary myStringDictionary = this.Context.Parameters;
            if (this.Context.Parameters.Count > 0)
            {
                foreach (string myString in this.Context.Parameters.Keys)
                {
                    sb.AppendFormat("String={0} Value= {1}\n", myString,
                    this.Context.Parameters[myString]);
                }
            }
            MessageBox.Show(sb.ToString());

        }

        private void AddConfigurationFileDetails()
        {
            try
            {


                //string DESTINATION_PATH = Context.Parameters["DESTINATION_PATH"];

                // Get the path to the executable file that is being installed on the target computer  
                string assemblypath = Context.Parameters["assemblypath"];

                string appConfigPath = assemblypath + ".config";
                string DESTINATION_PATH = Path.GetDirectoryName(assemblypath);

                // Write the path to the app.config file  
                XmlDocument doc = new XmlDocument();
                doc.Load(appConfigPath);

                XmlNode configuration = null;
                foreach (XmlNode node in doc.ChildNodes)
                    if (node.Name == "configuration")
                        configuration = node;

                if (configuration != null)
                {
                    //MessageBox.Show("configuration != null");  
                    // Get the ‘appSettings’ node  
                    XmlNode settingNode = null;
                    foreach (XmlNode node in configuration.ChildNodes)
                    {
                        if (node.Name == "appSettings")
                            settingNode = node;
                    }

                    if (settingNode != null)
                    {
                        //MessageBox.Show("settingNode != null");  
                        //Reassign values in the config file  
                        foreach (XmlNode node in settingNode.ChildNodes)
                        {
                            //MessageBox.Show("node.Value = " + node.Value);  
                            if (node.Attributes == null)
                                continue;
                            XmlAttribute attribute = node.Attributes["value"];
                            //MessageBox.Show("attribute != null ");  
                            //MessageBox.Show("node.Attributes['value'] = " + node.Attributes["value"].Value);  
                            if (node.Attributes["key"] != null)
                            {
                                //MessageBox.Show("node.Attributes['key'] != null ");  
                                //MessageBox.Show("node.Attributes['key'] = " + node.Attributes["key"].Value);  
                                switch (node.Attributes["key"].Value)
                                {
                                    case "DESTINATION_PATH":
                                        attribute.Value = DESTINATION_PATH;
                                        break;
                                }
                            }
                        }
                    }
                    doc.Save(appConfigPath);
                }
            }
            catch
            {
                throw;
            }
        }
        private void TaskScheduler()

        {
            string assemblypath = Context.Parameters["assemblypath"];
            // Run daily

            using (TaskService ts = new TaskService())

            {

                TaskDefinition td = ts.NewTask();

                td.RegistrationInfo.Description = "It will run continuously for 15 minutes interval daily.";

                // Run Task whether user logged on or not
                td.Principal.LogonType = TaskLogonType.S4U;

                // Run as Administrator
                td.Principal.RunLevel = TaskRunLevel.Highest;

                DailyTrigger trigger = new DailyTrigger();

                trigger.StartBoundary = DateTime.Now;

                trigger.Repetition.Interval = TimeSpan.FromMinutes(15);
                trigger.ExecutionTimeLimit = TimeSpan.FromMinutes(180);
                td.Triggers.Add(trigger);



                td.Actions.Add(new ExecAction(assemblypath, null, null));

                ts.RootFolder.RegisterTaskDefinition("Talon_SyncService_Scheduler", td);

                //Run the scheduled task
                Task task = ts.FindTask("Talon_SyncService_Scheduler");
                task.Run();
            }

        }
    }
}
