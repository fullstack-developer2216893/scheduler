﻿using Common;
using Common.Model;
using DatabaseCommunication;
using DP360CRMCommunication;
using DP360CRMCommunication.Model;
using SyncService.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SyncService
{
    public class CustomerSync
    {
        public async Task<List<SyncResult>> Sync(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            string token = Global.Token;
            SyncResult result = new Model.SyncResult();
            List<SyncResult> resultList = new List<SyncResult>();

            int recordcount = 0;
            int failedcount = 0;

            //DBview Customer List
            List<Customer> CustomerList = new List<Customer>();
            try
            {


                CustomerList = await new DBCustomer().GetCustomer(Credentials, LastExecutionInfo);
                recordcount = CustomerList.Count;
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "DBview Customer List";
                logdata.base_message = "CustomerSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata); 
            }
            try
            {
                int entrycount = 0;
                List<Leads> APICheckList = new List<Leads>();
                foreach (Customer item in CustomerList)
                {
                    if (entrycount < Constants.LeadAPILoadCount)
                    {
                        APICheckList.Add(MAPLeads(item));
                        entrycount++;
                    }
                    else
                    {
                        //call async api to save Customer in CRM
                        int resCount = await new BLLeads().Set(APICheckList);
                        APICheckList = new List<Leads>();
                        APICheckList.Add(MAPLeads(item));
                        entrycount = 1;
                        //if failed then increase failedcount, and save the inventory ids in result object failed id list
                        if (resCount <= 0)
                        {
                            failedcount++;
                            // result.FailedIDList.Add();
                        }
                    }
                }
                if (APICheckList.Count > 0)
                {
                    int resCount = await new BLLeads().Set(APICheckList);
                    //if failed then increase failedcount, and save the inventory ids in result object failed id list           
                    if (resCount <= 0)
                    {
                        failedcount++;
                        //  result.FailedIDList.Add();
                    }
                }


                result.ErrorRecordCount = failedcount;
                result.InsertedRecordCount = recordcount - failedcount;
                result.API = Constants.Customer;
                result.ExecutionDateTime = DateTime.Now;
                result.ExecutionCount = recordcount;
                result.Message = "Customer Synced successfully";
                result.Method = "Insert";
                resultList.Add(result);
            }
            catch (Exception ex)
            {
               

                ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Customer Sync.";
                ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;

                DataHandler DH = new DataHandler();
                 DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "call async api to save Customer in CRM";
                logdata.base_message = "CustomerSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            return resultList;
        }

        private Leads MAPLeads(Customer cust)
        {
            Leads leads = new Leads();
            leads.dealer_id = Global.DealerID;
            leads.lead.contact.first_name = cust.FirstName;
            leads.lead.contact.last_name = cust.LastName;
            leads.lead.contact.middle_name = cust.MiddleName;
            leads.lead.contact.address1 = cust.Address1;
            leads.lead.contact.address2 = cust.Address2;
            leads.lead.contact.city = cust.City;
            leads.lead.contact.state = cust.State;
            leads.lead.contact.zip = cust.PostalCode;
            leads.lead.contact.county = cust.County;
            leads.lead.contact.email = cust.EmailAddress;
            //leads.lead.contact.phone = Convert.ToInt32(cust.HomePhone);
            //leads.lead.contact.mobile = Convert.ToInt32(cust.MobilePhone);
            //leads.lead.contact.work_number = Convert.ToInt32(cust.WorkPhone);
            leads.lead.contact.phone = cust.HomePhone;
            leads.lead.contact.mobile = cust.MobilePhone;
            leads.lead.contact.work_number = cust.WorkPhone;
            leads.lead.contact.dob = cust.BirthDate;
            leads.lead.lead_type = cust.CustomerType;
            leads.lead.product.vin = cust.VIN;
            leads.lead.product.make = cust.Make;
            leads.lead.product.model = cust.Model;
            leads.lead.product.year = cust.ModelYear;
            // leads.lead.product.mileage = cust.Mileage;
            leads.lead.contact.lead_status = null;
            leads.lead.contact.sales_step = null;
            leads.lead.contact.status = null;
            return leads;
        }
    }
}
