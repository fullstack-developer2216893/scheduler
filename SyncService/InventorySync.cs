﻿using SyncService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseCommunication;
using Common;
using DP360CRMCommunication.Model;
using DP360CRMCommunication;
using Common.Model;


namespace SyncService
{
    public class InventorySync
    {
        public async Task<List<SyncResult>> Sync(DBCredentials Credentials)
        {
            string token = Global.Token;
            SyncResult result = new Model.SyncResult();
            List<SyncResult> resultList = new List<SyncResult>();

            int failedcount = 0;
            int insertedcount = 0;
            string message = "";
            int passcount = 0;
            int recordcount = 0;
            int count = 0;
            List<string> FailedList = new List<string>();
            List<string> duplicateVinsList = new List<string>();
            List<string> existingVinsList = new List<string>();
            List<string> NotAddedVinsList = new List<string>();
            IEnumerable<Inventory> NewEntries = null;
            IEnumerable<CRMInventory> DeactivateEntries = null;
            IEnumerable<Inventory> UpdateEntries = null;

            List<Inventory> InventoryList = new List<Inventory>();
            List<CRMInventory> APIInvetoryList = new List<CRMInventory>();

            try
            {
                //DBview Inventory List                
                InventoryList = await new DBInventory().GetInventory(Credentials);


                recordcount = InventoryList.Count;
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.WriteToDBLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "DBview Inventory List";
                logdata.base_message = "InventorySync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            try
            {
                //Get inventory from CRM
                APIInvetoryList = await new BLInventory().GetList();
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Get inventory from CRM";
                logdata.base_message = "InventorySync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            try
            {


                if (InventoryList.Count > 0)
                {
                    if (APIInvetoryList.Count > 0)
                    {
                        //List of new entries which needs to be posted to CRM
                        NewEntries = from entries in InventoryList.Where(l => l.inventoryStatus == "A")
                                     where !APIInvetoryList.Any(x => x.vin == entries.vin)
                                     select entries;

                        //List of entries for which status needs to be set to inactive
                        DeactivateEntries = from apientries in APIInvetoryList.Where(a => a.hidden != 1 && a.sold != true)
                                            where !InventoryList.Any(x => x.vin == apientries.vin)
                                            select apientries;

                        //List of  entries which needs to be updated to CRM
                        UpdateEntries = from entries in InventoryList
                                        where APIInvetoryList.Any(x => x.vin == entries.vin)
                                        select entries;
                    }
                    else
                    {
                        NewEntries = InventoryList;
                    }
                }
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Getting list for insert,update and deactivate";
                logdata.base_message = "InventorySync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            //Post new entries
          x: if (NewEntries.Count() > 0)
            {
                try
                {
                    int entrycount = 0;
                    List<CRMInventory> APICheckList = new List<CRMInventory>();
                    InventorySyncResponse InvResponse = new InventorySyncResponse();
                    foreach (Inventory inventory in NewEntries)
                    {
                        if (entrycount < Constants.InventoryAPILoadCount)
                        {
                            APICheckList.Add(MapInventory(inventory, "Insert", APIInvetoryList));
                            entrycount++;
                        }
                        else
                        {
                            //call async api to save inventoris in CRM

                            InvResponse = await new BLInventory().Set(APICheckList);
                            APICheckList = new List<CRMInventory>();
                            APICheckList.Add(MapInventory(inventory, "Insert", APIInvetoryList));
                            entrycount = 1;
                            passcount++;
                            message = message + CreateMessage(passcount, InvResponse.message);

                            if (InvResponse.data.AddedVins.Count > 0)
                            {
                                insertedcount = insertedcount + InvResponse.data.AddedVins.Count();
                            }
                            if (InvResponse.data.NotAddedVins.Count > 0)
                            {
                                failedcount = failedcount + InvResponse.data.NotAddedVins.Count();
                                NotAddedVinsList.AddRange(InvResponse.data.NotAddedVins);
                                duplicateVinsList.AddRange(InvResponse.data.duplicateVins);
                                existingVinsList.AddRange(InvResponse.data.existingVins);
                            }
                        }
                    }
                    if (APICheckList.Count > 0)
                    {
                        InvResponse = await new BLInventory().Set(APICheckList);

                        passcount++;
                        message = message + CreateMessage(passcount, InvResponse.message);

                        if (InvResponse.data.AddedVins.Count > 0)
                        {
                            insertedcount = insertedcount + InvResponse.data.AddedVins.Count();
                        }
                        if (InvResponse.data.NotAddedVins.Count > 0)
                        {
                            failedcount = failedcount + InvResponse.data.NotAddedVins.Count();
                            NotAddedVinsList.AddRange(InvResponse.data.NotAddedVins);
                            duplicateVinsList.AddRange(InvResponse.data.duplicateVins);
                            existingVinsList.AddRange(InvResponse.data.existingVins);
                        }
                    }
                    //Geting the list of failed vins to be inserted
                    if(count<1)
                    {
                        count++;
                        FailedList = NotAddedVinsList.Where(i => !duplicateVinsList.Contains(i)).ToList();
                        FailedList = FailedList.Where(i => !existingVinsList.Contains(i)).ToList();
                        NewEntries = NewEntries.Where(i => FailedList.Contains(i.vin)).ToList();
                        goto x;
                    }
                  
                    result = new Model.SyncResult();
                    result.ErrorRecordCount = failedcount;
                    result.InsertedRecordCount = insertedcount;
                    result.API = Constants.Inventory;
                    result.ExecutionDateTime = DateTime.Now;
                    result.Message = message;
                    result.Method = "Insert";
                    result.FailedIDList = FailedList;
                    result.UpdatedRecordCount = 0;
                    result.ExecutionCount = passcount;
                    result.ExecutionStatus = 1;

                    resultList.Add(result);
                }
                catch (Exception ex)
                {
                    ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Inventory insert sync.";
                    ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;
                    ex.Data[Common.Helper.LogDataKey.AdditionalInformation] += Environment.NewLine + message;

                    DataHandler DH = new DataHandler();
                    DH.WriteToLog(ex);

                    CRMLogData logdata = new CRMLogData();
                    logdata.id = null;
                    logdata.log_type = "Error";
                    logdata.method_name = "Post new entries";
                    logdata.base_message = "InventorySync";
                    logdata.exception_message = ex.Message;
                    logdata.parameter_name = "no parameter";
                    logdata.stack_trace = ex.StackTrace;
                    //logdata.log_time = DateTime.Now;
                    //logdata.created = DateTime.Now;
                    //logdata.modified = DateTime.Now;

                    CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                    logdataResponse = new BLLogData().Set(logdata);
                }
            }

            /* */
            //deactivate existing entries (update status)
             if (DeactivateEntries.Count() > 0)
             {
                 try
                 {
                     insertedcount = 0;
                     failedcount = 0;
                     FailedList = new List<string>();
                     passcount = 0;
                     message = "";
                     int entrycount = 0;
                     List<CRMInventory> DeactivateList = new List<CRMInventory>();
                     InventorySyncResponse InvResponse = new InventorySyncResponse();
                     foreach (CRMInventory inventory in DeactivateEntries)
                     {
                         if (entrycount < Constants.InventoryAPILoadCount)
                         {
                             inventory.hidden = 1;
                             inventory.sold = true;
                             if (inventory.suggested_price == "")
                             {
                                 inventory.suggested_price = "0.00";
                             }

                             inventory.unit_status = "C";


                             //inventory.inv_id =Convert.ToInt32(inventory.vin);
                             DeactivateList.Add(inventory);
                             entrycount++;
                         }
                         else
                         {

                             InvResponse = await new BLInventory().Put(DeactivateList, Credentials.dealer_id);
                             DeactivateList = new List<CRMInventory>();
                             inventory.hidden = 1;
                             inventory.sold = true;
                             if (inventory.suggested_price == "")
                             {
                                 inventory.suggested_price = "0.00";
                             }

                             inventory.unit_status = "C";

                             // inventory.inv_id = Convert.ToInt32(inventory.vin);
                             DeactivateList.Add(inventory);
                             entrycount = 1;

                             passcount++;
                             message = message + CreateMessage(passcount, InvResponse.message);

                             if (InvResponse.data.AddedVins.Count > 0)
                             {
                                 insertedcount = insertedcount + InvResponse.data.AddedVins.Count();
                             }
                             if (InvResponse.data.NotAddedVins.Count > 0) 
                             {
                                 failedcount = failedcount + InvResponse.data.NotAddedVins.Count();
                                 FailedList.AddRange(InvResponse.data.NotAddedVins);
                             }
                         }

                     }
                     if (DeactivateList.Count > 0)
                     {

                         InvResponse = await new BLInventory().Put(DeactivateList, Credentials.dealer_id);

                         passcount++;
                         message = message + CreateMessage(passcount, InvResponse.message);

                         if (InvResponse.data.AddedVins.Count > 0)
                         {
                             insertedcount = insertedcount + InvResponse.data.AddedVins.Count();
                         }
                         if (InvResponse.data.NotAddedVins.Count > 0)
                         {
                             failedcount = failedcount + InvResponse.data.NotAddedVins.Count();
                             FailedList.AddRange(InvResponse.data.NotAddedVins);
                         }
                     }
                     result = new Model.SyncResult();
                     result.ErrorRecordCount = failedcount;
                     result.InsertedRecordCount = 0;
                     result.API = Constants.Inventory;
                     result.ExecutionDateTime = DateTime.Now;
                     result.Message = message;
                     result.Method = "Deactivate";
                     result.FailedIDList = FailedList;
                     result.UpdatedRecordCount = insertedcount;
                     result.ExecutionCount = passcount;
                     result.ExecutionStatus = 1;

                     resultList.Add(result);
              
                 }
                 catch (Exception ex)
                 {
                     ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Inventory deactivate.";
                     ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;
                     ex.Data[Common.Helper.LogDataKey.AdditionalInformation] += Environment.NewLine + message;

                     DataHandler DH = new DataHandler();
                     DH.WriteToLog(ex);

                     CRMLogData logdata = new CRMLogData();
                     logdata.id = null;
                     logdata.log_type = "Error";
                     logdata.method_name = "deactivate existing entries";
                     logdata.base_message = "InventorySync";
                     logdata.exception_message = ex.Message;
                     logdata.parameter_name = "no parameter";
                     logdata.stack_trace = ex.StackTrace;
                     //logdata.log_time = DateTime.Now;
                     //logdata.created = DateTime.Now;
                     //logdata.modified = DateTime.Now;

                     CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                     logdataResponse = new BLLogData().Set(logdata);
                 }
             }
            //Update existing entries
            if (UpdateEntries.Count() > 0)
            {
                try
                {
                    insertedcount = 0;
                    failedcount = 0;
                    FailedList = new List<string>();
                    passcount = 0;
                    message = "";
                    if (UpdateEntries != null)
                    {
                        int entrycount = 0;
                        List<CRMInventory> UpdateList = new List<CRMInventory>();
                        InventorySyncResponse InvResponse = new InventorySyncResponse();
                        foreach (Inventory inventory in UpdateEntries)
                        {
                            if (entrycount < Constants.InventoryAPILoadCount)
                            {

                                UpdateList.Add(MapInventory(inventory, "Update", APIInvetoryList));
                                entrycount++;
                            }
                            else
                            {
                                InvResponse = await new BLInventory().Put(UpdateList, Credentials.dealer_id);
                                UpdateList = new List<CRMInventory>();
                                UpdateList.Add(MapInventory(inventory, "Update", APIInvetoryList));
                                entrycount = 1;

                                passcount++;
                                message = message + CreateMessage(passcount, InvResponse.message);

                                if (InvResponse.data.AddedVins.Count > 0)
                                {
                                    insertedcount = insertedcount + InvResponse.data.AddedVins.Count();
                                }
                                if (InvResponse.data.NotAddedVins.Count > 0)
                                {
                                    failedcount = failedcount + InvResponse.data.NotAddedVins.Count();
                                    FailedList.AddRange(InvResponse.data.NotAddedVins);
                                }
                            }

                        }
                        if (UpdateList.Count > 0)
                        {
                            InvResponse = await new BLInventory().Put(UpdateList, Credentials.dealer_id);

                            passcount++;
                            message = message + CreateMessage(passcount, InvResponse.message);

                            if (InvResponse.data.AddedVins.Count > 0)
                            {
                                insertedcount = insertedcount + InvResponse.data.AddedVins.Count();
                            }
                            if (InvResponse.data.NotAddedVins.Count > 0)
                            {
                                failedcount = failedcount + InvResponse.data.NotAddedVins.Count();
                                FailedList.AddRange(InvResponse.data.NotAddedVins);
                            }
                        }
                    }
                    result = new Model.SyncResult();
                    result.ErrorRecordCount = failedcount;
                    result.InsertedRecordCount = 0;
                    result.API = Constants.Inventory;
                    result.ExecutionDateTime = DateTime.Now;
                    result.Message = message;
                    result.Method = "Update";
                    result.FailedIDList = FailedList;
                    result.UpdatedRecordCount = insertedcount;
                    result.ExecutionCount = passcount;
                    result.ExecutionStatus = 1;

                    resultList.Add(result);
                }
                catch (Exception ex)
                {
                    ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Inventory update.";
                    ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;
                    ex.Data[Common.Helper.LogDataKey.AdditionalInformation] += Environment.NewLine + message;

                    DataHandler DH = new DataHandler();
                    DH.WriteToLog(ex);

                    CRMLogData logdata = new CRMLogData();
                    logdata.id = null;
                    logdata.log_type = "Error";
                    logdata.method_name = "Update existing entries";
                    logdata.base_message = "InventorySync";
                    logdata.exception_message = ex.Message;
                    logdata.parameter_name = "no parameter";
                    logdata.stack_trace = ex.StackTrace;
                    //logdata.log_time = DateTime.Now;
                    //logdata.created = DateTime.Now;
                    //logdata.modified = DateTime.Now;

                    CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                    logdataResponse = new BLLogData().Set(logdata);
                }
            }
            return resultList;
        }
        private CRMInventory MapInventory(Inventory inventory, string Action, List<CRMInventory> APIInvetoryList)
        {
            CRMInventory APIInventory = new CRMInventory();
            if (Action == "Update")
            {
                APIInventory = APIInvetoryList.Where(x => x.vin == inventory.vin).ToList()[0];
                APIInventory.inv_id = inventory.vin;
            }
            if (Action == "Insert")
            {
                APIInventory.fuel_type = "";
                APIInventory.email = "";
            }

            APIInventory.dealer_id = Convert.ToInt32(Global.DealerID);
            APIInventory.make = inventory.make;
            APIInventory.model = inventory.model;
            APIInventory.vin = inventory.vin;
            APIInventory.stocknumber = inventory.stockNumber;
            APIInventory.color = inventory.color;
            APIInventory.year = inventory.modelYear.ToString();
            APIInventory.unit_status = inventory.inventoryStatus;
            APIInventory.suggested_price = inventory.bookVAlueAmount.ToString();

            return APIInventory;

        }



        private string CreateMessage(int Passcount, string ResponseMessage)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Pass");
            sb.Append(Passcount);
            sb.Append("-");
            sb.Append(ResponseMessage);
            sb.Append("-");
            sb.Append(DateTime.Now);
            sb.Append(Environment.NewLine);

            return sb.ToString();
        }


    }
}
