﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DP360CRMCommunication.Helper;
using Common;
using DP360CRMCommunication;
using SyncService.Model;
using Common.Model;
using System.Windows.Forms;



namespace SyncService
{
    public class BLSynchronise
    {
         string method = "";
        public async Task<int> SyncData()
        {
            try
            {
                List<LastExecutionInfo> LastExecutionList = new List<LastExecutionInfo>();
                LastExecutionInfo LastExecutionInfo = new LastExecutionInfo();
                string start_time = DateTime.Now.ToString();

                //Get login credentials from local file
                UserCredential user = new UserCredential();
                user = new DataHandler().GetUserDataFromFile();
                if (user.username != null)
                {

                    

                    //Get token and save it for all consicutive API calls

                    AuthenticateUser auth = new AuthenticateUser();
                    string token = auth.Authenticate(user);
                    Global.Token = "Bearer" + " " + token;




                    Global.DealerID = user.dealer_id;

                    //Get Database credentials from API            
                    DBCredentials credentials = new BLDBCredentials().Get();
                    //Get all last execution infornamtion that saved in last 2 days
                    DateTime dt = DateTime.Now.AddDays(-2);
                    string date = dt.ToString("yyyy-MM-dd");
                    //  List<LastExecutionInfo> LastExecutionList = new List<LastExecutionInfo>();
                    LastExecutionList = new List<LastExecutionInfo>();
                    List<LastExecutionInfo> LastExecutionList_GET = new List<LastExecutionInfo>();
                    LastExecutionList_GET = new BLLastExecutionInfo().GetList(date);

                    if (LastExecutionList_GET.Count == 0)
                    {
                        dt = DateTime.Now.AddDays(-4);
                        date = dt.ToString("yyyy-MM-dd");
                        LastExecutionList_GET = new BLLastExecutionInfo().GetList(date);
                        if (LastExecutionList_GET.Count == 0)
                        {
                            dt = DateTime.Now.AddDays(-8);
                            date = dt.ToString("yyyy-MM-dd");
                            LastExecutionList_GET = new BLLastExecutionInfo().GetList(date);
                        }
                    }

                    
                    //Get the last execution information of Billed Work Order
                    LastExecutionList = new List<LastExecutionInfo>();
                    LastExecutionList = LastExecutionList_GET;
                    if (LastExecutionList.Count > 0)
                    {
                        LastExecutionInfo = (from data in LastExecutionList.Where(l => l.log_type == Constants.BilledWorkOrder && l.response == "1").ToList()
                                             orderby data.execution_timestamp descending
                                             select data).FirstOrDefault();
                    }

                    //Sync Billed Work Orders created after last execution date
                    List<SyncResult> billedworkorder_result = await new BilledWorkOrderSync().Sync(credentials, LastExecutionInfo);

                    //Save Last execution information 

                    LastExecutionInfo info_workorder = new LastExecutionInfo();
                    foreach (SyncResult item in billedworkorder_result)
                    {
                        if (item.ExecutionCount > 0)
                        {
                            info_workorder = MAPLastExecutionInfo(item);
                            info_workorder = await new BLLastExecutionInfo().Set(info_workorder);
                        }
                        if (info_workorder != null && info_workorder.response == "1")
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("execution_timestamp:");
                            sb.Append(info_workorder.execution_timestamp);
                            sb.Append(Environment.NewLine);
                            sb.Append("record_insert_count:");
                            sb.Append(info_workorder.record_insert_count);
                            sb.Append(Environment.NewLine);
                            sb.Append("record_update_count:");
                            sb.Append(info_workorder.record_update_count);
                            sb.Append(Environment.NewLine);
                            sb.Append("status:");
                            sb.Append(info_workorder.status);
                            sb.Append(Environment.NewLine);
                            sb.Append("log:");
                            sb.Append(info_workorder.log);
                            sb.Append(Environment.NewLine);
                            sb.Append("log_type:");
                            sb.Append(info_workorder.log_type);
                            sb.Append(Environment.NewLine);
                            sb.Append("execution_count:");
                            sb.Append(info_workorder.execution_count);
                            sb.Append(Environment.NewLine);
                            sb.Append("execution_status:");
                            sb.Append(info_workorder.execution_status);
                            sb.Append(Environment.NewLine);
                            sb.Append("response:");
                            sb.Append(info_workorder.response);
                            sb.Append(Environment.NewLine);
                            sb.Append("start_time:");
                            sb.Append(info_workorder.start_time);
                            sb.Append(Environment.NewLine);
                            sb.Append("end_time:");
                            sb.Append(info_workorder.end_time);
                            sb.Append(Environment.NewLine);
                            sb.Append("modified:");
                            sb.Append(info_workorder.modified);
                            sb.Append(Environment.NewLine);
                            sb.Append("created:");
                            sb.Append(info_workorder.created);
                            sb.Append(Environment.NewLine);
                            sb.Append("id:");
                            sb.Append(info_workorder.id);
                            sb.Append(Environment.NewLine);
                            sb.Append("Method:");
                            sb.Append(method);

                            DataHandler DH = new DataHandler();
                            DH.SetLog(sb.ToString());
                            
                            
                        }
                        
                    }

                    



                    //Get the last execution information of Closed Vehicle Deals
                    LastExecutionInfo = new LastExecutionInfo();
                    LastExecutionList = LastExecutionList_GET;
                    if (LastExecutionList.Count > 0)
                    {

                        LastExecutionInfo = (from data in LastExecutionList.Where(l => l.log_type == Constants.ClosedVehicleDeals && l.response == "1").ToList()
                                             orderby data.execution_timestamp descending
                                             select data).FirstOrDefault();
                    }


                    //Sync Closed Vehicle Deals created after last execution date
                    List<SyncResult> closedvehicledeals_result = await new ClosedVehicleDealsSync().Sync(credentials, LastExecutionInfo);

                    //Save Last execution information 

                    LastExecutionInfo info_closedvehicle = new LastExecutionInfo();
                    foreach (SyncResult item in closedvehicledeals_result)
                    {
                        if (item.ExecutionCount > 0)
                        {
                            info_closedvehicle = MAPLastExecutionInfo(item);
                            info_closedvehicle = await new BLLastExecutionInfo().Set(info_closedvehicle);
                        }
                        if (info_closedvehicle != null && info_closedvehicle.response == "1")
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("execution_timestamp:");
                            sb.Append(info_closedvehicle.execution_timestamp);
                            sb.Append(Environment.NewLine);
                            sb.Append("record_insert_count:");
                            sb.Append(info_closedvehicle.record_insert_count);
                            sb.Append(Environment.NewLine);
                            sb.Append("record_update_count:");
                            sb.Append(info_closedvehicle.record_update_count);
                            sb.Append(Environment.NewLine);
                            sb.Append("status:");
                            sb.Append(info_closedvehicle.status);
                            sb.Append(Environment.NewLine);
                            sb.Append("log:");
                            sb.Append(info_closedvehicle.log);
                            sb.Append(Environment.NewLine);
                            sb.Append("log_type:");
                            sb.Append(info_closedvehicle.log_type);
                            sb.Append(Environment.NewLine);
                            sb.Append("execution_count:");
                            sb.Append(info_closedvehicle.execution_count);
                            sb.Append(Environment.NewLine);
                            sb.Append("execution_status:");
                            sb.Append(info_closedvehicle.execution_status);
                            sb.Append(Environment.NewLine);
                            sb.Append("response:");
                            sb.Append(info_closedvehicle.response);
                            sb.Append(Environment.NewLine);
                            sb.Append("start_time:");
                            sb.Append(info_closedvehicle.start_time);
                            sb.Append(Environment.NewLine);
                            sb.Append("end_time:");
                            sb.Append(info_closedvehicle.end_time);
                            sb.Append(Environment.NewLine);
                            sb.Append("modified:");
                            sb.Append(info_closedvehicle.modified);
                            sb.Append(Environment.NewLine);
                            sb.Append("created:");
                            sb.Append(info_closedvehicle.created);
                            sb.Append(Environment.NewLine);
                            sb.Append("id:");
                            sb.Append(info_closedvehicle.id);
                            sb.Append(Environment.NewLine);
                            sb.Append("Method:");
                            sb.Append(method);

                            DataHandler DH = new DataHandler();
                            DH.SetLog(sb.ToString());
                           
                        }
                        
                    }
                    
                       //Get the last execution information of Customers
                       LastExecutionInfo = new LastExecutionInfo();
                       LastExecutionList = LastExecutionList_GET;
                       if (LastExecutionList.Count > 0)
                       {
                           LastExecutionInfo = (from data in LastExecutionList.Where(l => l.log_type == Constants.Customer && l.response == "1").ToList()
                                                orderby data.execution_timestamp descending
                                                select data).FirstOrDefault();

                       }

                    
                                          //Sync Customers created after last execution date
                                          List<SyncResult> customer_result = await new CustomerSync().Sync(credentials, LastExecutionInfo);

                                          //Save Last execution information 

                                          LastExecutionInfo info_customer = new LastExecutionInfo();
                                          foreach (SyncResult item in customer_result)
                                          {
                                              if (item.ExecutionCount > 0)
                                              {
                                                  info_customer = MAPLastExecutionInfo(item);
                                                  info_customer = await new BLLastExecutionInfo().Set(info_customer);
                                              }
                                              if (info_customer != null && info_customer.response == "1")
                                              {
                                                  StringBuilder sb = new StringBuilder();
                                                  sb.Append("execution_timestamp:");
                                                  sb.Append(info_customer.execution_timestamp);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("record_insert_count:");
                                                  sb.Append(info_customer.record_insert_count);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("record_update_count:");
                                                  sb.Append(info_customer.record_update_count);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("status:");
                                                  sb.Append(info_customer.status);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("log:");
                                                  sb.Append(info_customer.log);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("log_type:");
                                                  sb.Append(info_customer.log_type);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("execution_count:");
                                                  sb.Append(info_customer.execution_count);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("execution_status:");
                                                  sb.Append(info_customer.execution_status);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("response:");
                                                  sb.Append(info_customer.response);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("start_time:");
                                                  sb.Append(info_customer.start_time);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("end_time:");
                                                  sb.Append(info_customer.end_time);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("modified:");
                                                  sb.Append(info_customer.modified);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("created:");
                                                  sb.Append(info_customer.created);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("id:");
                                                  sb.Append(info_customer.id);
                                                  sb.Append(Environment.NewLine);
                                                  sb.Append("Method:");
                                                  sb.Append(method);

                                                  DataHandler DH = new DataHandler();
                                                  DH.SetLog(sb.ToString());
                                                  
                                              }
                                              
                                          }
                                         
                      //Get the last execution information of Inventory
                       LastExecutionInfo = new LastExecutionInfo();
                       LastExecutionList = LastExecutionList_GET;
                       if (LastExecutionList.Count > 0)
                       {
                           LastExecutionInfo = (from data in LastExecutionList.Where(l => l.log_type == Constants.Inventory && l.response == "1").ToList()
                                                orderby data.execution_timestamp descending
                                                select data).FirstOrDefault();
                       }
                                         
                                         //Sync Inventory created after last execution date
                                         //Sync Inventories
                                         List<SyncResult> inventory_result = await new InventorySync().Sync(credentials);

                                         //Save Last execution information 
                                         LastExecutionInfo info_inventory = new LastExecutionInfo();
                                         foreach (SyncResult item in inventory_result)
                                         {
                                             if (item.ExecutionCount > 0)
                                             {
                                                 info_inventory = MAPLastExecutionInfo(item);
                                                 info_inventory = await new BLLastExecutionInfo().Set(info_inventory);
                                             }
                                             if (info_inventory != null && info_inventory.response == "1")
                                             {
                                                 StringBuilder sb = new StringBuilder();
                                                 sb.Append("execution_timestamp:");
                                                 sb.Append(info_inventory.execution_timestamp);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("record_insert_count:");
                                                 sb.Append(info_inventory.record_insert_count);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("record_update_count:");
                                                 sb.Append(info_inventory.record_update_count);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("status:");
                                                 sb.Append(info_inventory.status);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("log:");
                                                 sb.Append(info_inventory.log);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("log_type:");
                                                 sb.Append(info_inventory.log_type);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("execution_count:");
                                                 sb.Append(info_inventory.execution_count);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("execution_status:");
                                                 sb.Append(info_inventory.execution_status);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("response:");
                                                 sb.Append(info_inventory.response);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("start_time:");
                                                 sb.Append(info_inventory.start_time);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("end_time:");
                                                 sb.Append(info_inventory.end_time);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("modified:");
                                                 sb.Append(info_inventory.modified);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("created:");
                                                 sb.Append(info_inventory.created);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("id:");
                                                 sb.Append(info_inventory.id);
                                                 sb.Append(Environment.NewLine);
                                                 sb.Append("Method:");
                                                 sb.Append(method);

                                                 DataHandler DH = new DataHandler();
                                                 DH.SetLog(sb.ToString());
                                             }
                                         }


                    
                      //Get the last execution information of Invoice
                      LastExecutionInfo = new LastExecutionInfo();
                      LastExecutionList = LastExecutionList_GET;
                      if (LastExecutionList.Count > 0)
                      {
                          LastExecutionInfo = (from data in LastExecutionList.Where(l => l.log_type == Constants.Invoice && l.response == "1").ToList()
                                               orderby data.execution_timestamp descending
                                               select data).FirstOrDefault();
                      }

                                   //Sync Invoice created after last execution date
                                        List<SyncResult> invoice_result = await new InvoiceSync().Sync(credentials, LastExecutionInfo);

                                        //Save Last execution information 

                                        LastExecutionInfo info_invoice = new LastExecutionInfo();
                                        foreach (SyncResult item in invoice_result)
                                        {
                                            if (item.ExecutionCount > 0)
                                            {
                                                info_invoice = MAPLastExecutionInfo(item);
                                                info_invoice = await new BLLastExecutionInfo().Set(info_invoice);
                                            }
                                            if (info_invoice != null && info_invoice.response == "1")
                                            {
                                                StringBuilder sb = new StringBuilder();
                                                sb.Append("execution_timestamp:");
                                                sb.Append(info_invoice.execution_timestamp);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("record_insert_count:");
                                                sb.Append(info_invoice.record_insert_count);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("record_update_count:");
                                                sb.Append(info_invoice.record_update_count);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("status:");
                                                sb.Append(info_invoice.status);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("log:");
                                                sb.Append(info_invoice.log);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("log_type:");
                                                sb.Append(info_invoice.log_type);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("execution_count:");
                                                sb.Append(info_invoice.execution_count);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("execution_status:");
                                                sb.Append(info_invoice.execution_status);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("response:");
                                                sb.Append(info_invoice.response);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("start_time:");
                                                sb.Append(info_invoice.start_time);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("end_time:");
                                                sb.Append(info_invoice.end_time);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("modified:");
                                                sb.Append(info_invoice.modified);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("created:");
                                                sb.Append(info_invoice.created);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("id:");
                                                sb.Append(info_invoice.id);
                                                sb.Append(Environment.NewLine);
                                                sb.Append("Method:");
                                                sb.Append(method);

                                                DataHandler DH = new DataHandler();
                                                DH.SetLog(sb.ToString());
                                            }
                                        }
                                          






                    string end_time = DateTime.Now.ToString();

                    StringBuilder sb1 = new StringBuilder();
                    sb1.Append("Sync process start time:");
                    sb1.Append(start_time);
                    sb1.Append(Environment.NewLine);
                    sb1.Append("Sync process end time:");
                    sb1.Append(end_time);
                    DataHandler DH1 = new DataHandler();
                    DH1.SetLog(sb1.ToString());
                }
                else
                {
                    // Console.WriteLine("Please register/login first.");
                    // Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
               DH.WriteToLog(ex);
               

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SyncData";
                logdata.base_message = "BLSynchronise-SyncData";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);

                throw;
            }
            return 0;
        }
        public LastExecutionInfo MAPLastExecutionInfo(SyncResult item)
        {
            LastExecutionInfo Log = new LastExecutionInfo();
            method = "";
            Log.execution_timestamp = item.ExecutionDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            Log.record_insert_count = item.InsertedRecordCount;
            Log.record_update_count = item.UpdatedRecordCount;
            Log.status = 1;
            Log.log = item.ExecutionDateTime.ToString();
            Log.log_type = item.API;
            Log.execution_count = item.ExecutionCount;
            Log.execution_status = 1;
            Log.response = "1";
            Log.start_time = "0001-01-01T00:00:00";
            Log.end_time = "0001-01-01T00:00:00";
            Log.modified = "0001-01-01T00:00:00";
            Log.created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            method = item.Method;


            return Log;
        }

    }
}
