﻿using Common;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Common.Model;
using Common.Helper;
using System.IO.Compression;




namespace SyncService
{
    class Program
    {
        static void Main(string[] args)
        {
       

        string Destination = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Resource";
        string Destination2 = ConfigurationManager.AppSettings["DESTINATION_PATH2"];
        string Destination1 = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Log";

            UserCredential user = new UserCredential();
            

            user = new DataHandler().GetUserDataFromFile();
            DataHandler DH = new DataHandler();


            if (user.username != null && user.password != null)
            {
                try
                {
                    if (user.execute_service == true)
                    {
                        BLSynchronise synchronise = new SyncService.BLSynchronise();
                        synchronise.SyncData().Wait();
                    }
                    //else
                    //{

                    //    string s = "Please check the checkbox in Migration tab to execute the Sync service.";
                    //    DH.SetLog(s);
                    //}
                }
                catch (Exception ex)
                {

                    DH.SetErrorLog(ex.Message);
                    //Console.WriteLine(ex.Message);
                    //Console.ReadLine();
                }
            }
            else
            {
                DH.SetErrorLog("Please login first."); 
            }
        }

    }
}
