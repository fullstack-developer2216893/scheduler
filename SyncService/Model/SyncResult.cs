﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncService.Model
{
    public class SyncResult
    {
        public List<string> FailedIDList { get; set; }
        public string Message { get; set; }
        public string Method { get; set; }
        public string API { get; set; }
        public int InsertedRecordCount { get; set; }
        public int UpdatedRecordCount { get; set; }
        public int ErrorRecordCount { get; set; }
        public string ExceptionMessage { get; set; }
        public DateTime ExecutionDateTime { get; set; }
        public int ExecutionCount { get; set; } 
        public int ExecutionStatus { get; set; }
    }
    //public class ListSyncResult
    //{
    //    public List<SyncResult> Syncresult { get; set; }
    //    public ListSyncResult()
    //    {
    //        Syncresult = new List<SyncResult>();
    //    }
    //}
}
