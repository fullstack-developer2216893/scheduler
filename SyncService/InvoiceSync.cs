﻿using Common;
using Common.Model;
using DatabaseCommunication;
using DP360CRMCommunication;
using DP360CRMCommunication.Model;
using SyncService.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SyncService
{
    public class InvoiceSync
    {
        public async Task<List<SyncResult>> Sync(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            string token = Global.Token;
            SyncResult result = new Model.SyncResult();
            List<string> FailedList = new List<string>();
            List<SyncResult> resultList = new List<SyncResult>();
            int recordcount = 0;
            int failedcount = 0;

            int resCount = 0;


            //DBview Invoice List
            List<Invoice> InvoiceList = new List<Invoice>();
            try
            {


                InvoiceList = await new DBInvoice().GetInvoice(Credentials, LastExecutionInfo);
                recordcount = InvoiceList.Count;
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
               DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "DBview Invoice List";
                logdata.base_message = "InvoiceSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            try
            {
                int entrycount = 0;
                CRMInvoice APIData = new CRMInvoice();
                // InvoiceResponse invResponse = new InvoiceResponse();
                APIData.dealer_id = Global.DealerID;
                foreach (Invoice item in InvoiceList)
                {
                    if (entrycount < Constants.InvoiceAPILoadCount)
                    {
                        APIData.data.Add(MAPInvoice(item));
                        entrycount++;
                    }
                    else
                    {
                        //call async api to save Invoice in CRM
                        resCount = await new BLInvoice().Set(APIData);
                        APIData = new CRMInvoice();
                        APIData.data.Add(MAPInvoice(item));
                        entrycount = 1;



                        //if failed then increase failedcount, and save the inventory ids in result object failed id list
                        if (resCount <= 0)
                        {
                            failedcount++;
                            //  result.FailedIDList.Add();
                        }
                    }
                }
                if (APIData.data.Count > 0)
                {
                    resCount = await new BLInvoice().Set(APIData);

                    //if failed then increase failedcount, and save the inventory ids in result object failed id list           
                    if (resCount <= 0)
                    {
                        failedcount++;
                        // result.FailedIDList.Add();
                    }
                }


                result.ErrorRecordCount = failedcount;
                result.InsertedRecordCount = recordcount - failedcount;
                result.API = Constants.Invoice;
                result.ExecutionDateTime = DateTime.Now;
                result.Message = "Invoice Synced successfully";
                result.Method = "Insert";
                result.ExecutionCount = recordcount;
                result.ExecutionStatus = 1;

                resultList.Add(result);

            }
            catch (Exception ex)
            {

                


                ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Invoice Sync.";
                ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;

                DataHandler DH = new DataHandler();
                DH.WriteToLog(ex);


                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "call async api to save Invoice in CRM";
                logdata.base_message = "InvoiceSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse(); 
                logdataResponse = new BLLogData().Set(logdata);
            }
            return resultList;  
        }
        private InvoiceData MAPInvoice(Invoice invoice)
        {
            InvoiceData APIinvoice = new InvoiceData();

            APIinvoice.contact.address1 = invoice.Address1;
            APIinvoice.contact.address2 = invoice.Address2;
            APIinvoice.contact.city = invoice.CustomerCity;
            APIinvoice.contact.country = invoice.CustomerCountry;
            APIinvoice.contact.county = invoice.CustomerCountry;
            APIinvoice.contact.email = invoice.CustomerEmailAddress;
            APIinvoice.contact.first_name = invoice.CustomerFirstName;
            APIinvoice.contact.last_name = invoice.CustomerLastName;
            APIinvoice.contact.middle_name = invoice.CustomerMiddleName;
            APIinvoice.contact.mobile = invoice.CustomerMobilePhone;
            APIinvoice.contact.phone = invoice.CustomerMobilePhone;
            APIinvoice.contact.state = invoice.CustomerState;
            APIinvoice.contact.work_number = invoice.CustomerWorkPhone;
            APIinvoice.customer_id = invoice.CustomerID.ToString();
            APIinvoice.date = invoice.InvoiceDateTime.ToString();
            APIinvoice.product.price = invoice.LineItemUnitPriceAmount.ToString();
            APIinvoice.sperson_first_name = invoice.Salesperson_FirstName;
            APIinvoice.sperson_last_name = invoice.Salesperson_LastName;
            APIinvoice.time = invoice.InvoiceDateTime.ToString();

            return APIinvoice;
        }

    }
}
