﻿using Common;
using DatabaseCommunication;
using SyncService.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DP360CRMCommunication.Model;
using DP360CRMCommunication;
using Common.Model;

namespace SyncService
    {
    public class BilledWorkOrderSync
    {
        public async Task<List<SyncResult>> Sync(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            string token = Global.Token;
            SyncResult result = new Model.SyncResult();
            List<SyncResult> resultList = new List<SyncResult>();

            int recordcount = 0;
            int failedcount = 0;

            //DBview BilledWorkOrder List
            List<BilledWorkOrder> BilledWorkOrderList = new List<BilledWorkOrder>();
            try
            {
                BilledWorkOrderList = await new DBBilledWorkOrder().GetBilledWorkOrder(Credentials, LastExecutionInfo);
                recordcount = BilledWorkOrderList.Count;
            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                DH.WriteToDBLog(ex);
                

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "DBview BilledWorkOrder List";
                logdata.base_message = "BilledWorkOrderSync-DBview BilledWorkOrder List";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }

            try
            {
                int entrycount = 0;
                List<Leads> APICheckList = new List<Leads>();
                foreach (BilledWorkOrder item in BilledWorkOrderList)
                {
                    if (entrycount < Constants.LeadAPILoadCount)
                    {
                        APICheckList.Add(MAPLeads(item));
                        entrycount++;
                    }
                    else
                    {
                        //call async api to save BilledWorkOrder in CRM
                        int resCount = await new BLLeads().Set(APICheckList);
                        APICheckList = new List<Leads>();
                        APICheckList.Add(MAPLeads(item));
                        entrycount = 1;
                        //if failed then increase failedcount, and save the inventory ids in result object failed id list
                        if (resCount <= 0)
                        {
                            failedcount++;
                            // result.FailedIDList.Add();
                        }
                    }
                }
                if (APICheckList.Count > 0)
                {
                    int resCount = await new BLLeads().Set(APICheckList);
                    //if failed then increase failedcount, and save the inventory ids in result object failed id list           
                    if (resCount <= 0)
                    {
                        failedcount++;
                        // result.FailedIDList.Add();
                    }
                }


                result.ErrorRecordCount = failedcount;
                result.InsertedRecordCount = recordcount - failedcount;
                result.API = Constants.BilledWorkOrder;
                result.ExecutionDateTime = DateTime.Now;
                result.ExecutionCount = recordcount;
                result.Message = "Billed Work Order Synced successfully";
                result.Method = "Insert";
                resultList.Add(result);
            }
            catch (Exception ex)
            {
                //result.Message = "An exception occurs during Billed Work Order Sync.";
                //result.ExceptionMessage = ex.Message;
                //result.ExecutionDateTime = DateTime.Now;
                //result.API = Constants.BilledWorkOrder;

                ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Billed Work Order Sync.";
                ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;

                DataHandler DH = new DataHandler();
                DH.WriteToLog(ex);
               

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "call async api to save BilledWorkOrder in CRM";
                logdata.base_message = "BilledWorkOrderSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now; 
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            return resultList;
        }
        private Leads MAPLeads(BilledWorkOrder BilledWorkOrder)
        {
            Leads leads = new Leads();
            leads.dealer_id = Global.DealerID;
            leads.lead.product.mileage = BilledWorkOrder.OutMileage;
            leads.lead.contact.first_name = BilledWorkOrder.FirstName;
            leads.lead.contact.last_name = BilledWorkOrder.LastName;
            leads.lead.contact.middle_name = BilledWorkOrder.MiddleName;
            leads.lead.contact.address1 = BilledWorkOrder.Address1;
            leads.lead.contact.address2 = BilledWorkOrder.Address2;
            leads.lead.contact.city = BilledWorkOrder.City;
            leads.lead.contact.state = BilledWorkOrder.State;
            leads.lead.contact.zip = BilledWorkOrder.PostalCode;
            leads.lead.contact.country = BilledWorkOrder.Country;
            leads.lead.contact.county = BilledWorkOrder.County;
            leads.lead.contact.email = BilledWorkOrder.EmailAddress;
            leads.lead.contact.phone = BilledWorkOrder.HomePhone;
            leads.lead.contact.mobile = BilledWorkOrder.MobilePhone;
            leads.lead.contact.work_number = BilledWorkOrder.WorkPhone;
            leads.lead.product.vin = BilledWorkOrder.VIN;
            leads.lead.product.make = BilledWorkOrder.Make;
            leads.lead.product.model = BilledWorkOrder.Model;
            leads.lead.product.year = BilledWorkOrder.ModelYear;
            leads.lead.product.stockno = BilledWorkOrder.StockNumber;
            leads.lead.contact.lead_status = "Closed";
            leads.lead.contact.sales_step = 8;
            leads.lead.contact.status = "Closed";
            return leads;
        }

    }
}
