﻿using Common;
using Common.Model;
using DatabaseCommunication;
using DP360CRMCommunication;
using DP360CRMCommunication.Model;
using SyncService.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SyncService
{
    public class ClosedVehicleDealsSync
    {
        public async Task<List<SyncResult>> Sync(DBCredentials Credentials, LastExecutionInfo LastExecutionInfo)
        {
            string token = Global.Token;
            SyncResult result = new Model.SyncResult();
            List<SyncResult> resultList = new List<SyncResult>();

            int recordcount = 0;
            int failedcount = 0;

            //DBview ClosedVehicleDeals List
            List<ClosedVehicleDeals> ClosedVehicleDealsList = new List<ClosedVehicleDeals>();
            try
            {


                ClosedVehicleDealsList = await new DBClosedVehicleDeals().GetClosedVehicleDeals(Credentials, LastExecutionInfo);
                recordcount = ClosedVehicleDealsList.Count;
            }
            catch (Exception ex)
            {

                DataHandler DH = new DataHandler();
                 DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "DBview ClosedVehicleDeals List";
                logdata.base_message = "ClosedVehicleDealsSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
            try
            {
                int entrycount = 0;
                List<Leads> APICheckList = new List<Leads>();
                foreach (ClosedVehicleDeals item in ClosedVehicleDealsList)
                {
                    if (entrycount < Constants.LeadAPILoadCount)
                    {
                        APICheckList.Add(MAPLeads(item));
                        entrycount++;
                    }
                    else
                    {
                        //call async api to save ClosedVehicleDeals in CRM
                        int resCount = await new BLLeads().Set(APICheckList);
                        APICheckList = new List<Leads>();
                        APICheckList.Add(MAPLeads(item));
                        entrycount = 1;
                        //if failed then increase failedcount, and save the inventory ids in result object failed id list
                        if (resCount <= 0)
                        {
                            failedcount++;
                            // result.FailedIDList.Add();
                        }
                    }
                }
                if (APICheckList.Count > 0)
                {
                    int resCount = await new BLLeads().Set(APICheckList);
                    //if failed then increase failedcount, and save the inventory ids in result object failed id list           
                    if (resCount <= 0)
                    {
                        failedcount++;
                        //  result.FailedIDList.Add();
                    }
                }


                result.ErrorRecordCount = failedcount;
                result.InsertedRecordCount = recordcount - failedcount;
                result.API = Constants.ClosedVehicleDeals;
                result.ExecutionDateTime = DateTime.Now;
                result.ExecutionCount = recordcount;
                result.Message = "Closed Vehicle Deals Synced successfully";
                result.Method = "Insert";
                resultList.Add(result);
            }
            catch (Exception ex)
            {
                
                ex.Data[Common.Helper.LogDataKey.Usermessage] += Environment.NewLine + "An exception occurs during Closed Vehicle Deals Sync.";
                ex.Data[Common.Helper.LogDataKey.LogType] = Common.Helper.LogType.Exception;

                DataHandler DH = new DataHandler();
                DH.WriteToLog(ex);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "call async api to save ClosedVehicleDeals in CRM";
                logdata.base_message = "ClosedVehicleDealsSync";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata); 

            }
            return resultList;
        }
        private Leads MAPLeads(ClosedVehicleDeals ClosedVehicleDeals)
        {
            Leads leads = new Leads();
            leads.dealer_id = Global.DealerID;
            leads.lead.date = ClosedVehicleDeals.CreateDAteTime;
            leads.lead.product.vin = ClosedVehicleDeals.VIN;
            leads.lead.product.make = ClosedVehicleDeals.Make;
            leads.lead.product.model = ClosedVehicleDeals.Model;
            leads.lead.product.year = ClosedVehicleDeals.ModelYear;
            leads.lead.product.mileage = ClosedVehicleDeals.Mileage;
            leads.lead.product.color = ClosedVehicleDeals.Color;
            leads.lead.product.stockno = ClosedVehicleDeals.StockNumber;
            leads.lead.contact.first_name = ClosedVehicleDeals.BuyerFirstName;
            leads.lead.contact.last_name = ClosedVehicleDeals.BuyerLastName;
            leads.lead.contact.middle_name = ClosedVehicleDeals.BuyerMiddleName;
            leads.lead.contact.address1 = ClosedVehicleDeals.BuyerAddress1;
            leads.lead.contact.address2 = ClosedVehicleDeals.BuyerAddress2;
            leads.lead.contact.city = ClosedVehicleDeals.BuyerCity;
            leads.lead.contact.state = ClosedVehicleDeals.BuyerState;
            leads.lead.contact.zip = ClosedVehicleDeals.BuyerPostalCode;
            leads.lead.contact.county = ClosedVehicleDeals.BuyerCounty;
            leads.lead.contact.country = ClosedVehicleDeals.CoBuyerCountry;
            leads.lead.contact.email = ClosedVehicleDeals.BuyerEmailAddress;
            leads.lead.contact.phone = ClosedVehicleDeals.BuyerHomePhone;
            leads.lead.contact.mobile = ClosedVehicleDeals.BuyerMobilePhone;
            leads.lead.contact.work_number = ClosedVehicleDeals.BuyerWorkPhone;
            leads.lead.sperson_first_name = ClosedVehicleDeals.VehicleSalesperson1_FirstName;
            leads.lead.sperson_last_name = ClosedVehicleDeals.VehicleSalesperson1_LastName;
            leads.lead.product.price = ClosedVehicleDeals.TotalRetailPrice.ToString();
            leads.lead.contact.lead_status = "Closed";
            leads.lead.contact.sales_step = 10;
            leads.lead.contact.status = "Sold";
            return leads;
        }
    }
}
