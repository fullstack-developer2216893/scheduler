﻿using DP360CRMCommunication.Helper;
using Common;
using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Common.Model;
using SyncService;

namespace Synchroniser
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void btn_LogIn_Click(object sender, EventArgs e)
        {            
            try
            {
               Clear();
                lbl_msg.ForeColor = Color.Black;               
                lbl_msg.Text = "Please wait.......";
                if (isLogInFormValid())
                {
                    AuthenticateUser auth = new AuthenticateUser();
                    UserCredential user = new UserCredential();
                    user.username = txt_LUserName.Text;
                    user.password = txt_LoginPass.Text;
                    user.dealer_id = txt_DealerID.Text;
                    user.execute_service = false;
                    string token = auth.Authenticate(user);

                    if (token.Length > 0)
                    {
                        token = "Bearer" + " " + token;
                        Global.Token = token;
                        Global.DealerID = user.dealer_id;

                        Reset();


                        MessageBox.Show("LogIn Successfull.");
                        this.Hide();
                        // this.Close();
                        lbl_msg.Text = "";

                        BLSynchronise bl = new BLSynchronise();
                         bl.SyncData();
                         
                      
                       // Home home = new Home();
                       // home.StartPosition = FormStartPosition.CenterScreen;
                       // home.ShowDialog();
                        // this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Invalid username or password/You have not authenticated yet.");
                        lbl_msg.Text = "";
                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                lbl_msg.Text = "";
                DataHandler DH = new DataHandler();
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("LogIn");
                DH.SetErrorLog(sb.ToString());

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type ="Error";
                logdata.method_name = "LogIn";
                logdata.base_message = "Click on LogIn button";
                logdata.exception_message=ex.Message;
                logdata.parameter_name ="no parameter";
                logdata.stack_trace =ex.StackTrace;
                //logdata.log_time =DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            Reset();
        }
        private void Reset()
        {
            txt_LUserName.Text = "";
            txt_LoginPass.Text = "";
            txt_DealerID.Text = "";
            lbl_msg.ForeColor = Color.Black;
            Clear();
        }
        private void Clear()
        {
            lbl_msg.Text = "";
            errorProvider1.Clear();
        }
        private bool isLogInFormValid()
        {
            bool result = true;
            lbl_msg.ForeColor = Color.Red;
           
            if (txt_LUserName.Text.Trim() == "")
            {
                Clear();
               lbl_msg.Text = "Username can not be blank.";
                result = false;
                errorProvider1.SetError(txt_LUserName, "Username can not be blank.");
            }
            else if (txt_LoginPass.Text.Trim() == "")
            {
                Clear();
                lbl_msg.Text = "Password can not be blank.";
                result = false;
                errorProvider1.SetError(txt_LoginPass, "Password can not be blank.");
            }
            //else if (!Regex.IsMatch(txt_LoginPass.Text.Trim(), @"^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$"))
            //{
            //    lbl_msg.Text = "Password should contain at least six characters with one special character,one capital letter and minimum one number.";
            //    result = false;
            //    errorProvider1.SetError(txt_LoginPass, "Password should contain at least six characters with one special character,one capital letter and minimum one number.");
            //}
            else if(txt_DealerID.Text.Trim()=="")
            {
                Clear();
                lbl_msg.Text = "DealerID can not be blank.";
                result = false;
                errorProvider1.SetError(txt_DealerID, "DealerID can not be blank.");
            }
          
            return result;
        }

        private void LogIn_Load(object sender, EventArgs e)
        {

        }
    }
}
 