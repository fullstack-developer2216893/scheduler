﻿using Common;
using Common.Model;
using DatabaseCommunication;
using DP360CRMCommunication;
using DP360CRMCommunication.Helper;
using DP360CRMCommunication.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Configuration;
using System.IO.Compression;
using System.IO;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Common.Model;
using Common.Helper;


namespace Synchroniser
{
    public partial class Home : Form
    {
        string Destination = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Resource";
        string Destination1 = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Log";
        string Destination2 = ConfigurationManager.AppSettings["DESTINATION_PATH2"];
        //string filepath1 = Destination2 + ConfigurationManager.AppSettings["BilledWorkOrder"] + "_" + ConfigurationManager.AppSettings["extCSV"];
        // string filename = ConfigurationManager.AppSettings["BilledWorkOrder"] + ".zip";
        int dealeridftp = Convert.ToInt32(Global.DealerID);

        int i = 1;
        int pageSize = 5000;
        DataHandler DH = new DataHandler();
        StringBuilder strMsg = new StringBuilder();
        protected StatusBar mainStatusBar = new StatusBar();
        protected StatusBarPanel statusPanel = new StatusBarPanel();
        protected StatusBarPanel statusPanel2 = new StatusBarPanel();
        public Home()
        {
            InitializeComponent();


            Version version = Assembly.GetExecutingAssembly().GetName().Version;

            // Set first panel properties and add to StatusBar  
            statusPanel.BorderStyle = StatusBarPanelBorderStyle.Sunken;
            statusPanel.Text = "";
            statusPanel.ToolTipText = "App Version";
            statusPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            mainStatusBar.Panels.Add(statusPanel);

            statusPanel2.BorderStyle = StatusBarPanelBorderStyle.Raised;
            statusPanel2.Text = String.Format("Version:{0}", version);
            statusPanel2.ToolTipText = "App Version";
            statusPanel2.AutoSize = StatusBarPanelAutoSize.Contents;
            mainStatusBar.Panels.Add(statusPanel2);


            mainStatusBar.ShowPanels = true;
            // Add StatusBar to Form controls  
            this.Controls.Add(mainStatusBar);

            UserCredential user = new UserCredential();
            DataHandler DH = new DataHandler();
            AuthenticateUser auth = new AuthenticateUser();

            user = DH.GetUserDataFromFile();
            //string token = auth.Authenticate(user);

            //if (token.Length > 0)
            //{
            //    token = "Bearer" + " " + token;
            //    Global.Token = token;
            //    //this.tabControl1.SelectedIndex = 1;
            DBCredentials Credentials = new BLDBCredentials().Get();
            if (Credentials.username != null && Credentials.password != null)
            {
                txt_dealerID.Text = Credentials.dealer_id.ToString();
                txtDatabase.Text = Credentials.db_name;
                txtIPAddress.Text = Credentials.ip_address;
                txtUsername.Text = Credentials.username;
                txtPassword.Text = Credentials.password;

                btnGenerate.Enabled = true;
            }
            else
            {
                btnGenerate.Enabled = false;
            }
            //}
            //else
            //{

            //    ConnectionMessage.Text = "Unable to connect to service.";
            //}

            if (user.execute_service == true)
            {
                chk_service.Checked = true;
            }
            else
            {
                chk_service.Checked = false;
            }
        }

        #region Credential


        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            // if (isCredentialFormValid())
            //{
            Reset();
            ConnectionMessage.Text = "Checking connection. Please wait...";

            DatabaseConnection db = new DatabaseConnection();
            DBCredentials Credentials = new DBCredentials();
            Credentials.ip_address = txtIPAddress.Text;
            Credentials.db_name = txtDatabase.Text;
            Credentials.username = txtUsername.Text;
            Credentials.password = txtPassword.Text;
           
            if (db.TestConnection(Credentials))
            {
                ConnectionMessage.Text = "Successfully connected to the database.";
                btnGenerate.Enabled = true;
            }
            else
            {
                ConnectionMessage.Text = "Connection cannot be established.";
                ConnectionMessage.Text += Environment.NewLine;
                ConnectionMessage.Text += "Check provided credentials.";
            }
            // }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isCredentialFormValid())
            {
                Reset();
                ConnectionMessage.ForeColor = Color.Black;
                ConnectionMessage.Text = "Checking connection. Please wait...";

                DatabaseConnection db = new DatabaseConnection();
                DBCredentials Credentials = new DBCredentials();

                Credentials.ip_address = txtIPAddress.Text;
                Credentials.dealer_id = Convert.ToInt32(txt_dealerID.Text);
                Credentials.db_name = txtDatabase.Text;
                Credentials.username = txtUsername.Text;
                Credentials.password = txtPassword.Text;
                Credentials.created = DateTime.Now;
                Credentials.modified = DateTime.Now;
                if (db.TestConnection(Credentials))
                {
                    //Save credentials
                    BLDBCredentialResponse result = new BLDBCredentialResponse();
                    result = new BLDBCredentials().Set(Credentials);

                    ConnectionMessage.Text = result.message;


                }
                else
                {
                    ConnectionMessage.ForeColor = Color.Red;
                    ConnectionMessage.Text = "Connection cannot be established.";
                    ConnectionMessage.Text += Environment.NewLine;
                    ConnectionMessage.Text += "Check provided credentials.";
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ConnectionMessage.ForeColor = Color.Black;
            Reset();
            // this.Close();
        }
        private void Reset()
        {

            ConnectionMessage.Text = "";
            errorProvider1.Clear();
        }
        private bool isCredentialFormValid()
        {
            bool result = true;
            ConnectionMessage.ForeColor = Color.Red;

            if (txt_dealerID.Text.Trim() == "")
            {
                result = false;
                Reset();
                ConnectionMessage.Text = "DealerID cannot be blank";
                errorProvider1.SetError(txt_dealerID, "DealerID cannot be blank");
            }
            else if (txtIPAddress.Text.Trim() == "")
            {
                result = false;
                Reset();
                ConnectionMessage.Text = "IP Address cannot be blank";
                errorProvider1.SetError(txtIPAddress, "IP Address cannot be blank");
            }
            else if (!Regex.IsMatch(txtIPAddress.Text.Trim(), @"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"))
            {
                result = false;
                Reset();
                ConnectionMessage.Text = "IP Address is not in correct format";
                errorProvider1.SetError(txtIPAddress, "IP Address is not in correct format");
            }
            else if (txtUsername.Text.Trim() == "")
            {
                result = false;
                Reset();
                ConnectionMessage.Text = "Username cannot be blank";
                errorProvider1.SetError(txtUsername, "Username cannot be blank");
            }
            else if (txtPassword.Text.Trim() == "")
            {
                result = false;
                Reset();
                ConnectionMessage.Text = "Password cannot be blank";
                errorProvider1.SetError(txtPassword, "Password cannot be blank");
            }
            else if (txtDatabase.Text.Trim() == "")
            {
                result = false;
                Reset();
                ConnectionMessage.Text = "Database name cannot be blank";
                errorProvider1.SetError(txtDatabase, "Database name cannot be blank");
            }
            return result;

        }


        #endregion


        #region Migration

        private void chk_service_CheckedChanged(object sender, EventArgs e)
        {
            UserCredential user = new UserCredential();
            DH = new DataHandler();
            try
            {
                user = DH.GetUserDataFromFile();

                if (chk_service.Checked == true)
                {
                    user.execute_service = true;
                }
                else
                {
                    user.execute_service = false;
                }
                DH.SetUserDataToFile(user);
            }
            catch (Exception ex)
            {

                DH.SetErrorLog(ex.Message);

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "chk_service_CheckedChanged";
                logdata.base_message = "checkbox check change for execute sync service";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }

        }
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            bool res = true;
            try
            {

                lbl_msg.ForeColor = Color.Black;
                lbl_msg.Text = "Please wait............";
                DBCredentials Credentials = new BLDBCredentials().Get();
                LastExecutionInfo LastExeInfo = new LastExecutionInfo();

                        //For insert CSV status................................
                         CSVStatusRequest req = new CSVStatusRequest();
                         CSVStatus csvstat = new CSVStatus();
                         req.status = "0";
                        BLCSVStatus BLCSVStat = new BLCSVStatus();
                         csvstat = BLCSVStat.Set(req);

                            //Global.ExceptionCount = 1;
                            //res = BilledWorkOrderCSVGenerate(Credentials, LastExeInfo);
                            Global.ExceptionCount = 1;
                            res = ClosedVehicleDealsCSVGenerate(Credentials, LastExeInfo);
                            Global.ExceptionCount = 1;
                            res = CustomerCSVGenerate(Credentials, LastExeInfo);
                            Global.ExceptionCount = 1;
                           res = InventoryCSVGenerate(Credentials, LastExeInfo);
                            // Global.ExceptionCount = 1;
                             //res=InvoiceCSVGenerate(Credentials, LastExeInfo);


                        if (strMsg.ToString() != null && strMsg.ToString() != "")
                        {
                            // MessageBox.Show(strMsg.ToString());
                            CustomMsgBox msgBox = new CustomMsgBox();
                            msgBox.ShowDialog();
                        }
                        if (res == true)
                        {

                            MessageBox.Show("CSV generated successfully.");
                        }
                        //For update CSVStatus.........................
                         if (csvstat.id > 0)
                        {
                         BLCSVStat = new BLCSVStatus();
                        BLCSVStat.Put(csvstat);
                       }
                        lbl_msg.Text = "";

                    }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            ConnectionMessage.Text = "";
                            DataHandler DH = new DataHandler();
                            DH.SetErrorLog(ex.Message);

                            CRMLogData logdata = new CRMLogData();
                            logdata.id = null;
                            logdata.log_type = "Error";
                            logdata.method_name = "Generate CSV";
                            logdata.base_message = "Click on Generate CSV button";
                            logdata.exception_message = ex.Message;
                            logdata.parameter_name = "no parameter";
                            logdata.stack_trace = ex.StackTrace;
                            //logdata.log_time = DateTime.Now;
                            //logdata.created = DateTime.Now;
                            //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
            }
        }
        public bool BilledWorkOrderCSVGenerate(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            string filename = ConfigurationManager.AppSettings["BilledWorkOrder"] + ".zip";
            bool res = false;
            try
            {
                DataHandler DH = new DataHandler();
                LastExecutionInfo info_BilledWorkOrder = new LastExecutionInfo();
                // DBCredentials Credentials = new BLDBCredentials().Get();


                //DBview BilledWorkOrder List
                List<BilledWorkOrder> BilledWorkOrderList = new List<BilledWorkOrder>();
                if (i > 0)
                {
                    do
                    {
                        BilledWorkOrderList = new DBBilledWorkOrder().GetBilledWorkOrder(Credentials, LastExeInfo, i, pageSize);

                        //Save data in CSV File  
                        if (BilledWorkOrderList.Count > 0)
                        {
                            DH.SetDBBilledWorkOrderListToCSV(BilledWorkOrderList, i, Credentials.dealer_id);
                            info_BilledWorkOrder.execution_count = BilledWorkOrderList.Count + info_BilledWorkOrder.execution_count;
                        }
                        i++;
                    }
                    while (BilledWorkOrderList.Count > 0);

                }

                res = true;
                info_BilledWorkOrder.execution_status = 1;
                info_BilledWorkOrder.execution_timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_BilledWorkOrder.log = info_BilledWorkOrder.execution_timestamp.ToString();
                info_BilledWorkOrder.log_type = Constants.BilledWorkOrder;
                info_BilledWorkOrder.record_insert_count = 0;
                info_BilledWorkOrder.record_update_count = 0;
                info_BilledWorkOrder.response = "CSV generate";
                info_BilledWorkOrder.status = 0;
                info_BilledWorkOrder.start_time = "0001-01-01T00:00:00";
                info_BilledWorkOrder.end_time = "0001-01-01T00:00:00";
                info_BilledWorkOrder.created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_BilledWorkOrder.modified = "0001-01-01T00:00:00";

                LastExeInfo = new LastExecutionInfo();
                LastExeInfo = new BLLastExecutionInfo().SET(info_BilledWorkOrder);
                string filepath = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "BilledWorkOrder" + ".zip";

                ZipFile.CreateFromDirectory(ConfigurationManager.AppSettings["BilledWorkOrder_PATH"], ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "BilledWorkOrder" + ".zip", CompressionLevel.Fastest, true);
                System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["BilledWorkOrder_PATH"]);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                if (Directory.Exists(ConfigurationManager.AppSettings["BilledWorkOrder_PATH"]))
                {
                    Directory.Delete(ConfigurationManager.AppSettings["BilledWorkOrder_PATH"]);
                }
                
               
                FTPUploadFile FTP = new FTPUploadFile();
                FTP.FileUploadFTP(filepath, filename, dealeridftp);
                if (File.Exists(filepath))
                {

                    File.Delete(filepath);
                }
            }

            catch (Exception ex)
            {
                res = false;
                DH = new DataHandler();
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("BilledWorkOrderCSVGenerate");
                sb.Append(ex);
                DH.SetErrorLog(sb.ToString());

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "BilledWorkOrderCSVGenerate";
                logdata.base_message = "on BilledWorkOrder CSV generate";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);

                if (Global.ExceptionCount > 1)
                {
                    strMsg.Append("Method:");
                    strMsg.Append("BilledWorkOrderCSVGenerate");
                    strMsg.Append(Environment.NewLine);
                    strMsg.Append("Exception:");
                    strMsg.Append(ex.Message);
                    strMsg.Append(Environment.NewLine);
                }
                SleepBilledWorkOrder(Credentials, LastExeInfo);
            }
            return res;
        }
        public bool ClosedVehicleDealsCSVGenerate(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            string filename = ConfigurationManager.AppSettings["ClosedVehicleDeals"] + ".zip";
            bool res = false;
            try
            {
                //DBview ClosedVehicleDeals List
                List<ClosedVehicleDeals> ClosedVehicleDealsList = new List<ClosedVehicleDeals>();
                LastExecutionInfo info_ClosedVehicleDeals = new LastExecutionInfo();

                i = 1;
                if (i > 0)
                {
                    do
                    {
                        LastExeInfo = new LastExecutionInfo();
                        ClosedVehicleDealsList = new DBClosedVehicleDeals().GetClosedVehicleDeals(Credentials, LastExeInfo, i, pageSize);

                        //Save data in CSV File               
                        DataHandler DH = new DataHandler();
                        if (ClosedVehicleDealsList.Count > 0)
                        {
                            DH.SetDBClosedVehicleDealsListToCSV(ClosedVehicleDealsList, i, Credentials.dealer_id);
                            info_ClosedVehicleDeals.execution_count = ClosedVehicleDealsList.Count + info_ClosedVehicleDeals.execution_count;
                        }
                        i++;
                    } while (ClosedVehicleDealsList.Count > 0);
                }

                res = true;
                info_ClosedVehicleDeals.execution_status = 1;
                info_ClosedVehicleDeals.execution_timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_ClosedVehicleDeals.log = info_ClosedVehicleDeals.execution_timestamp.ToString();
                info_ClosedVehicleDeals.log_type = Constants.ClosedVehicleDeals;
                info_ClosedVehicleDeals.record_insert_count = 0;
                info_ClosedVehicleDeals.record_update_count = 0;
                info_ClosedVehicleDeals.response = "CSV generate";
                info_ClosedVehicleDeals.start_time = "0001-01-01T00:00:00";
                info_ClosedVehicleDeals.end_time = "0001-01-01T00:00:00";
                info_ClosedVehicleDeals.created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_ClosedVehicleDeals.modified = "0001-01-01T00:00:00";
                info_ClosedVehicleDeals.status = 0;


                LastExeInfo = new LastExecutionInfo();
                LastExeInfo = new BLLastExecutionInfo().SET(info_ClosedVehicleDeals);
                string filepath = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "ClosedVehicleDeals" + ".zip";

                ZipFile.CreateFromDirectory(ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"], ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "ClosedVehicleDeals" + ".zip", CompressionLevel.Fastest, true);
                System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"]);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                if (Directory.Exists(ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"]))
                {
                    Directory.Delete(ConfigurationManager.AppSettings["ClosedVehicleDeals_PATH"]);
                }

                FTPUploadFile FTP = new FTPUploadFile();
                FTP.FileUploadFTP(filepath, filename, dealeridftp);
                if (File.Exists(filepath))
                {

                    File.Delete(filepath);
                }
            }
            catch (Exception ex)
            {
                res = false;
                DH = new DataHandler();
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("ClosedVehicleDealsCSVGenerate");
                DH.SetErrorLog(sb.ToString());

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "ClosedVehicleDealsCSVGenerate";
                logdata.base_message = "on ClosedVehicleDeals CSV generate";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);

                if (Global.ExceptionCount > 1)
                {
                    strMsg.Append("Method:");
                    strMsg.Append("ClosedVehicleDealsCSVGenerate");
                    strMsg.Append(Environment.NewLine);
                    strMsg.Append("Exception:");
                    strMsg.Append(ex.Message);
                    strMsg.Append(Environment.NewLine);
                }
                SleepClosedVehicleDeals(Credentials, LastExeInfo);

            }
            return res;
        }
        public bool CustomerCSVGenerate(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            string filename = ConfigurationManager.AppSettings["Customer"] + ".zip";
            bool res = false;
            try
            {
                //DBview Customer List
                List<Customer> CustomerList = new List<Customer>();
                LastExecutionInfo info_Customer = new LastExecutionInfo();

                i = 1;
                if (i > 0)
                {
                    do
                    {
                        LastExeInfo = new LastExecutionInfo();
                        CustomerList = new DBCustomer().GetCustomer(Credentials, LastExeInfo, i, pageSize);

                        //Save data in CSV File               
                        DH = new DataHandler();
                        if (CustomerList.Count > 0)
                        {
                            DH.SetDBCustomerListToCSV(CustomerList, i, Credentials.dealer_id);
                            info_Customer.execution_count = CustomerList.Count + info_Customer.execution_count;
                        }
                        i++;
                    } while (CustomerList.Count > 0);
                }

                res = true;
                info_Customer.execution_status = 1;
                info_Customer.execution_timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_Customer.log = info_Customer.execution_timestamp.ToString();
                info_Customer.log_type = Constants.Customer;
                info_Customer.record_insert_count = 0;
                info_Customer.record_update_count = 0;
                info_Customer.response = "CSV generate";
                info_Customer.status = 0;
                info_Customer.start_time = "0001-01-01T00:00:00";
                info_Customer.end_time = "0001-01-01T00:00:00";
                info_Customer.created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_Customer.modified = "0001-01-01T00:00:00";

                LastExeInfo = new LastExecutionInfo();
                LastExeInfo = new BLLastExecutionInfo().SET(info_Customer);
                string filepath = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "Customer" + ".zip";

                ZipFile.CreateFromDirectory(ConfigurationManager.AppSettings["Customer_PATH"], ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "Customer" + ".zip", CompressionLevel.Fastest, true);
                System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["Customer_PATH"]);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                if (Directory.Exists(ConfigurationManager.AppSettings["Customer_PATH"]))
                {
                    Directory.Delete(ConfigurationManager.AppSettings["Customer_PATH"]);
                }

                FTPUploadFile FTP = new FTPUploadFile();
                FTP.FileUploadFTP(filepath, filename, dealeridftp);
                if (File.Exists(filepath))
                {

                    File.Delete(filepath);
                }
            }
            catch (Exception ex)
            {
                res = false;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("CustomerCSVGenerate");
                DH.SetErrorLog(sb.ToString());

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "CustomerCSVGenerate";
                logdata.base_message = "on Customer CSV generate";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                if (Global.ExceptionCount > 1)
                {
                    strMsg.Append("Method:");
                    strMsg.Append("CustomerCSVGenerate");
                    strMsg.Append(Environment.NewLine);
                    strMsg.Append("Exception:");
                    strMsg.Append(ex.Message);
                    strMsg.Append(Environment.NewLine);
                }
                SleepCustomer(Credentials, LastExeInfo);
            }
            return res;
        }
        public bool InventoryCSVGenerate(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            string filename = ConfigurationManager.AppSettings["Inventory"] + ".zip";
            bool res = false;
            try
            {


                //DBview Inventory List   
                List<Inventory> InventoryList = new List<Inventory>();
                LastExecutionInfo info_Inventory = new LastExecutionInfo();


                i = 1;
                if (i > 0)
                {
                    do
                    {
                        LastExeInfo = new LastExecutionInfo();
                        InventoryList = new DBInventory().GetInventory(Credentials, i, pageSize);

                        //Save data in CSV File
                        DH = new DataHandler();
                        if (InventoryList.Count > 0)
                        {
                            DH.SetDBInventoryListToCSV(InventoryList, i, Credentials.dealer_id);
                            info_Inventory.execution_count = InventoryList.Count + info_Inventory.execution_count;
                        }
                        i++;
                    } while (InventoryList.Count > 0);
                }
                res = true;
                info_Inventory.execution_status = 1;
                info_Inventory.execution_timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_Inventory.log = DateTime.Now.ToString();
                info_Inventory.log_type = Constants.Inventory;
                info_Inventory.record_insert_count = 0;
                info_Inventory.record_update_count = 0;
                info_Inventory.response = "CSV generate";
                info_Inventory.status = 0;
                info_Inventory.start_time = "0001-01-01T00:00:00";
                info_Inventory.end_time = "0001-01-01T00:00:00";
                info_Inventory.created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_Inventory.modified = "0001-01-01T00:00:00";

                LastExeInfo = new LastExecutionInfo();
                LastExeInfo = new BLLastExecutionInfo().SET(info_Inventory);
                string filepath = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "Inventory" + ".zip";

                ZipFile.CreateFromDirectory(ConfigurationManager.AppSettings["Inventory_PATH"], ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "Inventory" + ".zip", CompressionLevel.Fastest, true);
                System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["Inventory_PATH"]);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                if (Directory.Exists(ConfigurationManager.AppSettings["Inventory_PATH"]))
                {
                    Directory.Delete(ConfigurationManager.AppSettings["Inventory_PATH"]);
                }

                FTPUploadFile FTP = new FTPUploadFile();
                FTP.FileUploadFTP(filepath, filename, dealeridftp);
                if (File.Exists(filepath))
                {

                    File.Delete(filepath);
                }
            }
            catch (Exception ex)
            {
                res = false;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("InventoryCSVGenerate");
                DH.SetErrorLog(sb.ToString());

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "InventoryCSVGenerate";
                logdata.base_message = "on Inventory CSV generate";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                if (Global.ExceptionCount > 1)
                {
                    strMsg.Append("Method:");
                    strMsg.Append("InventoryCSVGenerate");
                    strMsg.Append(Environment.NewLine);
                    strMsg.Append("Exception:");
                    strMsg.Append(ex.Message);
                    strMsg.Append(Environment.NewLine);
                }
                SleepInventory(Credentials, LastExeInfo);
            }
            return res;
        }
        public bool InvoiceCSVGenerate(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            //string filepath1 = Destination2 + ConfigurationManager.AppSettings["Invoice"] + "_" + ConfigurationManager.AppSettings["extCSV"];
            string filename = ConfigurationManager.AppSettings["Invoice"] + ".zip";
            bool res = false;
            try
            {
                //DBview Invoice List
                List<Invoice> InvoiceList = new List<Invoice>();
                LastExecutionInfo info_Invoice = new LastExecutionInfo();


                i = 1;
                if (i > 0)
                {
                    do
                    {
                        LastExeInfo = new LastExecutionInfo();
                        InvoiceList = new DBInvoice().GetInvoice(Credentials, LastExeInfo, i, pageSize);

                        //Save data in CSV File               
                        DH = new DataHandler();
                        if (InvoiceList.Count > 0)
                        {
                            DH.SetDBInvoiceListToCSV(InvoiceList, i, Credentials.dealer_id);

                            info_Invoice.execution_count = InvoiceList.Count + info_Invoice.execution_count;
                        }
                        i++;
                    }
                    while (InvoiceList.Count > 0);
                }
                res = true;
                info_Invoice.execution_status = 1;
                info_Invoice.execution_timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_Invoice.log = info_Invoice.execution_timestamp.ToString();
                info_Invoice.log_type = Constants.Invoice;
                info_Invoice.record_insert_count = 0;
                info_Invoice.record_update_count = 0;
                info_Invoice.response = "CSV generate";
                info_Invoice.status = 0;
                info_Invoice.start_time = "0001-01-01T00:00:00";
                info_Invoice.end_time = "0001-01-01T00:00:00";
                info_Invoice.created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                info_Invoice.modified = "0001-01-01T00:00:00";

                // LastExeInfo = new LastExecutionInfo();
                // LastExeInfo = new BLLastExecutionInfo().SET(info_Invoice);
                string filepath = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "Invoice" + ".zip";

                ZipFile.CreateFromDirectory(ConfigurationManager.AppSettings["Invoice_PATH"], ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\" + "Invoice" + ".zip", CompressionLevel.Fastest, true);
                System.IO.DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["Invoice_PATH"]);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                if (Directory.Exists(ConfigurationManager.AppSettings["Invoice_PATH"]))
                {
                    Directory.Delete(ConfigurationManager.AppSettings["Invoice_PATH"]);
                }

                //int a = 99970070;
                FTPUploadFile FTP = new FTPUploadFile();
                FTP.FileUploadFTP(filepath, filename, dealeridftp);
                if (File.Exists(filepath))
                {

                    File.Delete(filepath);
                }
            }
            catch (Exception ex)
            {
                res = false;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("InvoiceCSVGenerate");
                DH.SetErrorLog(sb.ToString());

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "InvoiceCSVGenerate";
                logdata.base_message = "on Invoice CSV generate";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                if (Global.ExceptionCount > 1)
                {
                    strMsg.Append("Method:");
                    strMsg.Append("InvoiceCSVGenerate");
                    strMsg.Append(Environment.NewLine);
                    strMsg.Append("Exception:");
                    strMsg.Append(ex.Message);
                    strMsg.Append(Environment.NewLine);
                }
                SleepInvoice(Credentials, LastExeInfo);
            }
            return res;
        }
        private void SleepBilledWorkOrder(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            try
            {
                if (Global.ExceptionCount < 2)
                {
                    Global.ExceptionCount++;
                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Method:");
                    sb.Append("BilledWorkOrderCSVGenerate");
                    DH.SetLog(sb.ToString());

                    Thread.Sleep(60000);
                    Thread t = new Thread(() => BilledWorkOrderCSVGenerate(Credentials, LastExeInfo));
                    t.Start();

                    t.Join();
                }
            }
            catch (Exception ex)
            {
                throw;
                DH = new DataHandler();
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("Retry:BilledWorkOrderCSVGenerate");
                DH.SetErrorLog(sb.ToString());
            }

        }
        private void SleepClosedVehicleDeals(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            try
            {
                if (Global.ExceptionCount < 2)
                {
                    Global.ExceptionCount++;

                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Method:");
                    sb.Append("ClosedVehicleDealsCSVGenerate");
                    DH.SetLog(sb.ToString());

                    Thread.Sleep(60000);
                    Thread t = new Thread(() => ClosedVehicleDealsCSVGenerate(Credentials, LastExeInfo));
                    t.Start();

                    t.Join();
                }

            }
            catch (Exception ex)
            {
                throw;
                DH = new DataHandler();
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("Retry:ClosedVehicleDealsCSVGenerate");
                DH.SetErrorLog(sb.ToString());
            }
        }
        private void SleepCustomer(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            try
            {
                if (Global.ExceptionCount < 2)
                {
                    Global.ExceptionCount++;

                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Method:");
                    sb.Append("CustomerCSVGenerate");
                    DH.SetLog(sb.ToString());

                    Thread.Sleep(60000);
                    Thread t = new Thread(() => CustomerCSVGenerate(Credentials, LastExeInfo));
                    t.Start();

                    t.Join();
                }

            }
            catch (Exception ex)
            {
                throw;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("Retry:CustomerCSVGenerate");
                DH.SetErrorLog(sb.ToString());
            }
        }
        private void SleepInventory(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            try
            {
                if (Global.ExceptionCount < 2)
                {
                    Global.ExceptionCount++;

                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Method:");
                    sb.Append("InventoryCSVGenerate");
                    DH.SetLog(sb.ToString());

                    Thread.Sleep(60000);
                    Thread t = new Thread(() => InventoryCSVGenerate(Credentials, LastExeInfo));
                    t.Start();

                    t.Join();

                }
            }
            catch (Exception ex)
            {

                throw;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("Retry:InventoryCSVGenerate");
                DH.SetErrorLog(sb.ToString());

            }
        }
        private void SleepInvoice(DBCredentials Credentials, LastExecutionInfo LastExeInfo)
        {
            try
            {
                if (Global.ExceptionCount < 2)
                {
                    Global.ExceptionCount++;
                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Method:");
                    sb.Append("InvoiceCSVGenerate");
                    DH.SetLog(sb.ToString());

                    Thread.Sleep(60000);
                    Thread t = new Thread(() => InvoiceCSVGenerate(Credentials, LastExeInfo));
                    t.Start();

                    t.Join();

                }
            }
            catch (Exception ex)
            {
                throw;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("Retry:InvoiceCSVGenerate");
                DH.SetErrorLog(sb.ToString());
            }
        }


        #endregion

        private void tab_Credential_Click(object sender, EventArgs e)
        {

        }

        private void Home_Load(object sender, EventArgs e)
        {

        }
    }
}
