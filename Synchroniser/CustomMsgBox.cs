﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Synchroniser
{
    public partial class CustomMsgBox : Form
    {
        public CustomMsgBox()
        {
            InitializeComponent();
        }   

        private void btn_OK_Click(object sender, EventArgs e)
        {
            string Destination = ConfigurationManager.AppSettings["DESTINATION_PATH"] + "\\Log";
            System.Diagnostics.Process.Start(Destination);
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
