﻿using System;
using DP360CRMCommunication.Model;
using System.Configuration;
using Common;
using Newtonsoft.Json;
using System.Text;
using Common.Helper;
using Common.Model;

namespace DP360CRMCommunication
{
    public class BLDealer
    {
        public int Set(Dealer Dealer, string token)
        {
            //Receive Auth token
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
            //string token = auth.Authenticate();
            CustomHttpResponse response = new CustomHttpResponse();
            try
            {


                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.DealerPOSTAPIUrl;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(Dealer);
                    response = HttpOperation.PostRequest(Url, JsonString, token);
                    //Status code needs to be checked later
                    if (response.StatusCode == "OK")
                    {
                        return 1;
                    }
                    DealerResponse res = new DealerResponse();
                    if (res.code != 200)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLDealer-Set"); 
                        DH.SetErrorLog(sb.ToString());
                        throw new Common.CustomException.APIException(res.message, res.code.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLDealer in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data saving in  DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLDealer-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return 0;
        }
        public DealerCommon Get(DBCredentials credential)
        {
            string token = Global.Token;
            DealerCommon DealerCommonInfo = new DealerCommon();
            try
            {


                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string dealerID = credential.dealer_id.ToString();
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.DealerGETAPIUrl + dealerID + Constants.json;
                    response = HttpOperation.GetRequest(Url, token);
                    //check the response message and return accordingly\
                    DealerResponse dealerResponse = new DealerResponse();
                    dealerResponse = (DealerResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(DealerResponse));
                    if (dealerResponse.data.Count > 0)
                    {
                        DealerCommonInfo = dealerResponse.data[0];
                    }

                    if (dealerResponse.code != 200)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLDealer-Get");
                        DH.SetErrorLog(sb.ToString());
                        throw new Common.CustomException.APIException(dealerResponse.message, dealerResponse.code.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLDealer in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Get");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during fetching data  from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Get";
                logdata.base_message = "BLDealer-Get";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return DealerCommonInfo;
        }
        public string GetDealerID()
        {
            string result = "";
            string token = Global.Token;
            try
            {


                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.GetSettingsAPIUrl;
                    response = HttpOperation.GetRequest(Url, token);

                    //check the response message and return accordingly\
                    // Credentials = (DBCredentials)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(DBCredentials));
                    DealerCommon dealer = new DealerCommon();
                    DealerResponse res = new DealerResponse();
                    //if (response.StatusCode == "OK")
                    //{

                    res = JsonConvert.DeserializeObject<DealerResponse>(response.Message);
                    dealer = res.data[0];
                    result = dealer.dealer_id.ToString();
                    //}

                    if (res.code != 200)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLDealer-GetDealerID");
                        DH.SetErrorLog(sb.ToString());
                        throw new Common.CustomException.APIException(res.message, res.code.ToString());
                    }
                }
                //result = "5000";
                return result;
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLDealer in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetDealerID");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during fetching dealer_id  from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetDealerID";
                logdata.base_message = "BLDealer-GetDealerID";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);

                throw;
            }
        }
    }
}
