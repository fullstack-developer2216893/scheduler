﻿using Common;
using Common.Helper;
using Common.Model;
using DP360CRMCommunication.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP360CRMCommunication
{
    public class BLCSVStatus
   
    {
        public CSVStatus Set(CSVStatusRequest stat) 
        {
            CSVStatusResponse res = new CSVStatusResponse();
            CSVStatus stat_res = new CSVStatus();
            try
            {             
                string token = Global.Token;
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.PostCSVStatusAPIUrl;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(stat);
                    response = HttpOperation.PostRequest(Url, JsonString, token, Global.DealerID);

                    //check the response message and return accordingly
                    if (response.Message != null)
                    {
                        res = JsonConvert.DeserializeObject<CSVStatusResponse>(response.Message);
                        if(res.data.dealer_id!=0)
                        {
                            stat_res = res.data;
                        }
                        if (res.message != "Talon CSV Status Data Saved")
                        {
                            DataHandler DH = new DataHandler();
                            StringBuilder sb = new StringBuilder();
                            sb.Append(response.Message);
                            sb.Append(Environment.NewLine);
                            sb.Append("Method:");
                            sb.Append("BLCSVStatus Set");
                            DH.SetErrorLog(sb.ToString());
                            throw new Common.CustomException.APIException(res.message, res.code.ToString());
                        }
                    }
                  

                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLCSVStatus in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");


                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLCSVStatus-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return stat_res;
        }

        public CSVStatus Get()
        {
            CSVStatus status = new CSVStatus();
            try
            {

                string dealer_id = Global.DealerID;
                string token = Global.Token;

             CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.GetCSVStatusAPIUrl + "/" + dealer_id + Constants.json;
                    response = HttpOperation.GetRequest(Url, token, dealer_id);

                    //check the response message and return accordingly\
                CSVStatusResponse res = new CSVStatusResponse(); 
                    if (response.StatusCode == "OK")
                    {
                        res = JsonConvert.DeserializeObject<CSVStatusResponse>(response.Message);
                        if (res.data.dealer_id!=0)
                        {
                            status = res.data;
                        }

                    }
                
                    if (res.code != 200 && res.message != "Success")
                    {
                        res = JsonConvert.DeserializeObject<CSVStatusResponse>(response.Message);
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLDBCredentials Get");
                        DH.SetErrorLog(sb.ToString());
                        DH.DeleteUserDataFromFile();
                        throw new Common.CustomException.APIException(res.message, res.code.ToString());
                    }
                }

              

            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLCSVStatus in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Get");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Get";
                logdata.base_message = "BLCSVStatus-Get";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return status;
        }
        public CSVStatusResponse Put(CSVStatus stat)
        {
            CSVStatusResponse res = new CSVStatusResponse();
            try
            {

                string dealer_id = Global.DealerID;
                string token = Global.Token;
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {

                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.PutCSVStatusAPIUrl + "/" + stat.id + Constants.json; 
                    CSVStatusRequest req = new CSVStatusRequest();
                    req.status = "1";
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    response = HttpOperation.PutRequest(Url, JsonString, token, Global.DealerID);

                    //check the response message and return accordingly
                    if (response.Message != null)
                    {
                        res = JsonConvert.DeserializeObject<CSVStatusResponse>(response.Message);
                        if (res.message!= "Talon CSV Status Updated")
                        {
                            DataHandler DH = new DataHandler();
                            StringBuilder sb = new StringBuilder();
                            sb.Append(response.Message);
                            sb.Append(Environment.NewLine);
                            sb.Append("Method:");
                            sb.Append("BLCSVStatus Set");
                            DH.SetErrorLog(sb.ToString());
                            throw new Common.CustomException.APIException(res.message, res.code.ToString());
                        }
                    }
                   

                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLCSVStatus in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");


                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLCSVStatus-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return res;
        }
    }
}
