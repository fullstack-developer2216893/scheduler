﻿using System;
using System.Configuration;
using DP360CRMCommunication.Model;
using Newtonsoft.Json;
using Common;
using System.Text;
using Common.Helper;

namespace DP360CRMCommunication.Helper 
{
    public class AuthenticateUser
    {
        public string Authenticate()
        {
            string Username = ConfigurationManager.AppSettings["Username"];
            string Password = ConfigurationManager.AppSettings["Password"];
            string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"users/get_token.json";
            string Token = "";
            string jsonstring = "{\"username\":\"" + Username + "\", \"password\":\"" + Password + "\"}";

            CustomHttpResponse response = HttpOperation.PostRequest(Url, jsonstring);
            AuthenticationResponse authresp = new AuthenticationResponse();
            if (response.StatusCode == "OK")
            { 

                authresp = (AuthenticationResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(AuthenticationResponse));
                Token = authresp.data.token;
            }
            else
            {
                throw new Common.CustomException.APIException(authresp.message, response.StatusCode);
            }
            return Token;

        }
        public string Authenticate(UserCredential user)
        {
            string Token = "";
            try
            {
                string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"api/2.0/vendors/token.json";

                string jsonstring = "{\"username\":\"" + user.username + "\", \"password\":\"" + user.password + "\"}";

                CustomHttpResponse response = HttpOperation.PostRequest(Url, jsonstring);
                AuthenticationResponse authresp = new AuthenticationResponse();

                if (response.StatusCode == "OK")
                {
                    authresp = JsonConvert.DeserializeObject<AuthenticationResponse>(response.Message);
                    Token = authresp.data.token;


                    UserCredential user_read = new UserCredential();
                    user_read = new DataHandler().GetUserDataFromFile();

                    if (user_read.password == null && user_read.username == null)
                    {
                        DataHandler DH = new DataHandler();
                        DH.SetUserDataToFile(user);
                    }

                }
                else
                {
                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(response.Message);
                    sb.Append(Environment.NewLine);
                    sb.Append("Method:");
                    sb.Append("Authenticate");
                    DH.SetErrorLog(sb.ToString());
                    throw new Common.CustomException.APIException(authresp.message, response.StatusCode);

                }

            }
            catch (Exception ex)
            {
                DataHandler DH = new DataHandler();
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                sb.Append(Environment.NewLine);
                sb.Append("Method:");
                sb.Append("Authenticate");
                DH.SetErrorLog(sb.ToString());

            }
            return Token;

        }


    }


}
