﻿using Common;
using Common.Helper;
using Common.Model;
using DP360CRMCommunication.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace DP360CRMCommunication
{
    public class BLLeads
    {
        public async Task<int> Set(List<Leads> LeadsList)
        {
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
            //string token = auth.Authenticate();
            int res = 0;
            string token = Global.Token;
            CustomHttpResponse response = new CustomHttpResponse();
            if (token.Length > 0)
            {
                string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LeadPOSTAPIUrl;
                Leads lead = new Leads();
                lead = LeadsList[0];
                string JsonString = JsonConvert.SerializeObject(lead);

                try
                {
                    response = await HttpOperation.PostRequestAsync(Url, JsonString, token, Global.DealerID.ToString());
                    //check the response message and return accordingly
                    if (response.StatusCode == "OK")
                    {
                        res = 1;
                    }
                    else if (response.code == 400 || response.code == 401 || response.code == 404)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLLeads-Set");
                        DH.SetErrorLog(sb.ToString());
                    }
                     

                }
                catch (Exception ex)
                {
                    ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLeads in API Layer");
                    ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set Async");
                    ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                    ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data saving in DP360 CRM. Check exception message for more details.");

                    CRMLogData logdata = new CRMLogData();
                    logdata.id = null;
                    logdata.log_type = "Error";
                    logdata.method_name = "Set";
                    logdata.base_message = "BLLeads-Set";
                    logdata.exception_message = ex.Message;
                    logdata.parameter_name = "no parameter";
                    logdata.stack_trace = ex.StackTrace;
                    //logdata.log_time = DateTime.Now;
                    //logdata.created = DateTime.Now;
                    //logdata.modified = DateTime.Now;

                    CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                    logdataResponse = new BLLogData().Set(logdata);
                    throw;
                }
            }
            return res;
        }

        public async Task<List<Leads>> GetList(int dealer_id)
        {
            string token = Global.Token;
            List<Leads> LeadsList = new List<Leads>();
            try
            {


                CustomHttpResponse response = new CustomHttpResponse();
                LeadsResponse LeadsResponse = new LeadsResponse();
                List<Leads> listData = new List<Leads>();
                if (token.Length > 0)
                {

                    int i = 0;
                    do
                    {
                        string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LeadPOSTAPIUrl + "/" + dealer_id + ".json?page=" + i++;
                        // string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"api/xml_inventories/5000.json?page=" + i++;
                        response = await HttpOperation.GetRequestAsync(Url, token);
                        //check the response message and return accordingly\
                        LeadsResponse = JsonConvert.DeserializeObject<LeadsResponse>(response.Message);
                        if (LeadsResponse.data.Count > 0)
                        {
                            listData.AddRange(LeadsResponse.data);
                        }
                        else
                        {
                            i = 0;
                        }
                    }
                    while (i > 0);





                    LeadsList = listData;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLeads in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetList");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during fetching data from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetList";
                logdata.base_message = "BLLeads-GetList";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return LeadsList;
        }

        public async Task<int> Put(List<Leads> LeadsList, int dealer_id)
        {
            int res = 0;
            try
            {

                string token = Global.Token;

                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LeadPOSTAPIUrl + "/" + dealer_id + ".json";
                    //string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"api/xml_inventories/5000.json";
                    string JsonString = JsonConvert.SerializeObject(LeadsList);
                    response = await HttpOperation.PutRequestAsync(Url, JsonString, token);
                    //check the response message and return accordingly\

                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLeads in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Put");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during updating data in DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Put";
                logdata.base_message = "BLLeads-Put";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return res;
        }
    }
}
