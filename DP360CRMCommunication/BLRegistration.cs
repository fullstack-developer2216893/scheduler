﻿using Common;
using Common.Helper;
using Common.Model;
using DP360CRMCommunication.Model;
using System;
using System.Configuration;
using System.Text;

namespace DP360CRMCommunication
{
    public class BLRegistration
    {
        public int Set(RegistrationInfo RegInfo)
        {
            int res = 0;

            CustomHttpResponse response = new CustomHttpResponse();
            try
            {


                /*string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + "PostAPIUrl";*/
                string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.RegPOSTAPIUrl;
                string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(RegInfo);
                response = HttpOperation.PostRequest(Url, JsonString);
                //check the response message and return accordingly
                if (response.StatusCode == "Created")
                {  
                    DataHandler DH = new DataHandler();
                    UserCredential user = new UserCredential();
                    user.username = RegInfo.username;
                    user.password = RegInfo.password;
                    user.dealer_id = RegInfo.dealer_id;
                    DH.SetUserDataToFile(user);
                    res = 1;

                }
                else
                {
                    DataHandler DH = new DataHandler();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(response.Message);
                    sb.Append(Environment.NewLine);
                    sb.Append("Method:");
                    sb.Append("BLRegistration-Set");
                    DH.SetErrorLog(sb.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLRegistration in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set ");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data saving in DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLRegistration-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return res;
        }
    }
}
