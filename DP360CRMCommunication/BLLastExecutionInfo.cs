﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using System.Configuration;
using System.Text;
using Common.Helper;
using Common.Model;


namespace DP360CRMCommunication
{
    public class BLLastExecutionInfo
    {
        public async Task<LastExecutionInfo> Set(LastExecutionInfo ExecutionInfo)
        {
            LastExecutionInfo info = new LastExecutionInfo();
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
            //string token = auth.Authenticate();

            try
            {


                string token = Global.Token;
                string dealer_id = Global.DealerID;
                CustomHttpResponse response = new CustomHttpResponse();
                LastExecutionSingleResponse lastexecutionresponse = new LastExecutionSingleResponse();
                if (token.Length > 0)
                {
                  
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LastExecutionPOSTAPIUrl;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(ExecutionInfo);
                    response = await HttpOperation.PostRequestAsync(Url, JsonString, token, dealer_id);
                    //check the response message and return accordingly
                    lastexecutionresponse = (LastExecutionSingleResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(LastExecutionSingleResponse));
                    if (lastexecutionresponse.code == 400 || lastexecutionresponse.code == 401 || lastexecutionresponse.code == 405)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLLastExecutionInfo-Set");
                        DH.SetErrorLog(sb.ToString());
                    }
                    else if (lastexecutionresponse.code == 201 || lastexecutionresponse.message == "Talon Log Data Saved")
                    {
                        info = lastexecutionresponse.data;
                    }
                   

                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLastExecutionInfo in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLLastExecutionInfo-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return info;
        }
        public LastExecutionInfo SET(LastExecutionInfo ExecutionInfo)
        {
            LastExecutionInfo info = new LastExecutionInfo();
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
            //string token = auth.Authenticate();
            try
            {


                string token = Global.Token;
                string dealer_id = Global.DealerID;
                CustomHttpResponse response = new CustomHttpResponse();
                //  LastExecutionResponse lastexecutionresponse = new LastExecutionResponse();
                LastExecutionSingleResponse lastexecutionresponse = new LastExecutionSingleResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LastExecutionPOSTAPIUrl;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(ExecutionInfo);
                    response = HttpOperation.PostRequest(Url, JsonString, token, dealer_id);
                    //check the response message and return accordingly
                    lastexecutionresponse = (LastExecutionSingleResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(LastExecutionSingleResponse));
                    if (lastexecutionresponse.code == 400 || lastexecutionresponse.code == 401 || lastexecutionresponse.code == 405)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLLastExecutionInfo-SET");
                        DH.SetErrorLog(sb.ToString());
                    }
                    else if (lastexecutionresponse.code == 201 || lastexecutionresponse.message == "Talon Log Data Saved")
                    {
                        info = lastexecutionresponse.data;
                    }

                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLastExecutionInfo in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "SET";
                logdata.base_message = "BLLastExecutionInfo-SET";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return info;
        }

        public LastExecutionInfo Get()
        {
            string token = Global.Token;
            LastExecutionInfo ExecutionInfo = new LastExecutionInfo();
            try
            {


                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LastExecutionGETAPIUrl;
                    response = HttpOperation.GetRequest(Url, token);
                    //check the response message and return accordingly\
                    ExecutionInfo = (LastExecutionInfo)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(LastExecutionInfo));
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLastExecutionInfo in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Get");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during fetching data  from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Get";
                logdata.base_message = "BLLastExecutionInfo-Get";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return ExecutionInfo;
        }


        public List<LastExecutionInfo> GetList(string Date)
        {

            string token = Global.Token;
            List<LastExecutionInfo> ExecutionInfo = new List<LastExecutionInfo>();
            try
            {


                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                CustomHttpResponse response = new CustomHttpResponse();
                string Url = "";
                if (token.Length > 0)
                {
                    if (Date == null || Date == "")
                    {
                        Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LastExecutionGETAPIUrl + "?date=" + "1900-01-01";
                    }
                    else
                    {
                        Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.LastExecutionGETAPIUrl + "?date=" + Date;
                    }

                    string dealer_id = Global.DealerID;
                    response = HttpOperation.GetRequest(Url, token, dealer_id);
                    //check the response message and return accordingly\
                    LastExecutionResponse LastExecutionResponse = new LastExecutionResponse();
                    LastExecutionResponse = (LastExecutionResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(LastExecutionResponse));
                    ExecutionInfo = LastExecutionResponse.data.ToList();
                    if (LastExecutionResponse.code != 200 && LastExecutionResponse.code != 0 && LastExecutionResponse.message != "Success")
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLLastExecutionInfo-GetList");
                        DH.SetErrorLog(sb.ToString());
                        throw new Common.CustomException.APIException(LastExecutionResponse.message, LastExecutionResponse.code.ToString());
                    }

                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLLastExecutionInfo in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetList");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during fetching data  from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetList";
                logdata.base_message = "BLLastExecutionInfo-GetList";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now; 
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return ExecutionInfo;
        }

    }
}
