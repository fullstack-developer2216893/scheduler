﻿using Common;
using Common.Helper;
using Common.Model;
using DP360CRMCommunication.Model;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace DP360CRMCommunication
{
    public class BLInvoice
    {
        public async Task<int> Set(CRMInvoice InvoiceList)
        {
            int res = 0;
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
            //string token = auth.Authenticate();
            //InvoiceResponse invResponse = new InvoiceResponse();
            try
            {


                string token = Global.Token;
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InvoicePOSTAPIUrl;
                    string JsonString = JsonConvert.SerializeObject(InvoiceList);
                    response = await HttpOperation.PostRequestAsync(Url, JsonString, token, Global.DealerID.ToString());
                    if (response.StatusCode == "Created")
                    {
                        res = 1;
                    }
                    else if (response.code == 400 || response.code == 401 || response.code == 404)
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLInvoice-Set"); 
                        DH.SetErrorLog(sb.ToString());
                    }

                   
                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLInvoice in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLInvoice-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return res;
        }
    }
}
