﻿using Common;
using Common.Helper;
using Common.Model;
using DP360CRMCommunication.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace DP360CRMCommunication
{
    public class BLInventory
    {
        private object CustomException;

        
        public async Task<InventorySyncResponse> Set(List<CRMInventory> InventoryList)
        {
            InventorySyncResponse InvResponse = new InventorySyncResponse();
            try
            {
                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();

                string token = Global.Token;
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InventoryPOSTAPIUrl;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(InventoryList);
                    response = await HttpOperation.PostRequestAsync(Url, JsonString, token, Global.DealerID);
                    //check the response message and return accordingly 
                    InvResponse = JsonConvert.DeserializeObject<InventorySyncResponse>(response.Message);

                    if (response.StatusCode != "Created")
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLInventory-Set");
                        DH.SetErrorLog(sb.ToString());
                        throw new Common.CustomException.APIException(InvResponse.message, InvResponse.code.ToString());
                    }
                    //else if (InvResponse.code == 400 || InvResponse.code == 405)
                    //{
                    //    DataHandler DH = new DataHandler();
                    //    StringBuilder sb = new StringBuilder();
                    //    sb.Append(response.Message);
                    //    sb.Append(Environment.NewLine);
                    //    sb.Append("Method:");
                    //    sb.Append("BLInventory-Set");
                    //    DH.SetErrorLog(sb.ToString());
                    //}

                }
            }
            catch (Exception ex)
            {
                //DataHandler dh = new DataHandler();
                //dh.SetUserResultToFile(ex.Message);
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLInventory in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLInventory-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return InvResponse;
        }
        public InventorySyncResponse SetList(List<CRMInventory> InventoryList)
        {
            InventorySyncResponse InvResponse = new InventorySyncResponse();
            try
            {


                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();

                string token = Global.Token;
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InventoryPOSTAPIUrl;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(InventoryList);
                    response = HttpOperation.PostRequest(Url, JsonString, token, Global.DealerID);
                    //check the response message and return accordingly

                    InvResponse = JsonConvert.DeserializeObject<InventorySyncResponse>(response.Message);

                }
            }
            catch (Exception ex)
            {


            }
            return InvResponse;
        }
        /*
        public async Task<CRMInventory> Get(string token)
        {
            CRMInventory InventoryInfo = new CRMInventory();
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
           //string token = auth.Authenticate();
            CustomHttpResponse response = new CustomHttpResponse();
            if (token.Length > 0)
            {
                string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InventoryPOSTAPIUrl;
                response = await HttpOperation.GetRequestAsync(Url, token);
                //check the response message and return accordingly\
                InventoryInfo = (CRMInventory)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(CRMInventory));
            }
            return InventoryInfo;
        }

        public CRMInventory Get()
        {
            string token = Global.Token;
            CRMInventory InventoryInfo = new CRMInventory();
            //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
            //string token = auth.Authenticate();
            CustomHttpResponse response = new CustomHttpResponse();
            if (token.Length > 0)
            {
                string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + "GetAPIUrl";
                response = HttpOperation.GetRequest(Url, token);
                //check the response message and return accordingly\
                InventoryInfo = (CRMInventory)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(CRMInventory));
            }
            return InventoryInfo;
        }*/
        public async Task<List<CRMInventory>> GetList()
        {
            string token = Global.Token;
            string dealer_id = Global.DealerID;
            List<CRMInventory> InventoryList = new List<CRMInventory>();
            try
            {
                CustomHttpResponse response = new CustomHttpResponse();
                InventoryResponse InvResponse = new InventoryResponse();
                List<CRMInventory> listData = new List<CRMInventory>();
                if (token.Length > 0)
                {

                    int i = 1;
                    //do
                   // {
                        string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InventoryGETAPIUrl + dealer_id + ".json?page=" + i++;
                        //string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"api/xml_inventories/5000.json?page=" + i++;
                        response = await HttpOperation.GetRequestAsync(Url, token, dealer_id);
                        //check the response message and return accordingly\
                        InvResponse = JsonConvert.DeserializeObject<InventoryResponse>(response.Message);
                        
                        if (InvResponse.code != 200)
                        {
                            DataHandler DH = new DataHandler();
                            StringBuilder sb = new StringBuilder();
                            sb.Append(response.Message);
                            sb.Append(Environment.NewLine);
                            sb.Append("Method:");
                            sb.Append("BLInventory-GetList");
                            DH.SetErrorLog(sb.ToString());
                            throw new Common.CustomException.APIException(InvResponse.message, InvResponse.code.ToString());
                        }
                        if (InvResponse.data.Count > 0)
                        {
                            listData.AddRange(InvResponse.data);
                            InventoryList = listData;
                        }

                   // }
                   // while (InvResponse.data.Count > 0);

                    
                }
            }
            //catch(Common.CustomException.APIException ex)
            //{
            //    ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "DBBilledWorkOrder in API call");
            //    ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetList Async");
            //    ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
            //    ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occurs in the API. Check exception message for more details");
            //    throw;
            //}
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLInventory in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "GetList");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching from DP360 CRM. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetList";
                logdata.base_message = "BLInventory-GetList";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return InventoryList;
        }
        public List<CRMInventory> GetAPIList()
        {
            string token = Global.Token;
            string dealer_id = Global.DealerID;
            List<CRMInventory> InventoryList = new List<CRMInventory>();
            try
            {
                CustomHttpResponse response = new CustomHttpResponse();
                InventoryResponse InvResponse = new InventoryResponse();
                List<CRMInventory> listData = new List<CRMInventory>();
                if (token.Length > 0)
                {

                    int i = 0;
                    do
                    {
                        string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InventoryGETAPIUrl + dealer_id + ".json?page=" + i++;
                        //string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"api/xml_inventories/5000.json?page=" + i++;
                        response = HttpOperation.GetRequest(Url, token, dealer_id);
                        //check the response message and return accordingly\
                        InvResponse = JsonConvert.DeserializeObject<InventoryResponse>(response.Message);
                        if (InvResponse.data.Count > 0)
                        {
                            listData.AddRange(InvResponse.data);
                        }

                    }
                    while (InvResponse.data.Count > 0);

                    InventoryList = listData;
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLInventory in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Put");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during  updating data in DP360 CRM. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "GetAPIList";
                logdata.base_message = "BLInventory-GetAPIList";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;

            }
            return InventoryList;
        }
        public async Task<InventorySyncResponse> Put(List<CRMInventory> InventoryList, int dealer_id)
        {
            //int res = 0;
            InventorySyncResponse InvResponse = new InventorySyncResponse();
            try
            {
                string token = Global.Token;

                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                CustomHttpResponse response = new CustomHttpResponse();

                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.InventoryGETAPIUrl + dealer_id + Constants.json;
                    // string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + @"api/xml_inventories/5000.json";
                    string JsonString = JsonConvert.SerializeObject(InventoryList);
                    response = await HttpOperation.PutRequestAsync(Url, JsonString, token, dealer_id.ToString());
                    //check the response message and return accordingly\
                    InvResponse = JsonConvert.DeserializeObject<InventorySyncResponse>(response.Message);

                    if (response.StatusCode!="Created")
                    {
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLInventory-Put");
                        DH.SetErrorLog(sb.ToString());
                        throw new Common.CustomException.APIException(InvResponse.message, InvResponse.code.ToString());
                    }
                    //else if (InvResponse.code == 400 || InvResponse.code == 401 || InvResponse.code == 405 || InvResponse.code == 500)
                    //{
                    //    DataHandler DH = new DataHandler();
                    //    DH.SetErrorLog(response.Message);
                    //}


                }
            }
            catch (Exception ex)
            {
                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLInventory in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Put");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during  updating data in DP360 CRM. Check exception message for more details");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Put";
                logdata.base_message = "BLInventory-Put";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now; 
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return InvResponse;
        }
    }
}
