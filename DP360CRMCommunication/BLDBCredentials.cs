﻿using System.Configuration;
using Common;
using DP360CRMCommunication.Model;
using Newtonsoft.Json;
using System;
using System.Text;
using Common.Helper;
using Common.Model;

namespace DP360CRMCommunication
{
    public class BLDBCredentials
    {
        public BLDBCredentialResponse Set(DBCredentials Credentials)
        {
            BLDBCredentialResponse res = new BLDBCredentialResponse();
            try
            {

                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                string token = Global.Token;
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    /*string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + "PostAPIUrl";*/
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.CredentialsAPIUrl + Constants.json;
                    string JsonString = Newtonsoft.Json.JsonConvert.SerializeObject(Credentials);
                    response = HttpOperation.PostRequest(Url, JsonString, token, Global.DealerID);
                    //check the response message and return accordingly

                    if (response.Message != null)
                    {
                        res = JsonConvert.DeserializeObject<BLDBCredentialResponse>(response.Message);
                        if (res.code != 200 && response.StatusCode != "Created")
                        {
                            DataHandler DH = new DataHandler();
                            StringBuilder sb = new StringBuilder();
                            sb.Append(response.Message);
                            sb.Append(Environment.NewLine);
                            sb.Append("Method:");
                            sb.Append("BLDBCredentials Set");
                            DH.SetErrorLog(sb.ToString());
                            throw new Common.CustomException.APIException(res.message, res.code.ToString());
                        }
                    }
                    else if (response.StatusCode == "Created")
                    {
                        res.message = "Credentials saved successfully";
                    }

                }
            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLDBCredentials in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Set");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during saving data in DP360 CRM. Check exception message for more details.");


                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Set";
                logdata.base_message = "BLDBCredentials-Set";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return res;
        }

        public DBCredentials Get()
        {
            DBCredentials Credentials = new DBCredentials();
            try
            {

                string dealer_id = Global.DealerID;
                string token = Global.Token;

                //Helper.AuthenticateUser auth = new Helper.AuthenticateUser();
                //string token = auth.Authenticate();
                CustomHttpResponse response = new CustomHttpResponse();
                if (token.Length > 0)
                {
                    string Url = ConfigurationManager.AppSettings["ApiBaseUrl"] + Constants.CredentialsAPIUrl + "/" + dealer_id + Constants.json;
                    response = HttpOperation.GetRequest(Url, token, dealer_id);

                    //check the response message and return accordingly\
                     //Credentials = (DBCredentials)Newtonsoft.Json.JsonConvert.DeserializeObject(response.Message, typeof(DBCredentials));
                    CredentialResponse res = new CredentialResponse();
                    if (response.StatusCode == "OK")
                    {
                        res = JsonConvert.DeserializeObject<CredentialResponse>(response.Message);
                        if (res.data.Count > 0)
                        {
                            Credentials = res.data[0];
                        } 
                    }
                    // BLDBCredentialResponse result = new BLDBCredentialResponse();
                    //  result = JsonConvert.DeserializeObject<BLDBCredentialResponse>(response.Message);
                    if (res.code != 200 && res.message != "Success")
                    {
                        res = JsonConvert.DeserializeObject<CredentialResponse>(response.Message);
                        DataHandler DH = new DataHandler();
                        StringBuilder sb = new StringBuilder();
                        sb.Append(response.Message);
                        sb.Append(Environment.NewLine);
                        sb.Append("Method:");
                        sb.Append("BLDBCredentials Get");
                        DH.SetErrorLog(sb.ToString());
                        DH.DeleteUserDataFromFile();
                        throw new Common.CustomException.APIException(res.message, res.code.ToString());
                    }
                }

                //Credentials.ip_address = "AMB-F6-DESK-073\\SQLEXPRESS14";
                //Credentials.db_name = "DEMO_DP360";
                //Credentials.username = "sa";
                //Credentials.password = "manager@123";
                //Credentials.dealer_id = 1370;

            }
            catch (Exception ex)
            {

                ex.Data.Add(Common.Helper.LogDataKey.EffectedClass, "BLDBCredentials in API Layer");
                ex.Data.Add(Common.Helper.LogDataKey.EffectedMethod, "Get");
                ex.Data.Add(Common.Helper.LogDataKey.LogType, Common.Helper.LogType.Exception);
                ex.Data.Add(Common.Helper.LogDataKey.Usermessage, "An exception occur during data fetching from DP360 CRM. Check exception message for more details.");

                CRMLogData logdata = new CRMLogData();
                logdata.id = null;
                logdata.log_type = "Error";
                logdata.method_name = "Get";
                logdata.base_message = "BLDBCredentials-Get";
                logdata.exception_message = ex.Message;
                logdata.parameter_name = "no parameter";
                logdata.stack_trace = ex.StackTrace;
                //logdata.log_time = DateTime.Now;
                //logdata.created = DateTime.Now;
                //logdata.modified = DateTime.Now;

                CRMLogDataResponse logdataResponse = new CRMLogDataResponse();
                logdataResponse = new BLLogData().Set(logdata);
                throw;
            }
            return Credentials;
        }

    }



}
