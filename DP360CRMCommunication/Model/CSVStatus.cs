﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP360CRMCommunication.Model
{
  public  class CSVStatus
    {
        public int vendor_id { get; set; }
        public int dealer_id { get; set; }
        public string status { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public int id { get; set; }
    } 
}
