﻿using System;

namespace DP360CRMCommunication.Model
{
    public class CRMInventory
    {
        public int? id { get; set; }
        public string inv_id { get; set; }
        public int? classid { get; set; }
        public string vehtype { get; set; }
        public string bodysubtype { get; set; }
        public string category_name { get; set; }
        public string condition { get; set; }
        public string year { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public decimal? price { get; set; }
        public string vin { get; set; }
        public string dealername { get; set; }
        public int? dealer_id { get; set; } 
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string telephone { get; set; }
        public string email { get; set; }
        public string stocknumber { get; set; }
        public string color { get; set; }
        public string miles { get; set; }
        public string description { get; set; }
        public bool? sold { get; set; }
        public DateTime? created { get; set; }
        public DateTime? modified { get; set; }
        public string suggested_price { get; set; }
        public string engine_id { get; set; }
        public string fuel_type { get; set; }
        public string unit_status { get; set; }
        public int? inventory_source_id { get; set; }
        public int? hidden { get; set; }
        public string floor_plans { get; set; }
        public int? sleeps { get; set; }
        //  public string other_data { get; set; }
        public CRMInventory()
        {
            inv_id = "";
            classid = 0;
            vehtype = "";
            bodysubtype = "";
            category_name = "";
            condition = "";
            year = "";
            make = "";
            model = "";
            price = 0;
            vin = "";
            dealername = "";
            dealer_id = 0;
            address = "";
            city = "";
            state = "";
            zipcode = "";
            telephone = "";
            email = "";
            stocknumber = "";
            color = "";
            miles = "";
            description = "";
            sold = false;
            created = Convert.ToDateTime("1900-01-01");
            modified = Convert.ToDateTime("1900-01-01");
            suggested_price = "";
            engine_id = "";
            fuel_type = "";
            unit_status = "";
            inventory_source_id = 0;
            hidden = 0;
            floor_plans = "";
            sleeps = 0;
        }
    }

}
