﻿namespace DP360CRMCommunication.Model
{
    public class InventoryResponseData
    {
        public string inv_id { get; set; }
        public string classid { get; set; }
        public string vehtype { get; set; }
        public string bodysubtype { get; set; }
        public string category_name { get; set; }
        public string condition { get; set; }
        public string year { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public string price { get; set; }
        public string vin { get; set; }
        public string dealername { get; set; }
        public string dealer_id { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string telephone { get; set; } 
        public string email { get; set; }
        public string stocknumber { get; set; }
        public string color { get; set; }
        public string miles { get; set; }
        public string description { get; set; }
        public string sold { get; set; }
        public string created { get; set; }
        public string modified { get; set; }
        public string suggested_price { get; set; }
        public string engine_id { get; set; }
        public string fuel_type { get; set; }
        public string unit_status { get; set; }
        public string inventory_source_id { get; set; }
        public string hidden { get; set; }
        public string floor_plans { get; set; }
        public string sleeps { get; set; }
    }
}
