﻿namespace DP360CRMCommunication.Model
{
    public class Contact
    {

        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string county { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string work_number { get; set; }
        public string mobile { get; set; }
        public string fax { get; set; }
        public string email { get; set; } 
        public string dob { get; set; }
        public string lead_status { get; set; }
        public int? sales_step { get; set; }
        public string status { get; set; }

    }
}
