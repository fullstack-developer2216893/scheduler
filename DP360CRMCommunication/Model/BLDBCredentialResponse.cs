﻿using Common;

namespace DP360CRMCommunication.Model
{
    public class BLDBCredentialResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public DBCredentials data { get; set; }

        public BLDBCredentialResponse()
        {
            data = new DBCredentials(); 
        }
    }

}
