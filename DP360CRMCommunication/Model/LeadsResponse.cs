﻿using System.Collections.Generic;

namespace DP360CRMCommunication.Model
{
    public class LeadsResponse
    {
        public string message { get; set; }
        public List<Leads> data { get; set; }

        public LeadsResponse()
        {
            data = new List<Leads>();
        }
    }
}
 