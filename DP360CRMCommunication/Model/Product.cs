﻿namespace DP360CRMCommunication.Model
{
    public class Product
    {
        public int? internal_inventory_id { get; set; }
        public int? trimid { get; set; }
        public int? year { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public string color { get; set; }
        public string condition { get; set; }
        public int? mileage { get; set; }
        public string category { get; set; }
        public string @class { get; set; }
        public string vin { get; set; }
        public string price { get; set; }
        public string location { get; set; }
        public string stockno { get; set; }

    }
}
 