﻿using System.Collections.Generic;

namespace DP360CRMCommunication.Model
{
    public class InventoryResponse
    {
        public string message { get; set; }
        public List<CRMInventory> data { get; set; }

        public int code;
        public InventoryResponse()
        {
            data = new List<CRMInventory>();
        }
    }
}
 