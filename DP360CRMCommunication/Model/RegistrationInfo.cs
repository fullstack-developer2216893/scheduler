﻿namespace DP360CRMCommunication.Model
{
    public class RegistrationInfo
    {
        public string dealer_id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string confirm_password { get; set; }
    }
}
 