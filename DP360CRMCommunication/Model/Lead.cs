﻿using System;

namespace DP360CRMCommunication.Model
{
    public class Lead
    {
        public DateTime date { get; set; }
        public DateTime time { get; set; }
        public string timezone { get; set; }
        public string lead_type { get; set; }
        public string sperson_first_name { get; set; }
        public string sperson_last_name { get; set; }
        public string comment { get; set; }
        public Contact contact { get; set; }
        public Product product { get; set; }
        public Lead()
        {
            contact = new Contact();
            product = new Product();
        }
    }

}
 