﻿namespace DP360CRMCommunication.Model
{
    public class Dealer
    {
        public int DealerId { get; set; }
        public int DealerDealId { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string TimeZone { get; set; }
        public string Source { get; set; }
        public Leads lead { get; set; }

        public Dealer()
        {
            lead = new Model.Leads();
        }
    }
}
  