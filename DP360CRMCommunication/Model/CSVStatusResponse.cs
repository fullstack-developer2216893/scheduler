﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DP360CRMCommunication.Model
{
   public class CSVStatusResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public CSVStatus data { get; set; }
        public CSVStatusResponse()
        {
            data = new CSVStatus();
        }
    } 
}
