﻿using System.Collections.Generic;

namespace DP360CRMCommunication.Model
{
    public class CRMInvoice
    {
        public string dealer_id { get; set; }
        public List<InvoiceData> data { get; set; }
        public CRMInvoice()
        {
            data = new List<InvoiceData>();
        }
    }
    public class InvoiceData
    {
        public string customer_id { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string timezone { get; set; }
        public string lead_type { get; set; }
        public string sperson_first_name { get; set; } 
        public string sperson_last_name { get; set; }
        public Contact contact { get; set; }
        public Product product { get; set; }
        public string comment { get; set; }
        public InvoiceData()
        {
            contact = new Contact();
            product = new Product();
        }
    }
}
