﻿namespace DP360CRMCommunication.Model
{
    public class AuthenticationResponse
    {
        public string message { get; set; }
        public string code { get; set; }
        public Data data { get; set; }

        public AuthenticationResponse()
        {
            data = new Data();
        }
    }
    public class Data
    {
        public string token { get; set; } 
    }
}
