﻿using Common;
using System.Collections.Generic;

namespace DP360CRMCommunication.Model
{
    public class CredentialResponse
    {
        public string message { get; set; }
        public int code { get; set; }
        public List<DBCredentials> data { get; set; }

        public CredentialResponse()
        {
            data = new List<DBCredentials>();
        } 
    }
}
