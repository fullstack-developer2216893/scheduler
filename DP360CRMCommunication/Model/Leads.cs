﻿using System;

namespace DP360CRMCommunication.Model
{
    public class Leads
    {
        public string dealer_id { get; set; }
        public DateTime date { get; set; }
        public DateTime time { get; set; }
        public string timezone { get; set; }
        public string source { get; set; }
        public Lead lead { get; set; }


        public Leads()
        {
            lead = new Lead();
        }

    }
}
 